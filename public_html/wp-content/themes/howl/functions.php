<?php
/**
 * @Functions
 * 
 */
?>
<?php
/*-------------------------------------------------------------------------*/
/* Theme name settings which is shared to some functions */
/*-------------------------------------------------------------------------*/
// Theme Title
$mtheme_ThemeTitle= "Howl";
// Theme Name
$mtheme_themename = "Howl";
$mtheme_themefolder = "howl";
// Notifier Info
$mtheme_notifier_name = "Howl";
$mtheme_notifier_url = "";
// Theme name in short
$mtheme_shortname = "mtheme_p2";
if (!defined('MTHEME')) {
	define('MTHEME', $mtheme_shortname);
}
if (!defined('MTHEME_NAME')) {
	define('MTHEME_NAME', $mtheme_themename);
}
// Stylesheet path
$mtheme_theme_path = get_template_directory_uri();
// Theme Options Thumbnail
$mtheme_theme_icon= $mtheme_theme_path . '/images/options/thumbnail.jpg';
// Minimum contents area
if ( ! isset( $content_width ) ) { $content_width = 756; }
define('MTHEME_MIN_CONTENT_WIDTH', $content_width);
// Maximum contents area
define('MTHEME_MAX_CONTENT_WIDTH', "1310");
define('MTHEME_FULLPAGE_WIDTH', "1310");
define('MTHEME_IMAGE_QUALITY', "100");
// Max Sidebar Count
define('MTHEME_MAX_SIDEBARS', "50");
// Demo Status
define('MTHEME_DEMO_STATUS', "0");
// Theme build mode flag. Disables default enqueue font.
define('MTHEME_BUILDMODE', "0");
//Switch off Plugin scripts
define('MTHEME_PLUGIN_SCRIPT_LOAD', "0");
//Session start if demo is switched On
if (MTHEME_DEMO_STATUS) {
	//if (!isset($_SESSION)) session_start();
}
// Flush permalinks on Theme Switch
function mtheme_rewrite_flush() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'mtheme_rewrite_flush' );
remove_action('init', 'mtheme_shortcode_plugin_script_style_loader');
/*-------------------------------------------------------------------------*/
/* Constants */
/*-------------------------------------------------------------------------*/
$mtheme_theme_path = get_template_directory_uri();
$mtheme_template_path=get_template_directory();
define('MTHEME_PARENTDIR', $mtheme_template_path);
define('MTHEME_FRAMEWORK', MTHEME_PARENTDIR . '/framework/' );
define('MTHEME_FRAMEWORK_PLUGINS', MTHEME_FRAMEWORK . 'plugins/' );
define('MTHEME_OPTIONS_ROOT', MTHEME_FRAMEWORK . 'options/' );
define('MTHEME_FRAMEWORK_ADMIN', MTHEME_FRAMEWORK . 'admin/' );
define('MTHEME_FRAMEWORK_FUNCTIONS', MTHEME_FRAMEWORK . 'functions/' );
define('MTHEME_FUNCTIONS', MTHEME_PARENTDIR . '/functions/' );
define('MTHEME_SHORTCODEGEN', MTHEME_FRAMEWORK . 'shortcodegen/' );
define('MTHEME_SHORTCODES', MTHEME_SHORTCODEGEN . 'shortcodes/' );
define('MTHEME_INCLUDES', MTHEME_PARENTDIR . '/includes/' );
define('MTHEME_WIDGETS', MTHEME_PARENTDIR . '/widgets/' );
define('MTHEME_IMAGES', MTHEME_PARENTDIR . '/images/' );
define('MTHEME_PATH', $mtheme_theme_path );
define('MTHEME_FONTJS', $mtheme_theme_path . '/js/font/' );

define('MTHEME_ROOT', get_template_directory_uri());
define('MTHEME_DEMO_ROOT', get_template_directory_uri() . '/framework/demopanel' );
define('MTHEME_CSS', get_template_directory_uri() . '/css' );
define('MTHEME_STYLESHEET', get_stylesheet_directory_uri());
define('MTHEME_JS', get_template_directory_uri() . '/js' );

/*-------------------------------------------------------------------------*/
/* Helper Variable for Javascript
/*-------------------------------------------------------------------------*/
function mtheme_uri_path_script() { 
?>
<script type="text/javascript">
var mtheme_uri="<?php echo esc_url( get_template_directory_uri() ); ?>";
</script>
<?php
}
add_action('wp_head', 'mtheme_uri_path_script');

/*-------------------------------------------------------------------------*/
/* Load Theme Options */
/*-------------------------------------------------------------------------*/
function mtheme_load_menu_scripts() {
    if(basename( $_SERVER['PHP_SELF']) == "nav-menus.php" ) {
        wp_enqueue_style( 'mtheme-admin-style', get_template_directory_uri() . '/framework/megamenu/megamenu.css' );
    }
}
add_action('admin_init', 'mtheme_load_menu_scripts');
/*-------------------------------------------------------------------------*/
/* Load MegaMenu */
/*-------------------------------------------------------------------------*/
require_once( MTHEME_FRAMEWORK . '/megamenu/megamenu.php' );
require_once( MTHEME_FRAMEWORK . '/megamenu/megamenu_admin.php' );
/*-------------------------------------------------------------------------*/
/* Load Theme Options */
/*-------------------------------------------------------------------------*/
require_once( MTHEME_OPTIONS_ROOT .'options-caller.php');

/*-------------------------------------------------------------------------*/
/* Theme Setup */
/*-------------------------------------------------------------------------*/
function mtheme_setup() {
	//Add Background Support
	add_theme_support( 'custom-background' );

	// Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	// Let WordPress manage the document title.
	add_theme_support( 'title-tag' );
	// Register Menu
	register_nav_menu( 'top_menu', 'Main Menu' );
	register_nav_menu( 'mobile_menu', 'Mobile Menu' );
	register_nav_menu( 'toggle_menu', 'Toggle Menu' );
	/*-------------------------------------------------------------------------*/
	/* Internationalize for easy localizing */
	/*-------------------------------------------------------------------------*/
	load_theme_textdomain( 'mthemelocal', get_template_directory() . '/languages' );
	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) ) {
		require_once( $locale_file );
	}

	/*-------------------------------------------------------------------------*/
	/* Enable shortcodes to Text Widgets */
	/*-------------------------------------------------------------------------*/
	add_filter('widget_text', 'do_shortcode');
	/*
	 * This theme styles the visual editor to resemble the theme style and column width.
	 */
	add_editor_style( array( 'css/editor-style.css' ) );
	/*-------------------------------------------------------------------------*/
	/* Add Post Thumbnails */
	/*-------------------------------------------------------------------------*/
	add_theme_support( 'post-thumbnails' );
	// This theme supports Post Formats.
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio') );
	set_post_thumbnail_size( 150, 150, true ); // default thumbnail size
	// add_image_size('blog-post', MTHEME_MIN_CONTENT_WIDTH, 300,true); // Blog post cropped
	// add_image_size('blog-full', MTHEME_FULLPAGE_WIDTH, '',true); // Blog post images
	add_image_size('gridblock-square', 450, 450, true ); // Fullsize
	add_image_size('gridblock-square-big', 750, 750, true ); // Fullsize

	add_image_size('gridblock-related', 120, 64,true); // Sidebar Related image
	add_image_size('gridblock-tiny', 160, 160,true); // Sidebar Thumbnails

	add_image_size('gridblock-small', 480, 342,true); // Portfolio Small
	add_image_size('gridblock-medium', 500, 356,true); // Portfolio Medium
	add_image_size('gridblock-large', 600, 428,true); // Portfolio Large

	add_image_size('gridblock-small-portrait', 480,600,true); // Portfolio Small
	add_image_size('gridblock-medium-portrait', 500,625,true); // Portfolio Medium
	add_image_size('gridblock-large-portrait', 600,750,true); // Portfolio Large

	add_image_size('gridblock-full', MTHEME_FULLPAGE_WIDTH, '',true); // Portfolio Full
	add_image_size('gridblock-ajax', 924, '', true ); // Fullsize
	//add_image_size('photowall-block', 700, '',true); // Portfolio Large
	add_image_size('admin-thumbnail', 50, '', true ); // Admin Thumbnail
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
}
add_action( 'after_setup_theme', 'mtheme_setup' );
add_filter('use_block_editor_for_post', '__return_false');
/*-----------------------*/
/* Demo Panel Action 	 */
/*-----------------------*/
add_action('mtheme_demo_panel', 'mtheme_demo_panel_display');
function mtheme_demo_panel_display() {
	if ( MTHEME_DEMO_STATUS ) { 
		require ( get_template_directory() . '/framework/demopanel/demo-panel.php');
	}
}
add_action('mtheme_get_sidebar_choice', 'mtheme_sidebar_choice');
function mtheme_sidebar_choice() {
	//Get the sidebar choice
	global $mtheme_sidebar_choice,$post;
	if ( isSet($post->ID) ) {
		$mtheme_sidebar_choice= get_post_meta($post->ID, MTHEME . '_sidebar_choice', true);
	}
	$site_layout_width=of_get_option('general_theme_page');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_layout'] ) ) {
			if ($_GET['demo_layout']=="boxed") {
				$site_layout_width="boxed";
			} else {
				$site_layout_width="fullwidth";
			}
		}
	}
}
function mtheme_footer_scripts() {
	echo esc_html( stripslashes_deep( of_get_option ( 'footer_scripts' ) ) );
}
add_action( 'wp_footer', 'mtheme_footer_scripts');
/*-------------------------------------------------------------------------*/
/* Load Framework sections
/*-------------------------------------------------------------------------*/
require_once (MTHEME_FRAMEWORK_FUNCTIONS . 'framework-functions.php');

add_action('mtheme_display_portfolio_single_navigation','mtheme_display_portfolio_single_navigation_action');
function mtheme_display_portfolio_single_navigation_action() {
	if (is_singular('mtheme_portfolio')) {

		$mtheme_post_archive_link = get_post_type_archive_link( 'mtheme_portfolio' );
		$theme_options_mtheme_post_arhive_link = of_get_option('portfolio_archive_page');
		if ($theme_options_mtheme_post_arhive_link!=0) {
			$mtheme_post_archive_link = get_page_link($theme_options_mtheme_post_arhive_link);
		}

		$portfolio_nav = mtheme_get_custom_post_nav();
		if (isSet($portfolio_nav['prev'])) {
			$previous_portfolio = $portfolio_nav['prev'];
		}
		if (isSet($portfolio_nav['next'])) {
			$next_portfolio = $portfolio_nav['next'];
		}
?>
	<nav>
		<div class="portfolio-nav-wrap">
			<div class="portfolio-nav">
				<?php
				if (isSet($portfolio_nav['prev'])) {
				?>
				<span title="<?php _e('Previous','mthemelocal'); ?>" class="portfolio-nav-item portfolio-prev">
					<a href="<?php echo esc_url( get_permalink( $previous_portfolio ) ); ?>"><i class="feather-icon-rewind"></i></a>
				</span>
				<?php
				}
				?>
				<span title="<?php _e('Gallery','mthemelocal'); ?>" class="portfolio-nav-item portfolio-nav-archive">
					<a href="<?php echo esc_url( $mtheme_post_archive_link ); ?>"><i class="feather-icon-grid"></i></a>
				</span>
				<?php
				if (isSet($portfolio_nav['next'])) {
				?>
				<span title="<?php _e('Next','mthemelocal'); ?>" class="portfolio-nav-item portfolio-next">
					<a href="<?php echo esc_url( get_permalink( $next_portfolio ) ); ?>"><i class="feather-icon-fast-forward"></i></a>
				</span>
				<?php
				}
				?>
			</div>
		</div>
	</nav>
<?php
	}
}

add_action('mtheme_display_gallery_single_navigation','mtheme_display_gallery_single_navigation_action');
function mtheme_display_gallery_single_navigation_action() {
	if (is_singular('mtheme_gallery')) {

		$mtheme_post_archive_link = get_post_type_archive_link( 'mtheme_gallery' );
		$theme_options_mtheme_post_arhive_link = of_get_option('gallery_archive_page');
		if ($theme_options_mtheme_post_arhive_link!=0) {
			$mtheme_post_archive_link = get_page_link($theme_options_mtheme_post_arhive_link);
		}

		$portfolio_nav = mtheme_get_custom_post_nav("mtheme_gallery");
		if (isSet($portfolio_nav['prev'])) {
			$previous_portfolio = $portfolio_nav['prev'];
		}
		if (isSet($portfolio_nav['next'])) {
			$next_portfolio = $portfolio_nav['next'];
		}
?>
	<nav>
		<div class="portfolio-nav-wrap">
			<div class="portfolio-nav">
				<?php
				if (isSet($portfolio_nav['prev'])) {
				?>
				<span title="<?php _e('Previous','mthemelocal'); ?>" class="portfolio-nav-item portfolio-prev">
					<a href="<?php echo esc_url( get_permalink( $previous_portfolio ) ); ?>"><i class="feather-icon-rewind"></i></a>
				</span>
				<?php
				}
				?>
				<span title="<?php _e('Gallery','mthemelocal'); ?>" class="portfolio-nav-item portfolio-nav-archive">
					<a href="<?php echo esc_url( $mtheme_post_archive_link ); ?>"><i class="feather-icon-grid"></i></a>
				</span>
				<?php
				if (isSet($portfolio_nav['next'])) {
				?>
				<span title="<?php _e('Next','mthemelocal'); ?>" class="portfolio-nav-item portfolio-next">
					<a href="<?php echo esc_url( get_permalink( $next_portfolio ) ); ?>"><i class="feather-icon-fast-forward"></i></a>
				</span>
				<?php
				}
				?>
			</div>
		</div>
	</nav>
<?php
	}
}

function mtheme_function_scripts_styles() {
	/*-------------------------------------------------------------------------*/
	/* Register Scripts and Styles
	/*-------------------------------------------------------------------------*/
	// JPlayer Script and Style

	wp_register_script( 'jPlayerJS', MTHEME_JS . '/html5player/jquery.jplayer.min.js', array( 'jquery' ),null, true );
	wp_register_style( 'css_jplayer', MTHEME_ROOT . '/css/html5player/jplayer.dark.css', array( 'MainStyle' ), false, 'screen' );

	// Touch Swipe
	wp_register_script( 'TouchSwipe', MTHEME_JS . '/jquery.touchSwipe.min.js', array( 'jquery' ),null, true );

	// Modernizer
	wp_register_script( 'Modernizer', MTHEME_JS . '/modernizr.custom.js', array( 'jquery' ),null, true );
	wp_register_script( 'Classie', MTHEME_JS . '/classie.js', array( 'jquery' ),null, true );

	// Owl Carousel
	wp_register_script( 'owlcarousel', MTHEME_JS . '/owlcarousel/owl.carousel.min.js', array( 'jquery' ), null,true );
	wp_register_style( 'owlcarousel_css', MTHEME_ROOT . '/css/owlcarousel/owl.carousel.css', array( 'MainStyle' ), false, 'screen' );

	// Donut Chart
	wp_register_script( 'DonutChart', MTHEME_JS . '/jquery.donutchart.js', array( 'jquery' ),null, true );

	wp_register_script( 'Typed', MTHEME_JS . '/typed.js', array( 'jquery' ),null, true );
    wp_enqueue_script( 'Typed' );

	// WayPoint
	wp_register_script( 'WayPointsJS', MTHEME_JS . '/waypoints/waypoints.min.js', array( 'jquery' ),null, true );

	// Before after
	wp_register_script( 'BeforeAfterMoveJS', MTHEME_JS . '/beforeafter/jquery.event.move.js', array( 'jquery' ),null, true );
	wp_register_script( 'BeforeAfterJS', MTHEME_JS . '/beforeafter/jquery.twentytwenty.js', array( 'jquery' ),null, true );

	// FlexSlider Script and Styles
	//wp_register_script( 'flexislider', MTHEME_JS . '/flexislider/jquery.flexslider.js', array('jquery') , '',true );
	//wp_register_style( 'flexislider_css', MTHEME_ROOT . '/css/flexislider/flexslider-page.css',array( 'MainStyle' ),false, 'screen' );

    // contactFormScript
    wp_register_script( 'contactform', MTHEME_JS . '/contact.js', array( 'jquery' ),null, true );

    // counter script
    wp_register_script( 'counter', MTHEME_JS . '/jquery.counterup.js', array( 'jquery' ),null, true );

	if( is_ssl() ) {
		$protocol = 'https';
	} else {
		$protocol = 'http';
	}

	$googlemap_apikey=of_get_option('googlemap_apikey');
	if (!isSet($googlemap_apikey)) {
		$googlemap_apikey = '';
	}
    // Google Maps Loader
    wp_register_script( 'GoogleMaps', $protocol . '://maps.google.com/maps/api/js?key='.$googlemap_apikey, array( 'jquery' ),null, false );

    // iSotope
    wp_register_script( 'isotope', MTHEME_JS . '/jquery.isotope.min.js', array( 'jquery' ), null,true );

	// Mobile Menu Script
	wp_register_style( 'MobileMenuCSS', MTHEME_CSS . '/menu/mobile-menu.css',array( 'MainStyle' ),false, 'screen' );

	// Responsive Style
	wp_register_style( 'ResponsiveCSS', MTHEME_CSS . '/responsive.css',array( 'MainStyle' ),false, 'screen' );

	// Custom Style
	//wp_register_style( 'CustomStyle', MTHEME_CSS . '/custom.css',array( 'MainStyle' ),false, 'screen' );

	// Dynamic Styles
	wp_register_style( 'Dynamic_CSS', MTHEME_CSS . '/dynamic_css.php',array( 'MainStyle' ),false, 'screen' );

/*-------------------------------------------------------------------------*/
/* Start Loading
/*-------------------------------------------------------------------------*/	
	/* Common Scripts */
	global $is_IE; //WordPress-specific global variable
	wp_enqueue_script('jquery');

	if($is_IE) {
		wp_enqueue_script( 'excanvas', MTHEME_JS . '/excanvas.js', array( 'jquery' ),null, true );
	}
	if (MTHEME_DEMO_STATUS) {
		wp_register_style( 'demo_css', MTHEME_DEMO_ROOT . '/demo.panel.css', array( 'MainStyle' ), false, 'screen' );	
		wp_register_script( 'demo_panel', MTHEME_DEMO_ROOT . '/js/demo-panel.js', array( 'jquery' ),null, true );
		wp_register_script( 'demo_cookie', MTHEME_DEMO_ROOT . '/js/jquery.cookie.js', array( 'jquery' ),null, true );
		wp_enqueue_style ('demo_css');
		wp_enqueue_script ('demo_cookie');
		wp_enqueue_script ('demo_panel');
	}
	$theme_menu_type = of_get_option('menu_type');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_menu'] ) ) {
			$theme_menu_type=$_GET['demo_menu'];
		}
	}
	if ( $theme_menu_type=="vertical" ) {
		wp_enqueue_script( 'verticalmenuJS', MTHEME_JS . '/menu/verticalmenu.js', array( 'jquery' ),null, true );
		wp_enqueue_style( 'verticalmenuCSS', MTHEME_CSS . '/menu/verticalmenu.css', array( 'MainStyle' ), false, 'screen' );
	} else {
		wp_enqueue_script( 'superfish', MTHEME_JS . '/menu/superfish.js', array( 'jquery' ),null, true );
		wp_enqueue_style( 'navMenuCSS', MTHEME_CSS . '/menu/superfish.css', array( 'MainStyle' ), false, 'screen' );
	}
	
	wp_register_script( 'magnific_lightbox', MTHEME_JS . '/magnific/jquery.magnific-popup.min.js', array( 'jquery' ),null, true );
	wp_register_style( 'magnific_lightbox', MTHEME_CSS . '/magnific/magnific-popup.css', array( 'MainStyle' ), false, 'screen' );

	wp_register_script( 'fotorama', MTHEME_JS . '/fotorama/fotorama.js', array( 'jquery' ),null, true );
	wp_register_style( 'fotoramacss', MTHEME_JS . '/fotorama/fotorama.css', array( 'MainStyle' ), false, 'screen' );

	wp_enqueue_script( 'EasingScript', MTHEME_JS . '/jquery.easing.min.js', array( 'jquery' ),null, true );
	wp_enqueue_script( 'portfolioloader', MTHEME_JS . '/page-elements.js', array( 'jquery' ), null,true );
	wp_localize_script('portfolioloader', 'ajax_var', array(
		'url' => esc_url( admin_url('admin-ajax.php') ),
		'nonce' => wp_create_nonce('ajax-nonce')
	));
	wp_enqueue_script( 'fitVids', MTHEME_JS . '/jquery.fitvids.js', array( 'jquery' ), null,true );
	wp_enqueue_script( 'stellar', MTHEME_JS . '/jquery.stellar.min.js', array( 'jquery' ), null,true );
	wp_enqueue_script ('WayPointsJS');
	wp_enqueue_script('hoverIntent');
    wp_enqueue_script('jquery-ui-core');
    wp_enqueue_script('jquery-ui-tooltip');

	wp_enqueue_style ('owlcarousel_css');

    wp_enqueue_script( 'Modernizer' );
    wp_enqueue_script( 'Classie' );
    
	wp_enqueue_script( 'stickymenu', MTHEME_JS . '/jquery.stickymenu.js', array( 'jquery' ), null,true );
	wp_enqueue_script( 'stickysidebar', MTHEME_JS . '/stickySidebar.js', array( 'jquery' ), null,true );

    wp_enqueue_script( 'magnific_lightbox' );
    wp_enqueue_style( 'magnific_lightbox' );

	if($is_IE) {
		wp_enqueue_script( 'ResponsiveJQIE', MTHEME_JS . '/css3-mediaqueries.js', array('jquery'),null, true );
	}
	wp_enqueue_script( 'custom', MTHEME_JS . '/common.js', array( 'jquery' ),null, true );

	/* Common Styles */
	wp_enqueue_style( 'MainStyle', get_stylesheet_directory_uri() . '/style.css',false, 'screen' );
	wp_enqueue_style( 'Animations', MTHEME_CSS . '/animations.css', array( 'MainStyle' ), false, 'screen' );

	wp_enqueue_style( 'fontAwesome', MTHEME_CSS . '/fonts/font-awesome/css/font-awesome.min.css', array( 'MainStyle' ), false, 'screen' );
	wp_enqueue_style( 'etFonts', MTHEME_CSS . '/fonts/et-fonts/et-fonts.css', array( 'MainStyle' ), false, 'screen' );
	wp_enqueue_style( 'featherFonts', MTHEME_CSS . '/fonts/feather-webfont/feather.css', array( 'MainStyle' ), false, 'screen' );
	wp_enqueue_style( 'lineFonts', MTHEME_CSS . '/fonts/fontello/css/fontello.css', array( 'MainStyle' ), false, 'screen' );
	wp_enqueue_style( 'simepleLineFont', MTHEME_CSS . '/fonts/simple-line-icons/simple-line-icons.css', array( 'MainStyle' ), false, 'screen' );

	//*** End of Common Script and Style Loads **//
	wp_enqueue_style ('MobileMenuCSS');

	// Conditional Load Flexslider
	if ( is_archive() || is_single() || is_search() || is_home() || is_page_template('template-bloglist.php') || is_page_template('template-bloglist-small.php') || is_page_template('template-bloglist_fullwidth.php') || is_page_template('template-gallery-posts.php') ) {
			wp_enqueue_script ('owlcarousel');
			wp_enqueue_style ('owlcarousel_css');
	}
	if ( is_singular('mtheme_portfolio') || is_singular('mtheme_gallery') ) {
		wp_enqueue_script ('BeforeAfterMoveJS');
		wp_enqueue_script ('BeforeAfterJS');
	}
	if ( is_singular('mtheme_gallery') ) {
		wp_enqueue_script ('fotorama');
		wp_enqueue_style ('fotoramacss');
	}
	if(is_single()) {
		// wp_enqueue_script ('flexislider');
		// wp_enqueue_style ('flexislider_css');
		// wp_enqueue_script ('TouchSwipe');
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
	// Conditional Load jPlayer
	if ( is_archive() || is_single() || is_search() || is_home() || is_page_template('template-fullscreen-home.php') || is_page_template('template-bloglist.php') || is_page_template('template-bloglist-small.php') || is_page_template('template-bloglist_fullwidth.php') || is_page_template('template-video-posts.php') || is_page_template('template-audio-posts.php') ) {
			wp_enqueue_script ('jPlayerJS');
			wp_enqueue_style ('css_jplayer');
	}
	// Conditional Load Contact Form
	if ( is_page_template('template-contact.php') ) {
		wp_enqueue_script ('contactform');
	}

	// Load Dynamic Styles last to over-ride all
	require_once ( MTHEME_PARENTDIR . '/css/dynamic_css.php' );
	wp_add_inline_style( 'ResponsiveCSS', $dynamic_css );

	// Conditional Load jQueries
	if(mtheme_got_shortcode('tabs') || mtheme_got_shortcode('accordion')) {
	    wp_enqueue_script('jquery-ui-core');
	    wp_enqueue_script('jquery-ui-tabs');
	    wp_enqueue_script('jquery-ui-accordion');
	}

	if(mtheme_got_shortcode('beforeafter') ) {
		wp_enqueue_script ('BeforeAfterMoveJS');
		wp_enqueue_script ('BeforeAfterJS');
	}

	if(mtheme_got_shortcode('portfoliogrid') || mtheme_got_shortcode('thumbnails') || is_post_type_archive() || is_tax() || is_singular('mtheme_gallery') ) {
		wp_enqueue_script ('isotope');
	}

	if(mtheme_got_shortcode('count')) { 
		wp_enqueue_script ('counter');
	}
	//Counter
	if(mtheme_got_shortcode('counter')) {  
		wp_enqueue_script ('DonutChart');
	}
	//Caraousel
	if(mtheme_got_shortcode('workscarousel')) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
	if(mtheme_got_shortcode('woocommerce_carousel_bestselling')) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}
	if(mtheme_got_shortcode('map')) {
		wp_enqueue_script ('GoogleMaps');
	}

	if( mtheme_got_shortcode('woocommerce_featured_slideshow') || mtheme_got_shortcode('blogcarousel') || mtheme_got_shortcode('slideshowcarousel') || mtheme_got_shortcode('recent_blog_slideshow') || mtheme_got_shortcode('recent_portfolio_slideshow') || mtheme_got_shortcode('portfoliogrid') || mtheme_got_shortcode('testimonials') ) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}

	if( mtheme_got_shortcode('audioplayer') || mtheme_got_shortcode('bloglist') ) {
		wp_enqueue_script ('jPlayerJS');
		wp_enqueue_style ('css_jplayer');
	}

	if( mtheme_got_shortcode('carousel_group') ) {
		wp_enqueue_script ('owlcarousel');
		wp_enqueue_style ('owlcarousel_css');
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_style( 'mtheme-ie', get_template_directory_uri() . '/css/ie.css', array( 'MainStyle' ), '' );

	// Embed a font
	if ( of_get_option('custom_font_embed')<>"" ) {
		echo stripslashes_deep( of_get_option('custom_font_embed') );
	}
	if ( of_get_option('custom_font_css')<>"" ) {
		$custom_font_css = stripslashes_deep( of_get_option('custom_font_css') );
		wp_add_inline_style( 'MainStyle', $custom_font_css );
	}

	//Start applying custom font families or fallbacks.
	$selected_css_heading_classes ='
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6,
.hero-text-wrap,
.client-say,
.sidebar h3,
.homemenu,
.mtheme-button,
#header-searchform #hs,
.vertical-menu,
.wpcf7-form p,
.toggle-menu-list li a,
.blog-details-section-inner,
.responsive-mobile-menu,
.page-link,
.entry-title h1,
.entry-title h2';

	$apply_selected_css_headings = $selected_css_heading_classes . ' {
		font-family: "Raleway","Helvetica Neue",Helvetica,Arial,sans-serif;
	}';
	wp_add_inline_style( 'MainStyle', $apply_selected_css_headings );
	wp_enqueue_style( 'Raleway', 'https://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,500,300,200,100' , array( 'MainStyle' ), null, 'screen' );
	wp_enqueue_style( 'PT_Sans', 'https://fonts.googleapis.com/css?family=PT+Sans:400,400italic,700,700italic' , array( 'MainStyle' ), null, 'screen' );
	wp_enqueue_style( 'PT_Mono', 'https://fonts.googleapis.com/css?family=PT+Mono' , array( 'MainStyle' ), null, 'screen' );

	// ******* Load Responsive and Custom Styles

	wp_enqueue_style ('ResponsiveCSS');
	wp_enqueue_style ('CustomStyle');

	// ******* No more styles will be loaded after this line

	// Load Fonts
	// This enqueue method through the function prevent any double loading of fonts.
	$page_contents = mtheme_enqueue_font ("page_contents");
	if ($page_contents != "Default Font") {
		wp_enqueue_style( $page_contents['name'], $page_contents['url'] , array( 'MainStyle' ), null, 'screen' );
	}

	$heading_font = mtheme_enqueue_font ("heading_font");
	if ($heading_font != "Default Font") {
		wp_enqueue_style( $heading_font['name'] , $heading_font['url'] , array( 'MainStyle' ), null, 'screen' );
	}

	$menu_font = mtheme_enqueue_font ("menu_font");
	if ($menu_font != "Default Font") {
		wp_enqueue_style( $menu_font['name'], $menu_font['url'] , array( 'MainStyle' ), null, 'screen' );
	}

	// Decorate header based on page settings
	if ( is_singular() ) {

		$page_title_image= '';
		$page_title_color= '';
		$header_background_color= '';
		$header_pagetitle_top_spacing= '';
		$header_pagetitle_bottom_spacing= '';
	
		$page_title_color= get_post_meta(get_the_id(), MTHEME . '_pagetitle_color', true);
		$header_background_color= get_post_meta(get_the_id(), MTHEME . '_pageheader_color', true);
		$header_pagetitle_top_spacing = get_post_meta(get_the_id(), MTHEME . '_pagetitle_top_spacing', true);
		$header_pagetitle_bottom_spacing = get_post_meta(get_the_id(), MTHEME . '_pagetitle_bottom_spacing', true);
		// Check which choice of image is set
		$page_title_image = get_post_meta(get_the_id(), MTHEME . '_header_image', true);

		$bg_att_type = 'fixed';
		// get header search form of choice
		$header_pagetitle_bg_position = "50% 0";

		// Apply CSS styles
		$header_image_style = '
		.title-container-wrap {
			background-image:url('.$page_title_image.');
			background-repeat:repeat;
			background-attachment:'. $bg_att_type .';
			background-position: '.$header_pagetitle_bg_position.';
			background-size:cover;
		}';
		wp_add_inline_style( 'MainStyle', $header_image_style );

		$page_title_color_set = '
		.title-container .entry-title h1 {
			color:'.$page_title_color.';
		}';
		wp_add_inline_style( 'MainStyle', $page_title_color_set );

		$page_title_background_color_set = '
		body.header-compact .title-container-wrap {
			background-color:'.$header_background_color.';
		}';
		wp_add_inline_style( 'MainStyle', $page_title_background_color_set );

		if ($header_pagetitle_top_spacing > 0) {
			$pagetitle_top_spacing_set = '
			body .title-container-outer-wrap .title-container-wrap {
				padding-top:'.$header_pagetitle_top_spacing.'px;
			}';
			wp_add_inline_style( 'MainStyle', $pagetitle_top_spacing_set );
		}

		if ($header_pagetitle_bottom_spacing > 0) {
			$pagetitle_bottom_spacing_set = '
			body .title-container-outer-wrap .title-container-wrap {
				padding-bottom:'.$header_pagetitle_bottom_spacing.'px;
			}';
			wp_add_inline_style( 'MainStyle', $pagetitle_bottom_spacing_set );
		}
	}

}
add_action( 'wp_enqueue_scripts', 'mtheme_function_scripts_styles' );

// Pagination for Custom post type singular portfoliogallery
add_filter('redirect_canonical','mtheme_disable_redirect_canonical');
function mtheme_disable_redirect_canonical( $redirect_url ) {
    if ( is_singular( 'portfoliogallery' ) )
	$redirect_url = false;
    return $redirect_url;
}

add_filter( 'option_posts_per_page', 'mtheme_tax_filter_posts_per_page' );
function mtheme_tax_filter_posts_per_page( $value ) {
    return (is_tax('types')) ? 1 : $value;
}
// Add to Body Class
function mtheme_body_class( $classes ) {
	if ( ! is_multi_author() )
		$classes[] = 'single-author';

	if ( is_active_sidebar( 'sidebar-2' ) && ! is_attachment() && ! is_404() )
		$classes[] = 'sidebar';

	$skin_style = of_get_option('general_theme_style');
	$classes[] = $skin_style;
	if ( MTHEME_DEMO_STATUS ) {
		$classes[] = 'demo';
	}

	$page_data = get_post_custom( get_the_id() );

	$site_layout_width=of_get_option('general_theme_page');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_layout'] ) ) {
			$site_layout_width=$_GET['demo_layout'];
		}
	}

	if ( is_singular() ) {
		$page_title_image = get_post_meta(get_the_id(), MTHEME . '_header_image', true);
		if ( isSet($page_title_image) && !empty($page_title_image) ) {
			$classes[] = 'has-title-background';
		} else {
			$classes[] = 'no-title-background';
		}
	}

	$isactive = get_post_meta( get_the_id(), "mtheme_pb_isactive", true );
	if (isSet($isactive) && $isactive==1) {
		$classes[] = 'pagebuilder-active';
	}
	
	$menu_type = of_get_option('menu_type');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_menu'] ) ) {
			$menu_type=$_GET['demo_menu'];
		}
	}
	if ( $menu_type=="vertical" ) {
		$classes[] = 'menu-is-vertical';
	} else {
		$classes[] = 'menu-is-horizontal';
	}

	if ($site_layout_width=="boxed") {
		$classes[] = 'theme-boxed';
	} else {
		$classes[] = 'theme-fullwidth';
	}
	$classes[] = 'body-dashboard-push';

	//$classes[] = 'preloading-process';

	$footerwidget_status = of_get_option('footerwidget_status');
	if ($footerwidget_status) {
		$classes[] = 'footer-is-on';
	} else {
		$classes[] = 'footer-is-off';
	}

	$force_fullcapsoff = of_get_option('force_fullcapsoff');
	if ($force_fullcapsoff) {
		$classes[] = 'sitewide-capsoff';
	}
	$force_fullcapsoff = of_get_option('mobile_menuonwhite');
	if ($force_fullcapsoff) {
		$classes[] = 'mobile-menu-onwhite';
	}

	$force_header = of_get_option('force_compactheaders');
	if ( is_archive() || is_search() || is_404() || is_home() || $force_header==true || $force_header == 1) {
		$classes[] = 'header-compact';
		$classes[] = 'compact-on-bright';
	} else {
		if ( is_singular() ) {
			if (isset($page_data[MTHEME . '_pageheader_style'][0])) {
				$pageheader_style = $page_data[MTHEME . '_pageheader_style'][0];
				$classes[] = $pageheader_style;
				if ( $pageheader_style == 'compact-on-bright' || $pageheader_style == 'compact-on-dark' ) {
					$classes[] = 'header-compact';
				} else {
					$classes[] = 'header-extended';
				}
				if ($pageheader_style=="header-bright-on-overlay" || $pageheader_style=="header-dark-on-overlay") {
					$classes[] = 'header-on-overlay';
				}
				if ($pageheader_style=="header-bright-on-overlay") {
					$classes[] = 'header-bright';
				}
				if ($pageheader_style=="header-dark-on-overlay") {
					$classes[] = 'header-dark';
				}
				if ($pageheader_style=="header-bright-logo-only") {
					$classes[] = 'header-bright';
				}
				if ($pageheader_style=="header-dark-logo-only") {
					$classes[] = 'header-dark';
				}
				if ($pageheader_style=="header-dark-logo-only" || $pageheader_style=="header-bright-logo-only" ) {
					$classes[] = 'header-logo-only';
				}
				if ($pageheader_style=="header-bright-logo-only" || $pageheader_style=="header-dark-logo-only" || $pageheader_style=="header-bright-on-overlay" || $pageheader_style=="header-dark-on-overlay" || $pageheader_style=="header-bright" || $pageheader_style=="compact-on-bright" || $pageheader_style=="header-dark" || $pageheader_style=="compact-on-dark" ) {
					$classes[] = $pageheader_style;
				} else {
					$classes[] = 'header-compact';
					$classes[] = 'compact-on-bright';				
				}
			} else {
				$classes[] = 'header-compact';
				$classes[] = 'compact-on-bright';
			}
		}
	}
	if ( is_singular() ) {
		if (isset($page_data[MTHEME . '_pagetitle_header'][0])) {
			$page_header = $page_data[MTHEME . '_pagetitle_header'][0];
			if ($page_header=="hide") {
				$classes[] = "header-is-off";
			} else {
				$classes[] = "header-is-on";
			}
		}
		if (isset($page_data[MTHEME . '_pagestyle'][0])) {
			$pagestyle = $page_data[MTHEME . '_pagestyle'][0];
			$classes[] = $pagestyle;
		}
	}

	return $classes;
}
add_filter( 'body_class', 'mtheme_body_class' );
//@ Page Menu
function mtheme_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'mtheme_page_menu_args' );
/*-------------------------------------------------------------------------*/
/* Excerpt Lenght */
/*-------------------------------------------------------------------------*/
function mtheme_excerpt_length($length) {
	return 80;
}
add_filter('excerpt_length', 'mtheme_excerpt_length');
/**
 * Creates a nicely formatted and more specific title element text for output
 */
function mtheme_wp_title( $title, $sep ) {
	global $paged, $page; //WordPress-specific global variable

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'mthemelocal' ), max( $paged, $page ) );

	return $title;
}
//add_filter( 'wp_title', 'mtheme_wp_title', 10, 2 );
// Open Graph
if( of_get_option('opengraph_status') ) {
	function mtheme_opengraph_doctype( $output ) {
		return $output . ' xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://www.facebook.com/2008/fbml"';
	}
	add_filter('language_attributes', 'mtheme_opengraph_doctype');

	function mtheme_add_og_metatags() {
		global $post;

		if ( is_singular() ) {

			echo sprintf( '<meta property="og:title" content="%s"/>', strip_tags( str_replace( array( '"', "'" ), array( '&quot;', '&#39;' ), $post->post_title ) ) );		
			echo '<meta property="og:type" content="article"/>';
			echo sprintf( '<meta property="og:url" content="%s"/>', get_permalink() );
			echo sprintf( '<meta property="og:site_name" content="%s"/>', get_bloginfo('name') );
			echo sprintf( '<meta property="og:description" content="%s"/>', mtheme_shortentext( $post->post_content, 50 ) );
			if( ! has_post_thumbnail( $post->ID ) ) { 

				$main_logo_dark=of_get_option('main_logo_dark');
				if (isSet($main_logo_dark) && $main_logo_dark!=='') {
					echo sprintf( '<meta property="og:image" content="%s"/>', esc_url($main_logo_dark) );
				}
			} else {
				$og_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
				echo sprintf( '<meta property="og:image" content="%s"/>', esc_url( $og_image[0] ) );
			}
		}
	}
	add_action( 'wp_head', 'mtheme_add_og_metatags', 5 );
}
/**
 * Register Sidebars.
 */
function mtheme_widgets_init() {
	// Default Sidebar
	register_sidebar(array(
		'name' => 'Default Sidebar',
		'id' => 'default_sidebar',
		'description' => __('Default sidebar selected for pages, blog posts and archives.','mthemelocal'),
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	// Social Header Sidebar
	register_sidebar(array(
		'name' => 'Social Header',
		'id' => 'social_header',
		'description' => __('For social widget to display social icons.','mthemelocal'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	// Social Header Sidebar
	register_sidebar(array(
		'name' => 'Mobile Social Header',
		'id' => 'mobilesocial_header',
		'description' => __('For social widget to display social icons.','mthemelocal'),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	// Default Portfolio Sidebar
	register_sidebar(array(
		'name' => 'Default Portfolio Sidebar',
		'id' => 'portfolio_sidebar',
		'description' => __('Default sidebar for portfolio pages.','mthemelocal'),
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));

	// Dynamic Sidebar
	for ($sidebar_count=1; $sidebar_count <= MTHEME_MAX_SIDEBARS; $sidebar_count++ ) {

		if ( of_get_option('theme_sidebar'.$sidebar_count) <> "" ) {
			register_sidebar(array(
				'name' => of_get_option('theme_sidebar'.$sidebar_count),
				'description' => of_get_option('theme_sidebardesc'.$sidebar_count),
				'id' => 'sidebar_' . $sidebar_count . '',
				'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
				'after_widget' => '</aside></div>',
				'before_title' => '<h3>',
				'after_title' => '</h3>',
			));
		}
	}

	// Footer
	register_sidebar(array(
		'name' => 'Footer Single Column 1',
		'id' => 'footer_1',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name' => 'Footer Single Column 2',
		'id' => 'footer_2',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name' => 'Footer Single Column 3',
		'id' => 'footer_3',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'name' => 'Footer Single Column 4',
		'id' => 'footer_4',
		'before_widget' => '<div class="sidebar-widget"><aside id="%1$s" class="widget %2$s">',
		'after_widget' => '</aside></div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
}
add_action( 'widgets_init', 'mtheme_widgets_init' );
/*-------------------------------------------------------------------------*/
/* Load Admin */
/*-------------------------------------------------------------------------*/
	require_once (MTHEME_FRAMEWORK . 'admin/admin_setup.php');
/*-------------------------------------------------------------------------*/
/* Core Libraries */
/*-------------------------------------------------------------------------*/
function mtheme_load_core_libaries() {
	require_once (MTHEME_FRAMEWORK . 'admin/tgm/class-tgm-plugin-activation.php');
	require_once (MTHEME_FRAMEWORK . 'admin/tgm/tgm-init.php');
}
/*-------------------------------------------------------------------------*/
/* Theme Specific Libraries */
/*-------------------------------------------------------------------------*/
add_action('init','mtheme_load_theme_metaboxes');
function mtheme_load_theme_metaboxes() {
	require_once (MTHEME_FRAMEWORK . 'metaboxgen/metaboxgen.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/page-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/post-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/portfolio-metaboxes.php');
	require_once (MTHEME_FRAMEWORK . 'metaboxes/gallery-metaboxes.php');
}
/*-------------------------------------------------------------------------*/
/* Load Constants : Core Libraries : Update Notifier*/
/*-------------------------------------------------------------------------*/
mtheme_load_core_libaries();
/* Custom ajax loader */
add_filter('wpcf7_ajax_loader', 'mtheme_wpcf7_ajax_loader_icon');
function mtheme_wpcf7_ajax_loader_icon () {
	return  get_template_directory_uri() . '/images/preloaders/preloader.png';
}

// If WooCommerce Plugin is active.
if ( class_exists( 'woocommerce' ) ) {

	add_action('admin_init','mtheme_update_woocommerce_images');
	function mtheme_update_woocommerce_images() {
		global $pagenow;
		if( is_admin() && isset($_GET['activated']) && 'themes.php' == $pagenow ) {
			update_option('shop_catalog_image_size', array('width' => 400, 'height' => '', 0));
			update_option('shop_single_image_size', array('width' => 550, 'height' => '', 0));
			update_option('shop_thumbnail_image_size', array('width' => 150, 'height' => '', 0));
		}
	}

	add_theme_support( 'woocommerce' );

	add_action( 'woocommerce_before_shop_loop_item_title', 'mtheme_woocommerce_template_loop_second_product_thumbnail', 11 );
	// Display the second thumbnail on Hover
	function mtheme_woocommerce_template_loop_second_product_thumbnail() {
		global $product, $woocommerce;

		$attachment_ids = $product->get_gallery_image_ids();

		if ( $attachment_ids ) {
			$secondary_image_id = $attachment_ids['0'];
			echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '', $attr = array( 'class' => 'mtheme-secondary-thumbnail-image attachment-shop-catalog woo-thumbnail-fadeOutUp' ) );
		}
	}

	if ( !is_admin() ) {
		add_filter( 'post_class', 'mtheme_product_has_many_images' );
	}
	// Add pif-has-gallery class to products that have a gallery
	function mtheme_product_has_many_images( $classes ) {
		global $product;

		$post_type = get_post_type( get_the_ID() );

		if ( $post_type == 'product' ) {

			$attachment_ids = $product->get_gallery_image_ids();
			if ( $attachment_ids ) {
				$secondary_image_id = $attachment_ids['0'];
				$classes[] = 'mtheme-hover-thumbnail';
			}
		}

		return $classes;
	}
	// Remove sidebars from Woocommerce generated pages
	remove_action( 'woocommerce_sidebar', 'woocommerce_get_sidebar');

	//Remove Star rating from archives
	//remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

	add_filter( 'woocommerce_breadcrumb_home_url', 'woo_custom_breadrumb_home_url' );
	function woo_custom_breadrumb_home_url() {
		return home_url().'/shop/';
	}
	function mtheme_woocommerce_category_add_to_products(){

	    $product_cats = wp_get_post_terms( get_the_ID(), 'product_cat' );

	    if ( $product_cats && ! is_wp_error ( $product_cats ) ){

	        $single_cat = array_shift( $product_cats );

	        echo '<h4 itemprop="name" class="product_category_title"><span>'. $single_cat->name . '</span></h4>';

		}
	}
	add_action( 'woocommerce_single_product_summary', 'mtheme_woocommerce_category_add_to_products', 2 );
	add_action( 'woocommerce_before_shop_loop_item_title', 'mtheme_woocommerce_category_add_to_products', 12 );

	function mtheme_remove_cart_button_from_products_arcvhive(){
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
	}
	//add_action('init','mtheme_remove_cart_button_from_products_arcvhive');

	function mtheme_remove_archive_titles() {
		return false;
	}
	add_filter('woocommerce_show_page_title', 'mtheme_remove_archive_titles');

	add_action( 'wp_enqueue_scripts', 'mtheme_remove_woocommerce_styles', 99 );
	function mtheme_remove_woocommerce_styles() {
		wp_dequeue_style( 'woocommerce_prettyPhoto_css' );
		wp_dequeue_script( 'prettyPhoto-init' );
	}

	function howl_loop_shop_per_page() {
	    return 12;
	};
	add_filter('loop_shop_per_page', 'howl_loop_shop_per_page', 10, 0);

	// Change number or products per row to 4
	add_filter('loop_shop_columns', 'mtheme_loop_columns');
	if (!function_exists('loop_columns')) {
		function mtheme_loop_columns() {
			return 4; // 4 products per row
		}
	}

	// Remove rating from archives
	function mtheme_remove_ratings_loop(){
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
	}
	//add_action('init','mtheme_remove_ratings_loop');

	remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_title', 5 );
	//remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

	// Add save percent next to sale item prices.
	add_filter( 'woocommerce_sale_price_html', 'woocommerce_custom_sales_price', 10, 2 );
	function woocommerce_custom_sales_price( $price, $product ) {
		$percentage = round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );
		return $price . '<br/>' . sprintf( __('Save %s', 'mthemelocal' ), $percentage . '%' );
	}




add_action('mtheme_header_woocommerce_shopping_cart_counter','mtheme_woocommerce_item_count', 10, 2);
	if (!function_exists('mtheme_woocommerce_item_count')) {
		function mtheme_woocommerce_item_count() {
			 
			global $woocommerce;
			if (isSet($woocommerce)) {
				if (isSet($woocommerce->cart)) {
					?>
			<div class="mtheme-header-cart cart">
				<span class="header-cart header-cart-toggle"><i class="feather-icon-cross"></i></span>
				<?php
				if ( $woocommerce->cart->cart_contents_count==0 ) {
				?>
				<div class="cart-contents">
						<div class="cart-empty">
						<?php _e('Your cart is currently empty.','mthemelocal'); ?>
						</div>		
				</div>		
				<?php
				}
				?>
			</div>
			<?php
				}
			}
		 
		}
	}
add_filter('add_to_cart_fragments', 'mtheme_woo_add_to_cart_fragment');
	if (!function_exists('mtheme_woo_add_to_cart_fragment')) {
		function mtheme_woo_add_to_cart_fragment( $fragments ) {
			global $woocommerce;

			ob_start();
			?>
			<div class="mtheme-header-cart cart">
				<span class="header-cart-close"><i class="feather-icon-cross"></i></span>
				<?php
				if ( $woocommerce->cart->cart_contents_count==0 ) {
				?>
				<div class="cart-contents">
						<div class="cart-empty">
						<?php _e('Your cart is currently empty.','mthemelocal'); ?>
						</div>		
				</div>		
				<?php
				}
				?>
				<?php if(!$woocommerce->cart->cart_contents_count): ?>

				<?php else: ?>
				<div class="cart-contents">
					<?php foreach($woocommerce->cart->cart_contents as $cart_item): ?>
					<div class="cart-elements clearfix">
						<a href="<?php echo get_permalink($cart_item['product_id']); ?>">
						<div class="cart-element-image">
							<?php $thumbnail_id = ($cart_item['variation_id']) ? $cart_item['variation_id'] : $cart_item['product_id']; ?>
							<?php echo get_the_post_thumbnail($thumbnail_id, 'gridblock-tiny'); ?>
						</div>
						<div class="cart-content-text">
							<span class="cart-title"><?php echo get_the_title($cart_item['product_id']); ?></span>
							<span class="cart-item-quantity-wrap"><span class="cart-item-quantity"><?php echo $cart_item['quantity']; ?> x </span><?php echo $woocommerce->cart->get_product_subtotal($cart_item['data'], 1); ?></span>
						</div>
						</a>
					</div>
					<?php endforeach; ?>
					<div class="cart-content-checkout">
						<div class="cart-view-link"><a href="<?php echo get_permalink(get_option('woocommerce_cart_page_id')); ?>"><?php _e('View Cart', 'mthemelocal'); ?></a></div>
						<div class="cart-checkout-link"><a href="<?php echo get_permalink(get_option('woocommerce_checkout_page_id')); ?>"><?php _e('Checkout', 'mthemelocal'); ?></a></div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<?php
			$header_cart = ob_get_clean();
			$fragments['div.cart'] = $header_cart;

			return $fragments;
		}
	}

}
?>