<?php
$site_layout_width=of_get_option('general_theme_page');
if (MTHEME_DEMO_STATUS) {
	if ( isSet( $_GET['demo_layout'] ) ) {
		$site_layout_width=$_GET['demo_layout'];
	}
}
if ( !of_get_option('headersearch_disable') ) {
?>
<div id="header-search-bar-wrap">
	
	<div class="header-search-close">
	<i class="feather-icon-cross"></i>
	</div>
	<div class="header-search-bar">
		<div class="search-instructions"><?php _e('Search','mthemelocal'); ?></div>
		<form method="get" id="header-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<input type="text" value="" name="s" id="hs" class="right" />
		<button id="header-searchbutton" title="<?php esc_attr_e('Search','mthemelocal');?>" type="submit"><i class="feather-icon-search"></i></button>
		</form>
	</div>
</div>
<?php
}
if ( !of_get_option('togglemenu_disable') ) {
?>
<nav class="toggle-menu toggle-menu-close" id="toggle-menu">
	<a class="toggle-menu-trigger" href="#"><span><?php _e('Menu','mthemelocal'); ?></span></a>
	<div class="toggle-menu-wrap">
		<?php
		get_template_part('/includes/menu/toggle','menu');
		?>
	</div>
</nav>
<?php
}
$theme_menu_type = of_get_option('menu_type');
if (MTHEME_DEMO_STATUS) {
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_menu'] ) ) {
			$theme_menu_type=$_GET['demo_menu'];
		}
	}
}
if ( $theme_menu_type!="vertical" ) {
	get_template_part('/includes/menu/horizontal','menu');
}
?>
