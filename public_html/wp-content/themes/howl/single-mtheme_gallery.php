<?php
/*
 Single Portfolio Page
*/
?>
<?php get_header(); ?>
<?php
/**
*  Portfolio Loop
 */
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php

		global $mtheme_portfolio_current_post;
		$mtheme_portfolio_current_post=$post;
		$width=MTHEME_FULLPAGE_WIDTH;
		$portfolio_page_header="";
		$gallery_type="";
		$portfolio_client="";
		$portfolio_projectlink="";
		$portfolio_client_link="";
		
		$custom = get_post_custom($post->ID);
		$mtheme_pagestyle="fullwidth";
		if (isset($custom[MTHEME . '_pagestyle'][0])) $mtheme_pagestyle=$custom[MTHEME . '_pagestyle'][0];
		if (isset($custom[MTHEME . '_gallerytype'][0])) $gallery_type=$custom[MTHEME . '_gallerytype'][0];

		$floatside="float-left";
		$two_column='two-column';
		$floatside_portfolio="float-left";
		$fullwidth_column='';
		$floatside_portfolio_opp = "float-right";
		if ($mtheme_pagestyle=="edge-to-edge") { $floatside=""; $two_column=""; $fullwidth_column="edge-to-edge-column"; $floatside_portfolio = ""; $floatside_portfolio_opp = "";}
		if ($mtheme_pagestyle=="fullwidth") { $floatside=""; $two_column=""; $fullwidth_column="fullwidth-column"; $floatside_portfolio = ""; $floatside_portfolio_opp = "";}
		if ($mtheme_pagestyle=="rightsidebar") { $floatside="float-left"; $floatside_portfolio = $floatside; $floatside_portfolio_opp = "float-right"; }
		if ($mtheme_pagestyle=="leftsidebar") { $floatside="float-right"; $floatside_portfolio = $floatside; $floatside_portfolio_opp = "float-left";}

		if ( !isSet($mtheme_pagestyle) || $mtheme_pagestyle=="" ) {
			$mtheme_pagestyle="rightsidebar";
			$floatside="float-left";
		}

?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

	<div class="entry-content <?php echo $fullwidth_column; ?> clearfix">
		<div class="portfolio-header-left <?php echo $two_column; ?> <?php echo $floatside_portfolio; ?>">
		<?php
		$attchament_status="false";
		$gallery_image_linking = of_get_option('gallery_image_linking');
		if (isSet($gallery_image_linking)) {
			if ( $gallery_image_linking ==1 ) {
				$attchament_status="true";
			}
		}
		if ( ! post_password_required() ) {
			
			switch ($gallery_type) {

				case "grid" :
					$grid = do_shortcode('[thumbnails attachment_linking="'.$attchament_status.'" animated="true" columns="3" title="true" description="true" id="'.get_the_id().'"]');
					echo $grid;
					
				break;

				case "wall" :
					$grid = do_shortcode('[thumbnails attachment_linking="'.$attchament_status.'" animated="true" boxtitle="true" title="false" description="false" gutter="nospace" id="'.get_the_id().'"]');
					echo $grid;
					
				break;

				case "masonary" :
					$grid = do_shortcode('[thumbnails attachment_linking="'.$attchament_status.'" format="masonary" animated="true" boxtitle="true" title="false" description="false" gutter="nospace" id="'.get_the_id().'"]');
					echo $grid;
					
				break;

				case "fullscreen" :
					$fullcreen = do_shortcode('[fotorama filltype="cover" padeid="'.get_the_id().'"]');
					echo $fullcreen;
					
				break;

				case "fullscreenfit" :
					$fullcreen = do_shortcode('[fotorama filltype="contain" padeid="'.get_the_id().'"]');
					echo $fullcreen;
					
				break;

				case "vertical" :
					$mtheme_thepostID=$post->ID;
					//global $mtheme_thepostID;
					$vertical_images = do_shortcode('[vertical_images pageid="'.get_the_id().'" imagesize="gridblock-full"]');
					echo $vertical_images;
				break;
				
			}
		}
		?>
		<div class="always-center mtheme-spaced-below">
				<?php
				if ( is_single() && !post_password_required() ) {
					get_template_part('/includes/share','this');
				}
				?>
		</div>
		<?php
		if ( post_password_required() ) {
			echo '<div class="entry-portfolio-content entry-content clearfix">';
			echo '<div id="password-protected">';

				if (MTHEME_DEMO_STATUS) { echo '<p><h2>DEMO Password is 1234</h2></p>'; }
				echo get_the_password_form();
			echo '</div>';
			echo '</div>';

		} else {
			echo '<div class="entry-portfolio-content entry-content clearfix">';
			the_content();
			echo '</div>';
		}
		?>

	</div>
	<?php
			global $mtheme_pagestyle;
			if ($mtheme_pagestyle=="rightsidebar" || $mtheme_pagestyle=="leftsidebar" ) {
				get_sidebar();
			}
	?>
	</div>
<?php
if ( ! post_password_required() ) {
if (of_get_option('gallery_comments')) {
	if ( comments_open() ) {
	?>
	<div class="entry-portfolio-content portfolio-header-wrap entry-content clearfix">
		<div class="two-column float-left">
		<?php
			comments_template();
		?>
		</div>
	</div>
	<?php
	}
}
}
?>
	</div>
	<?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>