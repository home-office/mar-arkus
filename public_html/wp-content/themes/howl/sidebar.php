<?php
/**
*  Sidebar
 */
?>
<?php
global $mtheme_sidebar_choice,$mtheme_pagestyle;
if ( !is_singular() ) {
	unset($mtheme_sidebar_choice);
}
$sidebar_position="sidebar-float-right";
if ($mtheme_pagestyle=="rightsidebar") { $sidebar_position = 'sidebar-float-right'; }
if ($mtheme_pagestyle=="leftsidebar") { $sidebar_position = 'sidebar-float-left'; }
?>
<div id="sidebar" class="sidebar-wrap<?php if ( is_single() || is_page() ) { echo "-single"; } ?> <?php echo $sidebar_position; ?>">
		<div class="sidebar clearfix">
			<!-- begin Dynamic Sidebar -->
			<?php
			if ( !isset($mtheme_sidebar_choice) || empty($mtheme_sidebar_choice) ) {
				$mtheme_sidebar_choice="Default Sidebar";
			}
			?>
			<?php
			if ( !function_exists('dynamic_sidebar') 
			
				|| !dynamic_sidebar($mtheme_sidebar_choice) ) :
			
			endif;
			?>
		</div>
	</div>
</div>