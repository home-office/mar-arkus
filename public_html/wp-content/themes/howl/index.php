<?php get_header(); ?>
<?php
global $mtheme_pagelayout_type,$mtheme_pagestyle;;
$mtheme_pagelayout_type="two-column";
$floatside="float-left";
?>
<div class="contents-wrap <?php echo $floatside; ?> two-column">
	<?php
	$sticky_posts = get_option( 'sticky_posts' );
	if ( $sticky_posts ) {
		$args_sticky = array(
		    'post__in'  => $sticky_posts
		);
		//query_posts($args_sticky);

		$sticky_query = new WP_Query( $args_sticky );
	    ?>
		<div class="entry-content-wrapper post-is-sticky">
		<?php
		if ( $sticky_query->have_posts() ) :
			while ( $sticky_query->have_posts() ) : $sticky_query->the_post();
				require (TEMPLATEPATH . "/post-summary.php");
			endwhile;
		endif;
		wp_reset_postdata();
		?>
		</div>
	<?php
	}
	if ( get_query_var('paged') ) {
		$paged = get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = get_query_var('page');
	} else {
		$paged = 1;
	}
	query_posts('paged='.$paged.'&ignore_sticky_posts=1&posts_per_page=');
	?>
	<div class="entry-content-wrapper">
	<?php get_template_part( 'loop', 'blog' ); ?>
	</div>
</div>
<?php
get_sidebar();
get_footer();