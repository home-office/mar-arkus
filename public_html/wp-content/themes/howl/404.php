<?php
/*
404 Page
*/
?>
 
<?php get_header(); ?>

<div class="page-contents-wrap">
	<div class="entry-page-wrapper entry-content clearfix">
		<div class="mtheme-404-wrap">
			<div class="mtheme-404-icon">
				<i class="feather-icon-umbrella"></i>
			</div>
			<div class="mtheme-404-error-message1">Page not available</div>
			<div class="mtheme-404-error-message2">But we're still here to help.</div>
			<h4><?php _e( 'Try searching for the page.', 'mthemelocal' ); ?></h4>
			<?php get_search_form(); ?>
		</div>
	</div><!-- .entry-content -->
</div>

<?php get_footer(); ?>