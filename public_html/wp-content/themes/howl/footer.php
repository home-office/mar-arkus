<?php
/*
* Footer
*/
?>
<div class="clearfix"></div>
</div>
<footer>
<?php
if ( !wp_is_mobile() ) {
?>
<div id="goto-top" title="top of page"><i class="fa fa-chevron-up"></i></div>
<?php
}
if (!is_page_template('template-blank.php')) {
?>
	<?php
	if (of_get_option('footerwidget_status') ) {
	?>
	<div class="footer-container-wrap clearfix">
		<div class="footer-container clearfix">
			<div id="footer" class="sidebar widgetized clearfix">
			
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 1");  
					echo '</div>';
					}
				?>
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 2");  
					echo '</div>';
					}
				?>
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 3"); 
					echo '</div>';
					}
				?>
				<?php if ( function_exists('dynamic_sidebar') ) { 
					echo '<div class="footer-column">';
					dynamic_sidebar("Footer Single Column 4");   
					echo '</div>';
					}
				?>
			</div>	
		</div>
	</div>
	<?php
	} else {
		echo '<div class="footer-margin"></div>';
	}
	?>
<div id="copyright">
<?php
$footer_info = stripslashes_deep( of_get_option('footer_copyright') );
echo do_shortcode( $footer_info );
?>
</div>
</footer>
<?php
} // end of blank template check
?>
</div>
<?php
wp_footer();
?>
</body>
</html>