<form method="get" id="mobile-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
<input type="text" placeholder="<?php esc_attr_e('Search','mthemelocal'); ?>" value="" name="s" id="ms" class="right" />
<button id="mobile-searchbutton" title="<?php esc_attr_e('Search','mthemelocal');?>" type="submit"><i class="feather-icon-search"></i></button>
</form>