<?php
/*
Template Name: 100% Width Page
*/
?>
<?php get_header(); ?>
<?php
if ( post_password_required() ) {
	
	echo '<div id="password-protected">';

	if (MTHEME_DEMO_STATUS) { _e('<p><h2>DEMO Password is 1234</h2></p>','mthemelocal'); }
	echo get_the_password_form();
	echo '</div>';
	
	} else {
	?>
	<div id="homepage" class="<?php if ( is_front_page() ) { echo 'is-front-page'; } ?>">
	<?php
	get_template_part( 'loop', 'page' );
	?>
	</div>
	<?php
	}
	?>
<?php get_footer(); ?>