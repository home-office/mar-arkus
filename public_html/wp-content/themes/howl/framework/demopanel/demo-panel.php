<!-- 
**********************************************************************
DEMO Panel code - START
**********************************************************************
-->
<?php
$demo_layout='';
$demo_menu='';
$demo_background='';

if ( isset($_GET['demo_menu'] )) {
	$demo_menu = $_GET['demo_menu'];
}
if ( isset($_GET['demo_background'] )) {
	$demo_background = $_GET['demo_background'];
}
if ( isset($_GET['demo_layout'] )) { 
	$demo_layout = $_GET['demo_layout'];
	if ($demo_layout =="fullwidth") {
		$demo_background='';
	}
}
?>
<div id="demopanel">
	<div class="demo_toggle closedemo">
	</div>	
	<div class="demo_toggle opendemo">
	</div>
	
	<div class="paneloptions">
	<form action="#" id="demoform" method="get">
<div class="clear"></div>

<div class="demo_background">

	<div class="demo-main-title">
		Demo Panel
	</div>
	<p class="demo-content entry-content">
Create unique and beautiful sites using Howl theme. Demo panel focuses on some of the features that can be set using theme options.
	</p>
	<div class="demo-sub-title">
		Layout
	</div>
	<div class="demo-selector">
		<a href="?demo_layout=fullwidth">
			<div class="demo-selector-icon demo-selector-fullwidth demo-active-<?php echo $demo_layout; ?>">
				<i class="feather-icon-align-justify"></i>
			</div>
			<div class="demo-selector-label">
				Fullwidth
			</div>
		</a>
	</div>
	<div class="demo-selector">
		<a href="?demo_layout=boxed">
			<div class="demo-selector-icon demo-selector-boxed demo-active-<?php echo $demo_layout; ?>">
				<i class="feather-icon-paper"></i>
			</div>
			<div class="demo-selector-label">
				Boxed
			</div>
		</a>
	</div>
	<div class="demo-sub-title">
		Menu Style
	</div>
	<div class="demo-selector">
		<a href="?demo_menu=horizontal">
			<div class="demo-selector-icon demo-selector-horzontal-menu demo-active-<?php echo $demo_menu; ?>">
				<i class="feather-icon-arrow-left"></i>
			</div>
			<div class="demo-selector-label">
				Horizontal
			</div>
		</a>
	</div>
	<div class="demo-selector">
		<a href="?demo_menu=vertical">
			<div class="demo-selector-icon demo-selector-vertical-menu demo-active-<?php echo $demo_menu; ?>">
				<i class="feather-icon-arrow-up"></i>
			</div>
			<div class="demo-selector-label">
				Vertical
			</div>
		</a>
	</div>
	<div class="demo-sub-title">
		Boxed Background
	</div>
	<div class="demo-selector">
		<a href="<?php echo esc_url('?demo_background=1&demo_layout=boxed'); ?>">
			<div class="demo-selector-icon demo-selector-background1 demo-active-<?php echo $demo_background; ?>">
				<i class="feather-icon-image"></i>
			</div>
			<div class="demo-selector-label">
				Image 1
			</div>
		</a>
	</div>
	<div class="demo-selector">
		<a href="<?php echo esc_url('?demo_background=2&demo_layout=boxed'); ?>">
			<div class="demo-selector-icon demo-selector-background2 demo-active-<?php echo $demo_background; ?>">
				<i class="feather-icon-image"></i>
			</div>
			<div class="demo-selector-label">
				Image 2
			</div>
		</a>
	</div>
	<div class="demo-selector demo-selector-centered">
		<a href="<?php echo esc_url('?demo_background=3&demo_layout=boxed'); ?>">
			<div class="demo-selector-icon demo-selector-background3 demo-selector-center demo-active-<?php echo $demo_background; ?>">
				<i class="feather-icon-image"></i>
			</div>
			<div class="demo-selector-label">
				Image 3
			</div>
		</a>
	</div>
	<div class="clear"></div>
	</div>
	</form>
	
	</div>
</div>
<!-- 
**********************************************************************
DEMO Panel code - END
**********************************************************************
-->
