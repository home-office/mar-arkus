jQuery(document).ready(function(){

	"use strict";

// **********************************************************************
// DEMO Panel Selectors. Only used in Demo
// **********************************************************************
	
	var panelClose = jQuery('#demopanel .closedemo');
	var panelOpen = jQuery('#demopanel .opendemo');
	var panelWrap = jQuery('#demopanel');
	
	jQuery('#demopanel .closedemo').click(function() {
		 panelClose.css('display', 'none');
		 panelOpen.css('display', 'block');
		 panelWrap.stop().animate({ right: '-275'}, {duration: 'fast'});
	});
	jQuery('#demopanel .opendemo').click(function() {
		 panelClose.css('display', 'block');
		 panelOpen.css('display', 'none');
		 panelWrap.stop().animate({ right: '0'}, {duration: 'fast'});
	});
});