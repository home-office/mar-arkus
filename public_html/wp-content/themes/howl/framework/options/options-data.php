<?php
/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the "id" fields, make sure to use all lowercase and no spaces.
 *  
 */

function optionsframework_options() {
	
	$options_fonts = mtheme_google_fonts();
	
	// Pull all the categories into an array
	$options_categories = array(); 
	array_push($options_categories, "All Categories");
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
    	$options_categories[$category->cat_ID] = $category->cat_name;
	}
	
	// Pull all the pages into an array
	$options_pages = array();
	array_push($options_pages, "Not Selected"); 
	$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
	if ($options_pages_obj) {
		foreach ($options_pages_obj as $page) {
			$options_pages[$page->ID] = $page->post_title;
		}
	}
	
	// Pull all the Featured into an array
	$featured_pages = get_posts('post_type=mtheme_featured&orderby=title&numberposts=-1&order=ASC');
	if ($featured_pages) {
		foreach($featured_pages as $key => $list) {
			$custom = get_post_custom($list->ID);
			if ( isset($custom["fullscreen_type"][0]) ) { 
				$slideshow_type=' ('.$custom["fullscreen_type"][0].')'; 
			} else {
			$slideshow_type="";
			}
			$options_featured[$list->ID] = $list->post_title . $slideshow_type;
		}
	} else {
		$options_featured[0]="Featured pages not found.";
	}
	
	// Pull all the Featured into an array
	$bg_slideshow_pages = mtheme_get_select_target_options('fullscreen_slideshow_posts');
	
	// Pull all the Portfolio into an array
	$portfolio_pages = get_posts('post_type=mtheme_portfolio&orderby=title&numberposts=-1&order=ASC');
	if ($portfolio_pages) {
		foreach($portfolio_pages as $key => $list) {
			$custom = get_post_custom($list->ID);
			$portfolio_list[$list->ID] = $list->post_title;
		}
	}
		
	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/framework/options/images/';
	$theme_imagepath =  get_template_directory_uri() . '/images/';
	$predefined_background_imagepath =  get_template_directory_uri() . '/images/titlebackgrounds/';
		
	$options = array();
		
$options[] = array( "name"			=> __("General", "mthemelocal" ),
					"type"			=> "heading");
	$options[] = array( "name"			=> __( 'Fav icon file', 'mthemelocal' ),
						"desc"			=> __( "Customize with your fav icon. The fav icon is displayed in the browser window", 'mthemelocal' ),
						"id"			=> "general_fav_icon",
						"type"			=> "upload");

	$options[] = array( "name"			=> "Theme page layout",
						"desc"			=> "Fullwidth or Boxed",
						"id"			=> "general_theme_page",
						"std"			=> "fullwidth",
						"type"			=> "images",
						"options"	=> array(
							'fullwidth' => $imagepath . 'theme-fullwidth.png',
							'boxed' => $imagepath . 'boxed.png')
						);

		$options[] = array( "name"			=> __( "Menu Type", 'mthemelocal' ),
							"desc"			=> __( "Type of Menu", 'mthemelocal' ),
							"id"			=> "menu_type",
							"std"			=> "horizontal",
							"type"			=> "images",
							"options"	=> array(
								'horizontal' => $imagepath . 'horizontal-menu.png',
								'vertical' => $imagepath . 'vertical-menu.png')
							);

	$options[] = array( "name"			=> __("Google Map API Key. ( Required to display GMap by Google )","mthemelocal"),
						"desc"			=> __("Goole map now requires an API key to display google maps. How to get a <a target='_blank' href='https://developers.google.com/maps/documentation/javascript/get-api-key'>Google Map API Key</a>","mthemelocal"),
						"id"			=> "googlemap_apikey",
						"std"			=> "Google Map API Key",
						"class"			=> "tiny",
						"type"			=> "text");

		$options[] = array( "name"			=> __( "Mobile menu top bar set to white", 'mthemelocal' ),
							"desc"			=> __( "Mobile menu top bar seto to white.", 'mthemelocal' ),
							"id"			=> "mobile_menuonwhite",
							"std"			=> "0",
							"type"			=> "checkbox");

		$options[] = array( "name"			=> __( "Apply compact header to all pages", 'mthemelocal' ),
							"desc"			=> __( "Apply compact header to all pages.", 'mthemelocal' ),
							"id"			=> "force_compactheaders",
							"std"			=> "0",
							"type"			=> "checkbox");

		$options[] = array( "name"			=> __( "Set full caps off for titles and headings", 'mthemelocal' ),
							"desc"			=> __( "Set full caps off for titles and headings", 'mthemelocal' ),
							"id"			=> "force_fullcapsoff",
							"std"			=> "0",
							"type"			=> "checkbox");

		$options[] = array( "name"			=> __( "Add Facebook Opengraph Meta tags", 'mthemelocal' ),
							"desc"			=> __( "Adds meta tags to recognize featured image , title and decription used in posts and pages.", 'mthemelocal' ),
							"id"			=> "opengraph_status",
							"std"			=> "0",
							"type"			=> "checkbox");

		$options[] = array( "name"			=> __( "Disable toggle menu", 'mthemelocal' ),
							"desc"			=> __( "Disables toggle menu.", 'mthemelocal' ),
							"id"			=> "togglemenu_disable",
							"std"			=> "0",
							"type"			=> "checkbox");

		$options[] = array( "name"			=> __( "Disable header search", 'mthemelocal' ),
							"desc"			=> __( "Disable header search", 'mthemelocal' ),
							"id"			=> "headersearch_disable",
							"std"			=> "0",
							"type"			=> "checkbox");

$options[] = array( "name" => __("Custom CSS", "mthemelocal" ),
					"type" => "heading");

	$options[] = array( "name" => __("Custom CSS", "mthemelocal" ),
						"desc" => __("You can include custom CSS to this field. There's also a custom.css file included with the theme which you can make additions to. <br/> eg. <code>.entry-title h1 { font-family: 'Lobster', cursive; }</code>", "mthemelocal" ),
						"id" => "custom_css",
						"std" => '',
						"class" => "big",
						"type" => "textarea");

$options[] = array( "name"			=> __("Logo", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> __( "Bright Logo ( required )", 'mthemelocal' ),
						"desc"			=> __( "Upload logo for dark header", 'mthemelocal' ),
						"id"			=> "main_logo_bright",
						"type"			=> "upload");
					
	$options[] = array( "name"			=> __( "Dark Logo ( required )", 'mthemelocal' ),
						"desc"			=> __( "Upload logo for bright header.", 'mthemelocal' ),
						"id"			=> "main_logo_dark",
						"type"			=> "upload");

	$options[] = array( "name"			=> __( "Logo Width", 'mthemelocal' ),
						"desc"			=> __( "Logo width in pixels", 'mthemelocal' ),
						"id"			=> "logo_width",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "228",
						"type"			=> "text");
						
	$options[] = array( "name"			=> __( "Top Space", 'mthemelocal' ),
						"desc"			=> __( "Top spacing for logo ( 0 sets default )", 'mthemelocal' ),
						"id"			=> "logo_topmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

	$options[] = array( "name"			=> __( "Left Space", 'mthemelocal' ),
						"desc"			=> __( "Left spacing for logo ( 0 sets default )", 'mthemelocal' ),
						"id"			=> "logo_leftmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

	$options[] = array( "name"			=> __( "Responsive/Mobile Logo", 'mthemelocal' ),
						"desc"			=> __( "Upload logo for responsive layout.", 'mthemelocal' ),
						"id"			=> "responsive_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> __( "Responsive Logo Width", 'mthemelocal' ),
						"desc"			=> __( "Responsive Logo width in pixels", 'mthemelocal' ),
						"id"			=> "responsive_logo_width",
						"min"			=> "0",
						"max"			=> "2000",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");

	$options[] = array( "name"			=> __( "Responsive Logo Top Space", 'mthemelocal' ),
						"desc"			=> __( "Top spacing for logo ( 0 sets default )", 'mthemelocal' ),
						"id"			=> "responsive_logo_topmargin",
						"min"			=> "0",
						"max"			=> "200",
						"step"			=> "0",
						"unit"			=> 'pixels',
						"std"			=> "0",
						"type"			=> "text");
						
	$options[] = array( "name"			=> __( "Custom WordPress Login Page Logo", 'mthemelocal' ),
						"desc"			=> __( "Upload logo for WordPress Login Page", 'mthemelocal' ),
						"id"			=> "wplogin_logo",
						"type"			=> "upload");

$options[] = array( "name"			=> __("Preloader", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> __( "Preloader screen Logo ( required )", 'mthemelocal' ),
						"desc"			=> __( "Upload logo for preloader screen.", 'mthemelocal' ),
						"id"			=> "preloader_logo",
						"type"			=> "upload");

	$options[] = array( "name"			=> __("SVG code to replace as preloader", "mthemelocal" ),
						"desc"			=> __("Add code to replace preloader animation.", "mthemelocal" ),
						"id"			=> "custom_preloader_svg",
						"std"			=> '',
						"type"			=> "textarea");

		$options[] = array( "name"			=> __( "Change theme preloader color", "mthemelocal" ),
							"desc"			=> __( "Change theme preloader color", "mthemelocal" ),
							"id"			=> "preloader_color",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"			=> __("Background", "mthemelocal" ),
					"type"			=> "heading");	
						
	$options[] = array( "name"			=> __( "Background color", 'mthemelocal' ),
						"desc"			=> __( "No color selected by default.", 'mthemelocal' ),
						"id"			=> "general_background_color",
						"std"			=> "",
						"type"			=> "color");
						
	$options[] = array( "name"			=> __( "Background image", 'mthemelocal' ),
						"desc"			=> __( "Upload background image", 'mthemelocal' ),
						"id"			=> "general_background_image",
						"type"			=> "upload");


$options[] = array( "name"			=> __("Color", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> __( "Accent Color", "mthemelocal" ),
						"desc"			=> __( "Accent Color", "mthemelocal" ),
						"id"			=> "accent_color",
						"std"			=> "",
						"type"			=> "color");

	$options[] = array( "name"			=> __( "Title background default color", "mthemelocal" ),
						"desc"			=> __( "Title background default color", "mthemelocal" ),
						"id"			=> "title_backgroundcolor",
						"std"			=> "",
						"type"			=> "color");

$options[] = array( "name"			=> __("Horizontal Menu Color", "mthemelocal" ),
				"type"			=> "heading",
				"subheading"			=> 'header_section_order');

		$options[] = array( "name"			=> __( "Menu dropdown background color", "mthemelocal" ),
							"desc"			=> __( "Menu dropdown  background color", "mthemelocal" ),
							"id"			=> "menusubcat_bgcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Menu dropdown link color", "mthemelocal" ),
							"desc"			=> __( "Menu dropdown link color", "mthemelocal" ),
							"id"			=> "menusubcat_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Menu dropdown link hover color", "mthemelocal" ),
							"desc"			=> __( "Menu dropdown link hover color", "mthemelocal" ),
							"id"			=> "menusubcat_linkhovercolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Menu dropdown link underline color", "mthemelocal" ),
							"desc"			=> __( "Menu dropdown link underline color", "mthemelocal" ),
							"id"			=> "menusubcat_linkunderlinecolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Sticky Menu background color", "mthemelocal" ),
							"desc"			=> __( "Sticky Menu background color", "mthemelocal" ),
							"id"			=> "menu_stickymenu_bgcolor",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"			=> __("Vertical Menu Color", "mthemelocal" ),
				"type"			=> "heading",
				"subheading"			=> 'header_section_order');

		$options[] = array( "name"			=> __( "Menu background color", "mthemelocal" ),
							"desc"			=> __( "Menu background color", "mthemelocal" ),
							"id"			=> "vmenu_bg_color",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Menu item background color", "mthemelocal" ),
							"desc"			=> __( "Menu item background color", "mthemelocal" ),
							"id"			=> "vmenu_item_bgcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Item color", "mthemelocal" ),
							"desc"			=> __( "Item color", "mthemelocal" ),
							"id"			=> "vmenu_itemcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Opened item color", "mthemelocal" ),
							"desc"			=> __( "Opened item color", "mthemelocal" ),
							"id"			=> "vmenu_active_itemcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Item hover color", "mthemelocal" ),
							"desc"			=> __( "Item hover color", "mthemelocal" ),
							"id"			=> "vmenu_hover_itemcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Search icon color", "mthemelocal" ),
							"desc"			=> __( "Search icon color", "mthemelocal" ),
							"id"			=> "vmenu_search_itemcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Social icons color", "mthemelocal" ),
							"desc"			=> __( "Social icons color", "mthemelocal" ),
							"id"			=> "vmenu_social_itemcolor",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"				=> __("Page Color", "mthemelocal" ),
					"type"				=> "heading",
					"subheading"		=> 'header_section_order');

		$options[] = array( "name"			=> __( "Page background", "mthemelocal" ),
							"desc"			=> __( "Page background", "mthemelocal" ),
							"id"			=> "page_background",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Page contents color", "mthemelocal" ),
							"desc"			=> __( "Page contents color", "mthemelocal" ),
							"id"			=> "page_contentscolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Page contents heading color", "mthemelocal" ),
							"desc"			=> __( "Page contents heading color", "mthemelocal" ),
							"id"			=> "page_contentsheading",
							"std"			=> "",
							"type"			=> "color");

$options[] = array( "name"				=> __("Sidebar Color", "mthemelocal" ),
					"type"				=> "heading",
					"subheading"		=> 'header_section_order');

		$options[] = array( "name"			=> __( "Sidebar heading color", "mthemelocal" ),
							"desc"			=> __( "Sidebar heading color", "mthemelocal" ),
							"id"			=> "sidebar_headingcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Sidebar link color", "mthemelocal" ),
							"desc"			=> __( "Sidebar link color", "mthemelocal" ),
							"id"			=> "sidebar_linkcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Sidebar text color", "mthemelocal" ),
							"desc"			=> __( "Sidebar text color", "mthemelocal" ),
							"id"			=> "sidebar_textcolor",
							"std"			=> "",
							"type"			=> "color");

		$options[] = array( "name"			=> __( "Sidebar link underline", "mthemelocal" ),
							"desc"			=> __( "Sidebar link underline", "mthemelocal" ),
							"id"			=> "sidebar_linkbordercolor",
							"std"			=> "",
							"type"			=> "color");

	$options[] = array( "name" => "Footer Color",
					"type" => "heading",
					"subheading" => 'header_section_order');

		$options[] = array( "name" => "Footer background",
							"desc" => "Footer background",
							"id" => "footer_bgcolor",
							"std" => "",
							"type" => "color");

	$options[] = array( "name" => "Footer Label text color",
						"desc" => "Footer Label text color",
						"id" => "footer_labeltext",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer text color",
						"desc" => "Footer text color",
						"id" => "footer_text",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer text link color",
						"desc" => "Footer text link color",
						"id" => "footer_link",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer text link hover color",
						"desc" => "Footer text link hover color",
						"id" => "footer_linkhover",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer seperator line color",
						"desc" => "Footer seperator line color",
						"id" => "footer_hline",
						"std" => "",
						"type" => "color");

	$options[] = array( "name" => "Footer copyright background",
						"desc" => "Footer copyright background",
						"id" => "footer_copyrightbg",
						"std" => "",
						"type" => "color");

		$options[] = array( "name" => "Footer copyright label text",
							"desc" => "Footer copyright label text",
							"id" => "footer_copyrighttext",
							"std" => "",
							"type" => "color");

						
$options[] = array( "name"			=> __("Fonts", "mthemelocal" ),
					"type"			=> "heading");
					
$options[] = array( "name"			=> __("Enable Google Web Fonts", "mthemelocal" ),
					"desc"			=> __("Enable Google Web fonts", "mthemelocal" ),
					"id"			=> "default_googlewebfonts",
					"std"			=> "0",
					"type"			=> "checkbox");
						
	$options[] = array(	"name"			=> __("Menu Font", "mthemelocal" ),
						"desc"			=> __("Select menu font", "mthemelocal" ),
						"id"			=> "menu_font",
						"std"			=> '',
						"type"			=> "selectyper",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);
						
	$options[] = array(	"name"			=> __("Heading Font (applies to all headings)", "mthemelocal" ),
						"desc"			=> __("Select heading font", "mthemelocal" ),
						"id"			=> "heading_font",
						"std"			=> '',
						"type"			=> "selectyper",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);	
						
	$options[] = array(	"name"			=> __("Contents", "mthemelocal" ),
						"desc"			=> __("Select font for headings inside posts and pages", "mthemelocal" ),
						"id"			=> "page_contents",
						"std"			=> '',
						"type"			=> "selectyper",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_fonts);

$options[] = array( "name"			=> __("Custom Font", "mthemelocal" ),
					"type"			=> "heading",
					"subheading"			=> 'default_googlewebfonts');

	$options[] = array( "name"			=> __("Font Embed Code", "mthemelocal" ),
						"desc"			=> __("eg. <code>&lt;link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'&gt;</code>", "mthemelocal" ),
						"id"			=> "custom_font_embed",
						"std"			=> '',
						"type"			=> "textarea");

	$options[] = array( "name"			=> __("CSS Codes for Custom Font", "mthemelocal" ),
						"desc"			=> __("eg. <code>.entry-title h1 { font-family: 'Lobster', cursive; }</code>", "mthemelocal" ),
						"id"			=> "custom_font_css",
						"std"			=> '',
						"type"			=> "textarea");

$options[] = array( "name"			=> __("Portfolio Page", "mthemelocal" ),
					"type"			=> "heading");
					
	$options[] = array( "name"			=> __("Enable comments", "mthemelocal" ),
						"desc"			=> __("Enable comments for portfolio items. Switching off will disable comments and comment information on portfolio thumbnails.", "mthemelocal" ),
						"id"			=> "portfolio_comments",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> __( "Default portfolio archive list orientation", "mthemelocal" ),
						"desc"			=> __( "Default portfolio archive list orientation", 'mthemelocal' ),
						"id"			=> "portfolio_archive_format",
						"std"			=> "landscape",
						"type"			=> "select",
						"options"	=> array(
							'landscape'		=> 'Landscape',
							'portrait' 		=> 'Portrait')
						);

	$options[] = array(	"name"			=> __("Prefered portfolio archive page", "mthemelocal" ),
						"desc"			=> __("Prefered portfolio archive page.", "mthemelocal" ),
						"id"			=> "portfolio_archive_page",
						"std"			=> '',
						"type"			=> "select",
						"class"			=> "small", //mini, tiny, small
						"options"			=> $options_pages);

	$options[] = array( "name"			=> __("Number of thumbnail columns for Portfolio archive listing", "mthemelocal" ),
						"desc"			=> __("Affects portfolio archives. eg. Browsing portfolio category links.", "mthemelocal" ),
						"id"			=> "portfolio_achivelisting",
						"min"			=> "1",
						"max"			=> "4",
						"step"			=> "0",
						"unit"			=> 'columns',
						"std"			=> "4",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Enable Recent Portfolio blocks", "mthemelocal" ),
						"desc"			=> __("Enable recent portfolio block in portfolio details page.", "mthemelocal" ),
						"id"			=> "portfolio_recently",
						"std"			=> "1",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> __( "Display Related portfolio orientation", "mthemelocal" ),
						"desc"			=> __( "Display Related portfolio orientation", 'mthemelocal' ),
						"id"			=> "portfolio_related_format",
						"std"			=> "landscape",
						"type"			=> "select",
						"options"	=> array(
							'landscape'		=> 'Landscape',
							'portrait' 		=> 'Portrait')
						);

	$options[] = array( "name"			=> __("Portfolio permalink slug (Important Note below)","mthemelocal"),
						"desc"			=> __("Slug name used in portfolio permalink. <br/> IMPORTANT NOTE: After changing this please make sure to flush the old cache by visiting wp-admin > Settings > Permalinks","mthemelocal"),
						"id"			=> "portfolio_permalink_slug",
						"std"			=> "project",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Portfolio refered as ( Singular )","mthemelocal"),
						"desc"			=> __("Text name to refer portfolio as a singular ( one item )","mthemelocal"),
						"id"			=> "portfolio_singular_refer",
						"std"			=> "Project",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Portfolio refered as ( Plural )","mthemelocal"),
						"desc"			=> __("Text name to refer portfolio as plural ( many items )","mthemelocal"),
						"id"			=> "portfolio_plural_refer",
						"std"			=> "Projects",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Portfolio project link text","mthemelocal"),
						"desc"			=> __("Portfolio project link text","mthemelocal"),
						"id"			=> "portfolio_link_text",
						"std"			=> "Project Link",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Refer to Client as","mthemelocal"),
						"desc"			=> __("Refer to Client as. Seen in Portfolio details pages","mthemelocal"),
						"id"			=> "portfolio_client_refer",
						"std"			=> "Client",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Refer to Skills as","mthemelocal"),
						"desc"			=> __("Refer to Skills as. Seen in Portfolio details pages","mthemelocal"),
						"id"			=> "portfolio_skill_refer",
						"std"			=> "Skills",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Filter tag for all Items","mthemelocal"),
						"desc"			=> __("Displays as a filterable tag in place of all items","mthemelocal"),
						"id"			=> "portfolio_allitems",
						"std"			=> "All Projects",
						"class"			=> "tiny",
						"type"			=> "text");

$options[] = array( "name"			=> __("Gallery Page", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> __("Gallery refered as ( Singular )","mthemelocal"),
						"desc"			=> __("Text name to refer portfolio as a singular ( one item )","mthemelocal"),
						"id"			=> "gallery_singular_refer",
						"std"			=> "Gallery",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Gallery permalink slug (Important Note below)","mthemelocal"),
						"desc"			=> __("Slug name used in gallery permalink. <br/> IMPORTANT NOTE: After changing this please make sure to flush the old cache by visiting wp-admin > Settings > Permalinks","mthemelocal"),
						"id"			=> "gallery_permalink_slug",
						"std"			=> "gallery",
						"class"			=> "tiny",
						"type"			=> "text");

	$options[] = array( "name"			=> __( "Display gallery archive dimension format", "mthemelocal" ),
						"desc"			=> __( "Display gallery archive dimension format", 'mthemelocal' ),
						"id"			=> "gallery_archive_format",
						"std"			=> "square",
						"type"			=> "select",
						"options"	=> array(
							'square'		=> 'Square',
							'landscape'		=> 'Landscape',
							'portrait' 		=> 'Portrait')
						);

	$options[] = array( "name"			=> __("Number of thumbnail columns for Gallery archive listing", "mthemelocal" ),
						"desc"			=> __("Affects gallery archives. eg. Browsing portfolio category links.", "mthemelocal" ),
						"id"			=> "gallery_achivelisting",
						"min"			=> "3",
						"max"			=> "4",
						"step"			=> "0",
						"unit"			=> 'columns',
						"std"			=> "4",
						"type"			=> "text");

	$options[] = array( "name"			=> __("Enable gallery image direct linking", "mthemelocal" ),
						"desc"			=> __("Enable gallery image direct linking.", "mthemelocal" ),
						"id"			=> "gallery_image_linking",
						"std"			=> "1",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> __("Enable comments", "mthemelocal" ),
						"desc"			=> __("Enable comments for gallery items. Switching off will disable comments.", "mthemelocal" ),
						"id"			=> "gallery_comments",
						"std"			=> "0",
						"type"			=> "checkbox");
						
$options[] = array( "name"			=> __("Blog", "mthemelocal" ),
					"type"			=> "heading");
					
	$options[] = array( "name"			=> __("Display Fullpost Archives", "mthemelocal" ),
						"desc"			=> __("Display fullpost archives", "mthemelocal" ),
						"id"			=> "postformat_fullcontent",
						"std"			=> "0",
						"type"			=> "checkbox");
						
	$options[] = array( "name"			=> __("Hide allowed HTML tags info", "mthemelocal" ),
						"desc"			=> __("Hide allowed HTML tags info after comments box", "mthemelocal" ),
						"id"			=> "blog_allowedtags",
						"std"			=> "0",
						"type"			=> "checkbox");

	$options[] = array( "name"			=> __("Read more text", "mthemelocal" ),
						"desc"			=> __("Enter text for Read more", "mthemelocal" ),
						"id"			=> "read_more",
						"std"			=> "Continue reading",
						"class"			=> "small",
						"type"			=> "text");

$options[] = array( "name"			=> __("Sidebars", "mthemelocal" ),
					"type"			=> "heading");

				
	for ($sidebar_count=1; $sidebar_count <=MTHEME_MAX_SIDEBARS; $sidebar_count++ ) {

	$options[] = array( "name"			=> __("Sidebar ", "mthemelocal" ) . $sidebar_count,
							"type"			=> "info");
						
		$options[] = array( "name"			=> __("Sidebar Name", "mthemelocal" ),
						"desc"			=> __("Activate sidebars by naming them.", "mthemelocal" ),
						"id"			=> "theme_sidebar".$sidebar_count,
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");

		$options[] = array( "name"			=> __("Sidebar Description", "mthemelocal" ),
						"desc"			=> __("A small description to display inside the widget to easily identify it. Widget description is only shown in admin mode inside the widget.", "mthemelocal" ),
						"id"			=> "theme_sidebardesc".$sidebar_count,
						"std"			=> "",
						"class"			=> "small",
						"type"			=> "text");
	}

$options[] = array( "name"			=> __("WPML", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> __( "Disable Built-in WPML language selector", 'mthemelocal' ),
						"desc"			=> __( "Disable Built-in WPML language selector", 'mthemelocal' ),
						"id"			=> "wpml_lang_selector_disable",
						"std"			=> "0",
						"type"			=> "checkbox");

$options[] = array( "name"			=> __("WooCommerce", "mthemelocal" ),
					"type"			=> "heading");

		$options[] = array( "name"			=> __("WooCommerce Shop default title", "mthemelocal" ),
						"desc"			=> __("Shop title for WooCommerce shop. ( default 'Shop' ).", "mthemelocal" ),
						"id"			=> "mtheme_woocommerce_shoptitle",
						"std"			=> "Shop",
						"class"			=> "small",
						"type"			=> "text");
						
$options[] = array( "name"			=> __("Footer", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> "Widgetized Footer",
						"desc"			=> "Display Widgetized Footer",
						"id"			=> "footerwidget_status",
						"std"			=> "1",
						"type"			=> "checkbox");
					
	$options[] = array( "name"			=> __("Copyright text", "mthemelocal" ),
						"desc"			=> __("Enter your copyright and other texts to display in footer", "mthemelocal" ),
						"id"			=> "footer_copyright",
						"std"			=> "Copyright &copy; [display_current_year]",
						"type"			=> "textarea");

$options[] = array( "name"			=> __("Export", "mthemelocal" ),
					"type"			=> "heading");

	$options[] = array( "name"			=> __("Export Options ( Copy this ) Read-Only.", "mthemelocal" ),
						"desc"			=> __("Select All, copy and store your theme options backup. You can use these value to import theme options settings.", "mthemelocal" ),
						"id"			=> "exportpack",
						"std"			=> '',
						"class"			=> "big",
						"type"			=> "exporttextarea");

$options[] = array( "name"			=> __("Import Options", "mthemelocal" ),
					"type"			=> "heading",
					"subheading"		=> 'exportpack');

	$options[] = array( "name"			=> __("Import Options ( Paste and Save )", "mthemelocal" ),
						"desc"			=> __("CAUTION: Copy and Paste the Export Options settings into the window and Save to apply theme options settings.", "mthemelocal" ),
						"id"			=> "importpack",
						"std"			=> '',
						"class"			=> "big",
						"type"			=> "importtextarea");

	return $options;
}

?>