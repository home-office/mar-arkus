<?php
//$prefix = 'fables_';

/*
$meta_box = array(
	'id' => 'my-meta-box',
	'title' => 'Custom meta box',
	'page' => 'page',
	'context' => 'normal',
	'priority' => 'high',
	'fields' => array(
		array(
			'name' => 'Text box',
			'desc' => 'Enter something here',
			'id' => $prefix . 'text',
			'type' => 'text',
			'std' => 'Default value 1'
		),
		array(
			'name' => 'Textarea',
			'desc' => 'Enter big text here',
			'id' => $prefix . 'textarea',
			'type' => 'textarea',
			'std' => 'Default value 2'
		),
		array(
			'name' => 'Select box',
			'id' => $prefix . 'select',
			'type' => 'select',
			'options' => array('Option 1', 'Option 2', 'Option 3')
		),
		array(
			'name' => 'Select box category',
			'id' => $prefix . 'select',
			'desc' => 'Enter big text here',
			'type' => 'select',
			'options' => mtheme_get_select_target_options('portfolio_category')
		),
		array(
			'name' => 'Radio',
			'id' => $prefix . 'radio',
			'desc' => 'Enter big text here',
			'type' => 'radio',
			'options' => array(
				array('name' => 'Name 1', 'value' => 'Value 1'),
				array('name' => 'Name 2', 'value' => 'Value 2')
			)
		)
	)
);
*/

global $mtheme_meta_box,$mtheme_common_page_box,$mtheme_active_metabox;

$mtheme_active_metabox="page";
$mtheme_sidebar_options = mtheme_generate_sidebarlist("page");

$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';

$mtheme_common_page_box = array(
	'id' => 'common-pagemeta-box',
	'title' => 'General Page Metabox',
	'page' => 'page',
	'context' => 'normal',
	'priority' => 'core',
	'fields' => array(
		array(
			'name' => __('Page Settings','mthemelocal'),
			'id' => MTHEME . '_page_section_id',
			'type' => 'break',
			'sectiontitle' => __('Page Settings','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Attach Images','mthemelocal'),
			'id' => MTHEME . '_image_attachments',
			'std' => __('Upload Images','mthemelocal'),
			'type' => 'image_gallery',
			'desc' => __('<div class="metabox-note">Attach images to this page/post.</div>','mthemelocal')
		),
		array(
			'name' => __('Revolution Slider','mthemelocal'),
			'id' => MTHEME . '_revslider',
			'type' => 'select',
			'desc' => __('Display Revolution Slider','mthemelocal'),
			'options' => mtheme_rev_slider_selectors()
		),
		array(
			'name' => __('Page Style','mthemelocal'),
			'id' => MTHEME . '_pagestyle',
			'type' => 'image',
			'std' => 'rightsidebar',
			'desc' => __('<strong>With Sidebar :</strong> Displays post with sidebar - two columns</br><strong>Fullwidth without sidebar :</strong> Displays post as without sidebar','mthemelocal'),
			'options' => array(
				'rightsidebar' => $mtheme_imagepath . 'page_rightsidebar.png',
				'leftsidebar' => $mtheme_imagepath . 'page_leftsidebar.png',
				'nosidebar' => $mtheme_imagepath . 'page_nosidebar.png',
				'edge-to-edge' => $mtheme_imagepath . 'page_edgetoedge.png')
		),
		array(
			'name' => __('Choice of Sidebar','mthemelocal'),
			'id' => MTHEME . '_sidebar_choice',
			'type' => 'select',
			'desc' => __('For Sidebar Active Pages and Posts','mthemelocal'),
			'options' => $mtheme_sidebar_options
		),
		array(
			'name' => __('Header Settings','mthemelocal'),
			'id' => MTHEME . '_header_section_id',
			'type' => 'break',
			'sectiontitle' => __('Header Settings','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Header style','mthemelocal'),
			'id' => MTHEME . '_pageheader_style',
			'type' => 'image',
			'std' => 'compact-on-bright',
			'desc' => __('Header style','mthemelocal'),
			'options' => array(
				'compact-on-bright'	=> $mtheme_imagepath . 'header_compact_bright.png',
				'compact-on-dark' => $mtheme_imagepath . 'header_compact_dark.png',
				'header-bright'	=> $mtheme_imagepath . 'header_transparent_bright.png',
				'header-dark' => $mtheme_imagepath . 'header_transparent_dark.png',
				'header-bright-on-overlay' => $mtheme_imagepath . 'header_overlay_bright.png',
				'header-dark-on-overlay' => $mtheme_imagepath . 'header_overlay_dark.png',
				'header-bright-logo-only' => $mtheme_imagepath . 'header_logo_only_bright.png',
				'header-dark-logo-only' => $mtheme_imagepath . 'header_logo_only_dark.png'
			)
		),
		array(
			'name' => __('Header image','mthemelocal'),
			'heading' => 'subhead',
			'id' => MTHEME . '_header_image',
			'type' => 'upload',
			'target' => 'image',
			'std' => '',
			'desc' => __('Header image','mthemelocal')
		),
		array(
			'name' => __('Header background color','mthemelocal'),
			'heading' => 'subhead',
			'id' => MTHEME . '_pageheader_color',
			'type' => 'color',
			'desc' => __('Display Page header color','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Header top spacing ( default 0 )','mthemelocal'),
			'heading' => 'subhead',
			'id' => MTHEME . '_pagetitle_top_spacing',
			'type' => 'text',
			'desc' => __('Header top spacing','mthemelocal'),
			'std' => '0'
		),
		array(
			'name' => __('Header bottom spacing ( default 0 )','mthemelocal'),
			'heading' => 'subhead',
			'id' => MTHEME . '_pagetitle_bottom_spacing',
			'type' => 'text',
			'desc' => __('Header bottom spacing','mthemelocal'),
			'std' => '0'
		),
		array(
			'name' => __('Display Page title','mthemelocal'),
			'id' => MTHEME . '_pagetitle_header',
			'type' => 'image',
			'desc' => __('Display Page title','mthemelocal'),
			'options' => array(
				'display' => $mtheme_imagepath . 'title_display.png',
				'hide' => $mtheme_imagepath . 'title_hide.png',
			)
		),
		array(
			'name' => 'Title color',
			'heading' => 'subhead',
			'id' => MTHEME . '_pagetitle_color',
			'type' => 'color',
			'desc' => 'Display Page title color',
			'std' => ''
		),
		array(
			'name' => __('Title style','mthemelocal'),
			'heading' => 'subhead',
			'id' => MTHEME . '_pagetitle_style',
			'type' => 'image',
			'std' => 'default',
			'desc' => __('Title style','mthemelocal'),
			'options' => array(
				'default' => $mtheme_imagepath . 'title_default_size.png',
				'bold-centered'	=> $mtheme_imagepath . 'title_big_size.png',
			)
		)
	)
);

// Add meta box
function mtheme_add_box() {
	global $mtheme_meta_box,$mtheme_common_page_box;
	add_meta_box($mtheme_common_page_box['id'], $mtheme_common_page_box['title'], 'mtheme_common_show_pagebox', $mtheme_common_page_box['page'], $mtheme_common_page_box['context'], $mtheme_common_page_box['priority']);
}

function mtheme_common_show_pagebox() {
	global $mtheme_common_page_box, $post;
	mtheme_generate_metaboxes($mtheme_common_page_box,$post);
}
?>