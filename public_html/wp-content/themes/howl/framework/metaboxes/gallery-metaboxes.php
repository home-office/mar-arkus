<?php
global $mtheme_meta_box,$mtheme_gallery_box,$mtheme_active_metabox;

$mtheme_active_metabox="gallery";
$mtheme_sidebar_options = mtheme_generate_sidebarlist("gallery");

// Pull all the Featured into an array
$bg_slideshow_pages = get_posts('post_type=mtheme_featured&orderby=title&numberposts=-1&order=ASC');

if ($bg_slideshow_pages) {
	$options_bgslideshow['none'] = "Not Selected";
	foreach($bg_slideshow_pages as $key => $list) {
		$custom = get_post_custom($list->ID);
		if ( isset($custom["fullscreen_type"][0]) ) { 
			$slideshow_type=$custom["fullscreen_type"][0]; 
		} else {
		$slideshow_type="";
		}
		if ($slideshow_type<>"Fullscreen-Video") {
			$options_bgslideshow[$list->ID] = $list->post_title;
		}
	}
} else {
	$options_bgslideshow[0]="Featured pages not found.";
}

$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';
$mtheme_imagepath_alt =  get_template_directory_uri() . '/framework/options/images/';

$mtheme_gallery_box = array(
	'id' => 'gallerymeta-box',
	'title' => 'Gallery Metabox',
	'page' => 'page',
	'context' => 'normal',
	'priority' => 'core',
	'fields' => array(
		array(
			'name' => __('Gallery Settings','mthemelocal'),
			'id' => MTHEME . '_gallery_section_id',
			'type' => 'break',
			'sectiontitle' => __('Gallery Settings','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Attach Images','mthemelocal'),
			'id' => MTHEME . '_image_attachments',
			'std' => __('Upload Images','mthemelocal'),
			'type' => 'image_gallery',
			'desc' => __('<div class="metabox-note">Attach images to this page/post.</div>','mthemelocal')
		),
		array(
			'name' => __('Gallery type','mthemelocal'),
			'id' => MTHEME . '_gallerytype',
			'std' => 'grid',
			'type' => 'image',
			'triggerStatus'=> 'on',
			'toggleClass' => '.videoembed',
			'toggleAction' => 'show',
			'toggleTrigger' => 'Video',
			'class'=>'gallery_header',
			'desc' => __('Select type of Gallery.','mthemelocal'),
			'options' => array(
				'grid' => $mtheme_imagepath_alt . 'portfolio_grid.png',
				'vertical' => $mtheme_imagepath_alt . 'portfolio_vertical.png',
				'wall' => $mtheme_imagepath_alt . 'portfolio_wall.png',
				'masonary' => $mtheme_imagepath_alt . 'portfolio_masonary.png',
				'fullscreen' => $mtheme_imagepath_alt . 'portfolio_fullscreen.png',
				'fullscreenfit' => $mtheme_imagepath_alt . 'portfolio_fullscreen_fit.png',
				'None' => $mtheme_imagepath_alt . 'portfolio_none.png'
				)
		),
		array(
			'name' => __('Page Style','mthemelocal'),
			'id' => MTHEME . '_pagestyle',
			'type' => 'image',
			'std' => 'fullwidth',
			'desc' => __('<strong>With Sidebar :</strong> Displays post with sidebar - two columns</br><strong>Fullwidth without sidebar :</strong> Displays post as without sidebar','mthemelocal'),
			'options' => array(
				'fullwidth' => $mtheme_imagepath . 'page_nosidebar.png',
				'rightsidebar' => $mtheme_imagepath . 'page_rightsidebar.png',
				'leftsidebar' => $mtheme_imagepath . 'page_leftsidebar.png',
				'edge-to-edge' => $mtheme_imagepath . 'page_edgetoedge.png')
		),
		array(
			'name' => __('Header Settings','mthemelocal'),
			'id' => MTHEME . '_header_section_id',
			'type' => 'break',
			'sectiontitle' => __('Header Settings','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Header style','mthemelocal'),
			'id' => MTHEME . '_pageheader_style',
			'type' => 'image',
			'std' => 'compact-on-bright',
			'desc' => __('Header style','mthemelocal'),
			'options' => array(
				'compact-on-bright'	=> $mtheme_imagepath . 'header_compact_bright.png',
				'compact-on-dark' => $mtheme_imagepath . 'header_compact_dark.png',
				'header-bright'	=> $mtheme_imagepath . 'header_transparent_bright.png',
				'header-dark' => $mtheme_imagepath . 'header_transparent_dark.png',
				'header-bright-on-overlay' => $mtheme_imagepath . 'header_overlay_bright.png',
				'header-dark-on-overlay' => $mtheme_imagepath . 'header_overlay_dark.png',
				'header-bright-logo-only' => $mtheme_imagepath . 'header_logo_only_bright.png',
				'header-dark-logo-only' => $mtheme_imagepath . 'header_logo_only_dark.png'
			)
		),
		array(
			'name' => __('Title style','mthemelocal'),
			'id' => MTHEME . '_pagetitle_style',
			'type' => 'image',
			'std' => 'default',
			'desc' => __('Title style','mthemelocal'),
			'options' => array(
				'default' => $mtheme_imagepath . 'title_default_size.png',
				'bold-centered'	=> $mtheme_imagepath . 'title_big_size.png',
			)
		),
		array(
			'name' => __('Display Page title','mthemelocal'),
			'id' => MTHEME . '_pagetitle_header',
			'type' => 'image',
			'desc' => __('Display Page title','mthemelocal'),
			'options' => array(
				'display' => $mtheme_imagepath . 'title_display.png',
				'hide' => $mtheme_imagepath . 'title_hide.png',
			)
		),
		array(
			'name' => __('Header image','mthemelocal'),
			'id' => MTHEME . '_header_image',
			'type' => 'upload',
			'target' => 'image',
			'std' => '',
			'desc' => __('<div class="metabox-note">Please provide full url of background. eg. <code>http://www.domain.com/path/image.jpg</code></div>','mthemelocal')
		),
		array(
			'name' => __('Header background color','mthemelocal'),
			'id' => MTHEME . '_pageheader_color',
			'type' => 'color',
			'desc' => __('Display Page header color','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => 'Title color',
			'id' => MTHEME . '_pagetitle_color',
			'type' => 'color',
			'desc' => 'Display Page title color',
			'std' => ''
		),
		array(
			'name' => __('Header top spacing','mthemelocal'),
			'id' => MTHEME . '_pagetitle_top_spacing',
			'type' => 'text',
			'desc' => __('Header top spacing','mthemelocal'),
			'std' => '0'
		),
		array(
			'name' => __('Header bottom spacing','mthemelocal'),
			'id' => MTHEME . '_pagetitle_bottom_spacing',
			'type' => 'text',
			'desc' => __('Header bottom spacing','mthemelocal'),
			'std' => '0'
		)
	)
);
add_action("admin_init", "mtheme_galleryitemmetabox_init");
function mtheme_galleryitemmetabox_init(){
	add_meta_box("mtheme_galleryInfo-meta", "Gallery Options", "mtheme_galleryitem_metaoptions", "mtheme_gallery", "normal", "low");
}
/*
* Meta options for Gallery post type
*/
function mtheme_galleryitem_metaoptions(){
	global $mtheme_gallery_box, $post;
	mtheme_generate_metaboxes($mtheme_gallery_box,$post);
}
?>