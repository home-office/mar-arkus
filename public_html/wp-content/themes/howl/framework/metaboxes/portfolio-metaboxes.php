<?php
global $mtheme_meta_box,$mtheme_portfolio_box,$mtheme_active_metabox;

$mtheme_active_metabox="portfolio";
$mtheme_sidebar_options = mtheme_generate_sidebarlist("portfolio");

// Pull all the Featured into an array
$bg_slideshow_pages = get_posts('post_type=mtheme_featured&orderby=title&numberposts=-1&order=ASC');

if ($bg_slideshow_pages) {
	$options_bgslideshow['none'] = "Not Selected";
	foreach($bg_slideshow_pages as $key => $list) {
		$custom = get_post_custom($list->ID);
		if ( isset($custom["fullscreen_type"][0]) ) { 
			$slideshow_type=$custom["fullscreen_type"][0]; 
		} else {
		$slideshow_type="";
		}
		if ($slideshow_type<>"Fullscreen-Video") {
			$options_bgslideshow[$list->ID] = $list->post_title;
		}
	}
} else {
	$options_bgslideshow[0]="Featured pages not found.";
}

$mtheme_imagepath =  get_template_directory_uri() . '/framework/options/images/metaboxes/';
$mtheme_imagepath_alt =  get_template_directory_uri() . '/framework/options/images/';

$mtheme_portfolio_box = array(
	'id' => 'portfoliometa-box',
	'title' => 'Portfolio Metabox',
	'page' => 'page',
	'context' => 'normal',
	'priority' => 'core',
	'fields' => array(
		array(
			'name' => __('Portfolio Settings','mthemelocal'),
			'id' => MTHEME . '_portfolio_section_id',
			'type' => 'break',
			'sectiontitle' => __('Portfolio Settings','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Attach Images','mthemelocal'),
			'id' => MTHEME . '_image_attachments',
			'std' => __('Upload Images','mthemelocal'),
			'type' => 'image_gallery',
			'desc' => __('<div class="metabox-note">Attach images to this page/post.</div>','mthemelocal')
		),
		array(
			'name' => __('Portfolio type','mthemelocal'),
			'id' => MTHEME . '_portfoliotype',
			'std' => 'Image',
			'type' => 'image',
			'triggerStatus'=> 'on',
			'toggleClass' => '.videoembed',
			'toggleAction' => 'show',
			'toggleTrigger' => 'Video',
			'class'=>'portfolio_header',
			'desc' => __('Select type of Portfolio.','mthemelocal'),
			'options' => array(
				'Image' => $mtheme_imagepath_alt . 'portfolio_image.png',
				'Metro' => $mtheme_imagepath_alt . 'portfolio_metro.png',
				'beforeafter' => $mtheme_imagepath_alt . 'portfolio_beforeafter.png',
				'Vertical' => $mtheme_imagepath_alt . 'portfolio_vertical.png',
				'Slideshow' => $mtheme_imagepath_alt . 'portfolio_slideshow.png',
				'Video' => $mtheme_imagepath_alt . 'portfolio_video.png',
				'None' => $mtheme_imagepath_alt . 'portfolio_none.png'
				)
		),
		array(
			'name' => __('Page Style','mthemelocal'),
			'id' => MTHEME . '_pagestyle',
			'type' => 'image',
			'std' => 'rightsidebar',
			'desc' => __('<strong>With Sidebar :</strong> Displays post with sidebar - two columns</br><strong>Fullwidth without sidebar :</strong> Displays post as without sidebar','mthemelocal'),
			'options' => array(
				'portfolio_default' => $mtheme_imagepath . 'portfolio_default.png',
				'fullwidth' => $mtheme_imagepath . 'page_nosidebar.png',
				'rightsidebar' => $mtheme_imagepath . 'page_rightsidebar.png',
				'leftsidebar' => $mtheme_imagepath . 'page_leftsidebar.png',
				'edge-to-edge' => $mtheme_imagepath . 'page_edgetoedge.png')
		),
		array(
			'name' => __('Description for gallery thumbnail ( Portfolio Gallery )','mthemelocal'),
			'id' => MTHEME . '_thumbnail_desc',
			'type' => 'textarea',
			'desc' => __('This description is displayed below each thumbnail.','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Portfolio AJAX description','mthemelocal'),
			'id' => MTHEME . '_ajax_description',
			'heading' => 'subhead',
			'type' => 'textarea',
			'desc' => __('Used for Ajax item description.','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Choice of Sidebar','mthemelocal'),
			'id' => MTHEME . '_sidebar_choice',
			'class' => 'sidebar_choice',
			'type' => 'select',
			'desc' => __('For Sidebar Active Pages and Posts','mthemelocal'),
			'options' => $mtheme_sidebar_options
		),
		array(
			'name' => __('Video Embed Code','mthemelocal'),
			'id' => MTHEME . '_video_embed',
			'heading' => 'subhead',
			'class'=> 'videoembed',
			'type' => 'textarea',
			'desc' => __('Video Embed code for Video Portfolio.','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Gallery thumbnail link type','mthemelocal'),
			'id' => MTHEME . '_thumbnail_linktype',
			'type' => 'image',
			'std' => 'Lightbox',
			'triggerStatus'=> 'on',
			'toggleClass' => '.portfoliolinktype',
			'toggleAction' => 'hide',
			'toggleTrigger' => 'meta_thumbnail_direct',
			'class'=>'thumbnail_linktype',
			'desc' => __('Link type of portfolio image in portfolio galleries.','mthemelocal'),
			'options' => array(
				'Lightbox_DirectURL' => $mtheme_imagepath_alt . 'thumb_lightbox_directlink.png',
				'Lightbox' => $mtheme_imagepath_alt . 'thumb_lightbox.png',
				'Customlink' => $mtheme_imagepath_alt . 'thumb_customlink.png',
				'DirectURL' => $mtheme_imagepath_alt . 'thumb_directlink.png'
				)
		),
		array(
			'name' => __('Fill for Lightbox Video','mthemelocal'),
			'id' => MTHEME . '_lightbox_video',
			'heading' => 'subhead',
			'class'=> 'portfoliolinktype',
			'type' => 'text',
			'desc' => __('To display a Lightbox Video.<br/>Eg.<br/>http://www.youtube.com/watch?v=D78TYCEG4<br/>http://vimeo.com/172881','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Fill for Custom Link','mthemelocal'),
			'id' => MTHEME . '_customlink',
			'heading' => 'subhead',
			'class'=> 'portfoliolinktype',
			'type' => 'text',
			'desc' => __('For any link. URL followed with <code>http://</code>','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Skills Required (optional)','mthemelocal'),
			'id' => MTHEME . '_skills_required',
			'heading' => 'subhead',
			'type' => 'text',
			'desc' => __('Comma seperated skills sets. eg. PHP,HTML,CSS,Illustrator,Photoshop','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Client Name (optional)','mthemelocal'),
			'id' => MTHEME . '_clientname',
			'type' => 'text',
			'heading' => 'subhead',
			'desc' => __('Name of Client','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Client Link (optional)','mthemelocal'),
			'id' => MTHEME . '_clientname_link',
			'type' => 'text',
			'heading' => 'subhead',
			'desc' => __('URL of Client','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Project Link (optional)','mthemelocal'),
			'id' => MTHEME . '_projectlink',
			'type' => 'text',
			'heading' => 'subhead',
			'desc' => __('Project link. URL followed with <code>http://</code>','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Custom Thumbnail. (optional)','mthemelocal'),
			'id' => MTHEME . '_customthumbnail',
			'type' => 'upload',
			'target' => 'image',
			'desc' => __('Thumbnail URL. URL followed with <code>http://</code>','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Header Settings','mthemelocal'),
			'id' => MTHEME . '_header_section_id',
			'type' => 'break',
			'sectiontitle' => __('Header Settings','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => __('Header style','mthemelocal'),
			'id' => MTHEME . '_pageheader_style',
			'type' => 'image',
			'std' => 'compact-on-bright',
			'desc' => __('Header style','mthemelocal'),
			'options' => array(
				'compact-on-bright'	=> $mtheme_imagepath . 'header_compact_bright.png',
				'compact-on-dark' => $mtheme_imagepath . 'header_compact_dark.png',
				'header-bright'	=> $mtheme_imagepath . 'header_transparent_bright.png',
				'header-dark' => $mtheme_imagepath . 'header_transparent_dark.png',
				'header-bright-on-overlay' => $mtheme_imagepath . 'header_overlay_bright.png',
				'header-dark-on-overlay' => $mtheme_imagepath . 'header_overlay_dark.png',
				'header-bright-logo-only' => $mtheme_imagepath . 'header_logo_only_bright.png',
				'header-dark-logo-only' => $mtheme_imagepath . 'header_logo_only_dark.png'
			)
		),
		array(
			'name' => __('Title style','mthemelocal'),
			'id' => MTHEME . '_pagetitle_style',
			'type' => 'image',
			'std' => 'default',
			'desc' => __('Title style','mthemelocal'),
			'options' => array(
				'default' => $mtheme_imagepath . 'title_default_size.png',
				'bold-centered'	=> $mtheme_imagepath . 'title_big_size.png',
			)
		),
		array(
			'name' => __('Display Page title','mthemelocal'),
			'id' => MTHEME . '_pagetitle_header',
			'type' => 'image',
			'desc' => __('Display Page title','mthemelocal'),
			'options' => array(
				'display' => $mtheme_imagepath . 'title_display.png',
				'hide' => $mtheme_imagepath . 'title_hide.png',
			)
		),
		array(
			'name' => __('Header image','mthemelocal'),
			'id' => MTHEME . '_header_image',
			'type' => 'upload',
			'target' => 'image',
			'std' => '',
			'desc' => __('<div class="metabox-note">Please provide full url of background. eg. <code>http://www.domain.com/path/image.jpg</code></div>','mthemelocal')
		),
		array(
			'name' => __('Header background color','mthemelocal'),
			'id' => MTHEME . '_pageheader_color',
			'type' => 'color',
			'desc' => __('Display Page header color','mthemelocal'),
			'std' => ''
		),
		array(
			'name' => 'Title color',
			'id' => MTHEME . '_pagetitle_color',
			'type' => 'color',
			'desc' => 'Display Page title color',
			'std' => ''
		),
		array(
			'name' => __('Header top spacing','mthemelocal'),
			'id' => MTHEME . '_pagetitle_top_spacing',
			'type' => 'text',
			'desc' => __('Header top spacing','mthemelocal'),
			'std' => '0'
		),
		array(
			'name' => __('Header bottom spacing','mthemelocal'),
			'id' => MTHEME . '_pagetitle_bottom_spacing',
			'type' => 'text',
			'desc' => __('Header bottom spacing','mthemelocal'),
			'std' => '0'
		)
	)
);
add_action("admin_init", "mtheme_portfolioitemmetabox_init");
function mtheme_portfolioitemmetabox_init(){
	add_meta_box("mtheme_portfolioInfo-meta", "Portfolio Options", "mtheme_portfolioitem_metaoptions", "mtheme_portfolio", "normal", "low");
}
/*
* Meta options for Portfolio post type
*/
function mtheme_portfolioitem_metaoptions(){
	global $mtheme_portfolio_box, $post;
	mtheme_generate_metaboxes($mtheme_portfolio_box,$post);
}
?>