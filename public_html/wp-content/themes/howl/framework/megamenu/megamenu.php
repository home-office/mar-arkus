<?php
class mtheme_Menu_Megamenu extends Walker_Nav_Menu  {
    /**
        * @see Walker::$tree_type
        * @since 3.0.0
        * @var string
        */
    var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
    
    var $choice_of_menu = "none";
    var $textbox_active = false;
    var $megamenu_active = false;
    var $menu_row_counter = 0;

    /**
        * @see Walker::$db_fields
        * @since 3.0.0
        * @todo Decouple this.
        * @var array
        */
    var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

    /**
        * @see Walker::start_lvl()
        * @since 3.0.0
        *
        */
    function start_lvl(&$output, $depth = 0, $args = array()) {
    global $wp_query;
        $indent = str_repeat("\t", $depth);
        $element = 'ul'; $widget_class = '';
        $this->menu_row_counter = 0;
        if($depth==0&&$this->megamenu_active) {
            $element = 'ul';
        }
        
        $output .= "\n$indent<{$element} class=\"children children-depth-".$depth." clearfix  $widget_class \">\n";
    }

    /**
        * @see Walker::end_lvl()
        * @since 3.0.0
        *
        * @param string $output Passed by reference. Used to append additional content.
        * @param int $depth Depth of page. Used for padding.
        */
    function end_lvl(&$output, $depth = 0, $args = array()) {
        $indent = str_repeat("\t", $depth);

        $element = 'ul';
        $output .= "$indent</{$element}>\n";

        $this->textbox_active = false;
    }

    /**
        * @see Walker::start_el()
        * @since 3.0.0
        *
        * @param string $output Passed by reference. Used to append additional content.
        * @param object $item Menu item data object.
        * @param int $depth Depth of menu item. Used for padding.
        * @param int $current_page Menu item ID.
        * @param object $args
        */
    function start_el(&$output, $object, $depth = 0, $args = array(), $id = 0) {
        global $wp_query;
        $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

        $class_names = $value = '';

        $value = get_post_meta( $object->ID, 'menu-item-megamenu-'.$object->ID,true);
        $value = ($value=="on") ? true  : false ;

        $type = get_post_meta( $object->ID, 'menu-item-megamenu-layout-'.$object->ID,true);

        $enable_textbox = get_post_meta( $object->ID, 'menu-item-enable-textbox-'.$object->ID,true);
        $textbox = get_post_meta( $object->ID, 'menu-item-textbox-'.$object->ID,true);

        if($depth==0) {
            $this->megamenu_active = $value;

            if($this->megamenu_active) {
                if($type=="column")
                    $this->choice_of_menu = "column";
            }

        }

        if($depth==1 && $enable_textbox=="on")
        {
            $this->textbox_active = true;
        }
        else
            $this->textbox_active = false;

        $classes = empty( $object->classes ) ? array() : (array) $object->classes;
        $classes[] = 'menu-item-' . $object->ID;
        $ex_class = '';
        
        $megaItem=($this->megamenu_active)? "mega-item":"";

        
        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $object, $args ) );
        $menu_columns=get_post_meta( $object->ID, 'mega-menu-columns-'.$object->ID,true);
        if($menu_columns && $this->megamenu_active){
            $menu_column_class = " mega_width ";

            if($menu_columns == "2 Columns"){
            $menu_column_class .= "mega-two";
            }elseif($menu_columns == "3 Columns"){
            $menu_column_class .= "mega-three";
            }else{
            $menu_column_class .= "mega-four";
            }
            $class_names.=$menu_column_class." ";
        }
        $class_names = ' class="' . esc_attr( $class_names )." $ex_class $megaItem ". '   "';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $object->ID, $object, $args );
        $id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';


        if(!$this->megamenu_active) {     
            $output .= $indent . '<li' . $id  . $class_names .'>';

            $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
            $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
            $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
            $attributes .= ! empty( $object->url )        ? ' href="'   . esc_url( $object->url        ) .'"' : '';

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before . apply_filters( 'the_title', $object->title, $object->ID ) . $args->link_after;
            $item_output .= '</a> ';
            $item_output .= $args->after;

        } elseif ($this->choice_of_menu=="column") {
            
            if ($depth==1) {
                $output .= $indent . '<li '.$class_names.'><div' . $id  . $class_names .'>';
                $this->menu_row_counter++;
            } else {
                $output .= $indent . '<li' . $id .  $class_names .'>';
            }
            
            $attributes  = ! empty( $object->attr_title ) ? ' title="'  . esc_attr( $object->attr_title ) .'"' : '';
            $attributes .= ! empty( $object->target )     ? ' target="' . esc_attr( $object->target     ) .'"' : '';
            $attributes .= ! empty( $object->xfn )        ? ' rel="'    . esc_attr( $object->xfn        ) .'"' : '';
            $attributes .= ! empty( $object->url )        ? ' href="'   . esc_url( $object->url        ) .'"' : '';

            $item_output = $args->before;
            if($depth==1)
                $item_output .= '<h6>';
            else
                $item_output .= '<a'. $attributes .'>';


            $item_output .= $args->link_before . apply_filters( 'the_title', $object->title, $object->ID ) . $args->link_after;

            if($depth==1) {
                $item_output .= '</h6>';
            } else {
                $item_output .= '</a>';
            }

            $item_output .= $args->after;


            if($depth==1 && $this->textbox_active ) {
                $item_output .= "<div class='megamenu-textbox'> ".do_shortcode( $textbox)." </div>";
            }
        }
        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $object, $depth, $args );
    }

    /**
        * @see Walker::end_el()
        * @since 3.0.0
        *
        * @param string $output Passed by reference. Used to append additional content.
        * @param object $item Page data object. Not used.
        * @param int $depth Depth of page. Not Used.
        */
    function end_el(&$output, $object, $depth = 0, $args = array(), $current_object_id = 0){
        if(!$this->megamenu_active) {
            $output .= "</li>\n";
        } else {
            if($depth==1) {
                $output .= "</div>\n</li>";
            } else {
                $output .= "</li>\n";
            }
        }
    }
}
?>