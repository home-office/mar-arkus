<?php
class mtheme_MegaMenu_Nav_Menu extends Walker_Nav_Menu {
    /**
        * @see Walker_Nav_Menu::start_lvl()
        * @since 3.0.0
        *
        * @param string $output Passed by reference.
        * @param int $depth Depth of page.
        */
    function start_lvl(&$output, $depth = 0, $args = array()) {}

    /**
        * @see Walker_Nav_Menu::end_lvl()
        * @since 3.0.0
        *
        * @param string $output Passed by reference.
        * @param int $depth Depth of page.
        */
    function end_lvl(&$output, $depth = 0, $args = array()) {}

    /**
        * @see Walker::start_el()
        * @since 3.0.0
        *
        * @param string $output Passed by reference. Used to append additional content.
        * @param object $item Menu item data object.
        * @param int $depth Depth of menu item. Used for padding.
        * @param int $current_page Menu item ID.
        * @param object $args
        */
    function start_el(&$output, $object, $depth = 0, $args = array(), $id = 0) {
        global $_wp_nav_menu_max_depth;
        $_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

        ob_start();
        $item_id = esc_attr( $object->ID );
        $removed_args = array(
            'action',
            'customlink-tab',
            'edit-menu-item',
            'menu-item',
            'page-tab',
            '_wpnonce',
        );

        $original_title = '';
        if ( 'taxonomy' == $object->type ) {
            $original_title = get_term_field( 'name', $object->object_id, $object->object, 'raw' );
        } elseif ( 'post_type' == $object->type ) {
            $original_object = get_post( $object->object_id );
            $original_title = $original_object->post_title;
        }

        $classes = array(
            'menu-item menu-item-depth-' . $depth,
            'menu-item-' . esc_attr( $object->object ),
            'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
        );

        $title = $object->title;

        if ( isset( $object->post_status ) && 'draft' == $object->post_status ) {
            $classes[] = 'pending';
            /* translators: %s: title of menu item in draft status */
            $title = sprintf( __('%s (Pending)','mthemelocal'), $object->title );
        }

        $title = empty( $object->label ) ? $title : $object->label;
        $value = get_post_meta( $item_id, 'menu-item-megamenu-'.$item_id,true);
        $value = ($value=="on") ? "checked='checked'"  : "";

        $type = get_post_meta( $item_id, 'menu-item-megamenu-layout-'.$item_id,true);

        $menu_columns = get_post_meta( $item_id, 'mega-menu-columns-'.$item_id,true);
        $enable_textbox = get_post_meta( $item_id, 'menu-item-enable-textbox-'.$item_id,true);
        $enable_textbox= ($enable_textbox=="on") ? "checked='checked'"  : "";

        $textbox = get_post_meta( $item_id, 'menu-item-textbox-'.$item_id,true);
?>
        <li id="menu-item-<?php echo $item_id; ?>" class="<?php echo implode(' ', $classes ); ?>">
            <dl class="menu-item-bar">
                <dt class="menu-item-handle">
                    <span class="item-title"><?php echo esc_html( $title ); ?></span>
                    <span class="item-controls">
                        <span class="item-type item-type-mtheme-default"><?php echo esc_html( $object->type_label ); ?></span>
                        <a class="item-edit" id="edit-<?php echo $item_id; ?>" title="<?php _e('Edit Menu Item','mthemelocal'); ?>" href="<?php
                            echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) );
                        ?>"><?php _e( 'Edit Menu Item' ,'mthemelocal'); ?>
                        </a>
                    </span>
                </dt>
            </dl>

            <div class="menu-item-settings" id="menu-item-settings-<?php echo $item_id; ?>">
                <?php if( 'custom' == $object->type ) : ?>
                    <p class="field-url description description-wide">
                        <label for="edit-menu-item-url-<?php echo $item_id; ?>">
                            <?php _e( 'URL','mthemelocal' ); ?><br />
                            <input type="text" id="edit-menu-item-url-<?php echo $item_id; ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->url ); ?>" />
                        </label>
                    </p>
                <?php endif; ?>
                <p class="description description-thin">
                    <label for="edit-menu-item-title-<?php echo $item_id; ?>">
                        <?php _e( 'Navigation Label','mthemelocal' ); ?><br />
                        <input type="text" id="edit-menu-item-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->title ); ?>" />
                    </label>
                </p>
                <p class="description description-thin">
                    <label for="edit-menu-item-attr-title-<?php echo $item_id; ?>">
                        <?php _e( 'Title Attribute','mthemelocal' ); ?><br />
                        <input type="text" id="edit-menu-item-attr-title-<?php echo $item_id; ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->post_excerpt ); ?>" />
                    </label>
                </p>

                <p class="field-link-target description description-thin">
                    <label for="edit-menu-item-target-<?php echo $item_id; ?>">
                        <?php _e( 'Link Target','mthemelocal' ); ?><br />
                        <select id="edit-menu-item-target-<?php echo $item_id; ?>" class="widefat edit-menu-item-target" name="menu-item-target[<?php echo $item_id; ?>]">
                            <option value="" <?php selected( $object->target, ''); ?>><?php _e('Same window or tab','mthemelocal'); ?></option>
                            <option value="_blank" <?php selected( $object->target, '_blank'); ?>><?php _e('New window or tab','mthemelocal'); ?></option>
                        </select>
                    </label>
                </p>
                <p class="field-css-classes description description-thin">
                    <label for="edit-menu-item-classes-<?php echo $item_id; ?>">
                        <?php _e( 'CSS Classes (optional)','mthemelocal' ); ?><br />
                        <input type="text" id="edit-menu-item-classes-<?php echo $item_id; ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo $item_id; ?>]" value="<?php echo esc_attr( implode(' ', $object->classes ) ); ?>" />
                    </label>
                </p>
                <p class="field-xfn description description-thin">
                    <label for="edit-menu-item-xfn-<?php echo $item_id; ?>">
                        <?php _e( 'Link Relationship (XFN)','mthemelocal' ); ?><br />
                        <input type="text" id="edit-menu-item-xfn-<?php echo $item_id; ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->xfn ); ?>" />
                    </label>
                </p>
                <p class="field-description description description-wide">
                    <label for="edit-menu-item-description-<?php echo $item_id; ?>">
                        <?php _e( 'Description','mthemelocal' ); ?><br />
                        <textarea id="edit-menu-item-description-<?php echo $item_id; ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo $item_id; ?>]"><?php echo esc_html( $object->description ); ?></textarea>
                        <span class="description"><?php _e('The description will be displayed in the menu if the current theme supports it.','mthemelocal'); ?></span>
                    </label>
                </p>

                <div class="menu-item-actions description-wide submitbox">
                    <?php
                    /* New fields insertion starts here */
                    //if($depth == 0) {
                    ?>  
                    <p class="mtheme_megamenu_box mtheme_megamenu_choice clearfix">
                        <label for="menu-item-megamenu-<?php echo $item_id; ?>">Mega Menu </label>
                        <input type="checkbox" id="menu-item-megamenu-<?php echo $item_id; ?>"  name="menu-item-megamenu-<?php echo $item_id; ?>" <?php  echo $value; ?> />
                        <input type="hidden" name="menu-item-megamenu-layout-<?php echo $item_id; ?>" id="menu-item-megamenu-layout-<?php echo $item_id; ?>" value="column" />
                        <select name="mega-menu-columns-<?php echo $item_id; ?>" id="mega-menu-columns-<?php echo $item_id; ?>">
                        	<option <?php if($menu_columns == "2 Columns") echo("selected") ?>>2 Columns</option>
                        	<option <?php if($menu_columns == "3 Columns") echo("selected") ?>>3 Columns</option>
                        	<option <?php if($menu_columns == "4 Columns") echo("selected") ?>>4 Columns</option>
                        </select>
                    </p> 
                    <?php
                    //}
                    /* New fields insertion ends here */
                    ?>
                    <?php
                    /* New fields insertion starts here */
                    //if($depth > 0) {
                    ?> 
                    <p class="mtheme_megamenu_row_box mtheme_megamenu_textbox_choice clearfix">
                        <label for="menu-item-enable-textbox-<?php echo $item_id; ?>"><?php echo _e('Megamenu text box','mthemelocal'); ?></label>
                        <input type="checkbox" id="menu-item-enable-textbox-<?php echo $item_id; ?>"  name="menu-item-enable-textbox-<?php echo $item_id; ?>" <?php  echo $enable_textbox; ?> class="enable_textbox" />
                        <textarea rows="3" cols="20" name="menu-item-textbox-<?php echo $item_id; ?>" id="menu-item-textbox-<?php echo $item_id; ?>" class="textbox <?php if($enable_textbox=="") echo "hide"; ?>" ><?php echo $textbox; ?></textarea>
                        <span class="megamenu_help_note"><?php _e('Add "no-title" to CSS Classes field if title is not required.','mthemelocal'); ?></span>
                    </p>
                    <?php
                    //}
                    /* New fields insertion ends here */
                    ?>
                    <?php if( 'custom' != $object->type ) : ?>
                        <p class="link-to-original">
                            <?php printf( __('Original:','mthemelocal').' %s', '<a href="' . esc_attr( $object->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
                        </p>
                    <?php endif; ?>
                    <a class="item-delete submitdelete deletion" id="delete-<?php echo $item_id; ?>" href="<?php
                        echo wp_nonce_url(
                                add_query_arg(
                                        array(
                                                'action' => 'delete-menu-item',
                                                'menu-item' => $item_id,
                                        ),
                                        remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
                                ),
                                'delete-menu_item_' . $item_id
                        ); ?>"><?php _e('Remove','mthemelocal'); ?>
                    </a> 
                    <span class="meta-sep"> | </span> 
                    <a class="item-cancel submitcancel" id="cancel-<?php echo $item_id; ?>" href="<?php	echo add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) );
                            ?>#menu-item-settings-<?php echo $item_id; ?>"><?php _e('Cancel','mthemelocal'); ?>
                    </a>
                </div>
                <input type="hidden" name="menu-item-megamenu-label-<?php echo $item_id; ?>" value="<?php echo $object->title; ?>" />
                <input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo $item_id; ?>]" value="<?php echo $item_id; ?>" />
                <input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->object_id ); ?>" />
                <input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->object ); ?>" />
                <input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->menu_item_parent ); ?>" />
                <input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->menu_order ); ?>" />
                <input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo $item_id; ?>]" value="<?php echo esc_attr( $object->type ); ?>" />
            </div><!-- .menu-item-settings-->
            <ul class="menu-item-transport"></ul>
            <?php
            $output .= ob_get_clean();
    }

    /**
        * @see Walker::end_el()
        * @since 3.0.0
        *
        * @param string $output Passed by reference. Used to append additional content.
        * @param object $item Page data object. Not used.
        * @param int $depth Depth of page. Not Used.
        */
    function end_el(&$output, $object, $depth = 0, $args = array(), $id = 0) {
        $output .= "</li>\n";
    }
}
add_filter( 'wp_edit_nav_menu_walker', 'mtheme_extend_adminsettings_walker' , 100);
function mtheme_extend_adminsettings_walker($name)
{
    return 'mtheme_MegaMenu_Nav_Menu';
}

add_action( 'wp_update_nav_menu_item', 'mtheme_update_menu', 100, 3);
function mtheme_update_menu($menu_id, $menu_item_db)
{
	$menu_label = '';
	if (isset($_POST["menu-item-megamenu-label-".$menu_item_db])) {
        $value = $_POST["menu-item-megamenu-label-".$menu_item_db];
    }
	
	$value = '';
	$value_type = '';
	$value_textbox ='';
	$value_item_textbox ='';
	$value_menu_columns ='';
	
	if (isset($_POST['menu-item-megamenu-'.$menu_item_db])) {
        $value = $_POST['menu-item-megamenu-'.$menu_item_db];
    }
	if (isset($_POST['menu-item-megamenu-layout-'.$menu_item_db])) {
        $value_type = $_POST['menu-item-megamenu-layout-'.$menu_item_db];
    }
	if (isset($_POST['menu-item-enable-textbox-'.$menu_item_db])) {
        $value_textbox = $_POST['menu-item-enable-textbox-'.$menu_item_db];
    }
	if (isset($_POST['menu-item-textbox-'.$menu_item_db])) {
        $value_item_textbox = $_POST['menu-item-textbox-'.$menu_item_db];
    }
	if (isset($_POST['mega-menu-columns-'.$menu_item_db])) {
        $value_menu_columns = $_POST['mega-menu-columns-'.$menu_item_db];
    }

    update_post_meta( $menu_item_db, 'menu-item-megamenu-'.$menu_item_db , $value );
    update_post_meta( $menu_item_db, 'menu-item-megamenu-layout-'.$menu_item_db , $value_type );
    update_post_meta( $menu_item_db, 'menu-item-enable-textbox-'.$menu_item_db, $value_textbox );
    update_post_meta( $menu_item_db, 'menu-item-textbox-'.$menu_item_db, $value_item_textbox );
    update_post_meta( $menu_item_db, 'mega-menu-columns-'.$menu_item_db, $value_menu_columns );

}
