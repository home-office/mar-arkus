<div class="preloader-wrap">
    <div class="preloader-site">
        <div>
<?php
$main_logo_dark=of_get_option('main_logo_dark');
$main_logo_bright=of_get_option('main_logo_bright');
if ( $main_logo_dark == "" && $main_logo_bright <> "" ) {
    $main_logo_dark = $main_logo_bright;
}
if ( $main_logo_bright == "" && $main_logo_dark <> "" ) {
    $main_logo_dark = $main_logo_bright;
}
$get_preloader_logo = of_get_option('preloader_logo');
if (isSet($get_preloader_logo) && $get_preloader_logo<>'') {
    $preloader_logo = $get_preloader_logo;
} else {
    $preloader_logo = $main_logo_dark;
}
if (isSet($preloader_logo) && !empty($preloader_logo) && $preloader_logo<>'') {
    echo '<img class="preloader-site-logo" src="'.esc_url($preloader_logo).'" alt="logo" />';
} else {
    echo '<img class="preloader-site-logo" src="'.MTHEME_PATH.'/images/logo_dark.png" alt="logo" />';
}

$custom_preloader_svg = of_get_option('custom_preloader_svg');
if (isSet($custom_preloader_svg) && !empty($custom_preloader_svg) && $custom_preloader_svg<>'') {
    echo stripslashes($custom_preloader_svg);
} else {
?>
<svg id="grid-preloader" class="grid-preloader-accent" width="30" height="30" viewBox="0 0 105 105" xmlns="http://www.w3.org/2000/svg" fill="#24bee8">
    <circle class="circle-one" cx="12.5" cy="12.5" r="12.5">
    </circle>
    <circle class="circle-two" cx="12.5" cy="52.5" r="12.5" fill-opacity=".5">
    </circle>
    <circle class="circle-three" cx="52.5" cy="12.5" r="12.5">
    </circle>
    <circle class="circle-four" cx="52.5" cy="52.5" r="12.5" fill-opacity=".5">
    </circle>
    <circle class="circle-three" cx="92.5" cy="12.5" r="12.5">
    </circle>
    <circle class="circle-two" cx="92.5" cy="52.5" r="12.5" fill-opacity=".5">
    </circle>
    <circle class="circle-four" cx="12.5" cy="92.5" r="12.5">
    </circle>
    <circle class="circle-one" cx="52.5" cy="92.5" r="12.5" fill-opacity=".5">
    </circle>
    <circle class="circle-three" cx="92.5" cy="92.5" r="12.5">
    </circle>
</svg>

<?php
}
?>
        </div>
    </div>
</div>
