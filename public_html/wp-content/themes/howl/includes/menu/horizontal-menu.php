<div class="stickymenu-zone outer-wrap">
<div class="outer-header-wrap clearfix">
	<div class="top-bar-wrap clearfix">
		<div class="header-logo-section">
			<div class="logo">
				<a href="<?php echo home_url(); ?>/">
					<?php
					$general_theme_style=of_get_option('general_theme_style');
					$main_logo_dark=of_get_option('main_logo_dark');
					$main_logo_bright=of_get_option('main_logo_bright');

					if ( $main_logo_bright =='') {
						$main_logo_bright = $main_logo_dark;
					}
					
					$main_header_type = of_get_option('main_header_type');

					if (! MTHEME_DEMO_STATUS) {
						if ( $main_logo_bright<>"" ) {
							echo '<img class="logo-theme-light" src="'.esc_url($main_logo_bright).'" alt="logo" />';
						}
						if ($main_logo_dark <> "") {
							echo '<img class="logo-theme-dark" src="'.esc_url($main_logo_dark).'" alt="logo" />';
						}
						if ( $main_logo_bright == "" && $main_logo_dark == "" ) {
							echo '<img class="logo-theme-light" src="'.esc_url(MTHEME_PATH.'/images/logo.png').'" alt="logo" />';
							echo '<img class="logo-theme-dark" src="'.esc_url(MTHEME_PATH.'/images/logo_dark.png').'" alt="logo" />';
						}
					} else {
						echo '<img class="logo-theme-light" src="'.esc_url(MTHEME_PATH.'/images/logo.png').'" alt="logo" />';
						echo '<img class="logo-theme-dark" src="'.esc_url(MTHEME_PATH.'/images/logo_dark.png').'" alt="logo" />';
					}
					?>
				</a>
			</div>
		</div>
		<div class="logo-menu-wrap">
			<nav>
				<div class="login-socials-wrap clearfix">
					<?php if ( !function_exists('dynamic_sidebar') 
				
						|| !dynamic_sidebar('Social Header') ) : ?>
				
					<?php endif; ?>
				</div>
				<div class="mainmenu-navigation">
						<?php
						$wpml_lang_selector_disable= of_get_option('wpml_lang_selector_disable');
						if (!$wpml_lang_selector_disable) {
						?>
						<div class="wpml-lang-selector-wrap">
							<?php do_action('icl_language_selector'); ?>
						</div>
						<?php
						}
						?>
						<div class="homemenu">
						<?php

						$custom_menu_call = '';
						if (MTHEME_DEMO_STATUS) {
							if ( is_page('one-page') ) {
								$custom_menu_call = 'onepage';
							}
						}
						wp_nav_menu( array(
						 'container' =>false,
						 'menu' => $custom_menu_call,
						 'theme_location' => 'top_menu',
						 'menu_class' => 'sf-menu',
						 'echo' => true,
						 'before' => '',
						 'after' => '',
						 'link_before' => '',
						 'link_after' => '',
						 'depth' => 0,
						 'fallback_cb' => 'mtheme_nav_fallback',
						 'walker' => new mtheme_Menu_Megamenu()
						 )
						);
						if ( !of_get_option('headersearch_disable') ) {
						?>
						<span class="header-search"><i class="feather-icon-search"></i></span>
						<?php
						}
						if ( class_exists( 'woocommerce' ) ) {
							echo '<span class="header-cart header-cart-toggle"><i class="feather-icon-bag"></i></span>';
						}
						do_action('mtheme_header_woocommerce_shopping_cart_counter');
						?>
						</div>
				</div>
			</nav>
		</div>
	</div>
</div>
</div>