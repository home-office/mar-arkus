<div class="vertical-sidemenu-wrap clearfix">
	<div class="vertical-bar-wrap">
		<div class="vertical-logo-section">
			<div class="logo">
				<a href="<?php echo esc_url( home_url() ); ?>/">
					<?php
					$main_logo_dark=of_get_option('main_logo_dark');
					$main_logo_bright=of_get_option('main_logo_bright');

					if ( $main_logo_dark == "" && $main_logo_bright <> "" ) {
					    $main_logo_dark = $main_logo_bright;
					}
					if ( $main_logo_bright == "" && $main_logo_dark <> "" ) {
					    $main_logo_dark = $main_logo_bright;
					}
				
					$main_header_type = of_get_option('main_header_type');

					if (! MTHEME_DEMO_STATUS) {
						if ( $main_logo_bright<>"" ) {
							echo '<img class="logo-theme-light" src="'.esc_url($main_logo_bright).'" alt="logo" />';
						}
						if ($main_logo_dark <> "") {
							echo '<img class="logo-theme-dark" src="'.esc_url($main_logo_dark).'" alt="logo" />';
						}
						if ( $main_logo_bright == "" && $main_logo_dark == "" ) {
							echo '<img class="logo-theme-light" src="'.esc_url(MTHEME_PATH.'/images/logo.png').'" alt="logo" />';
							echo '<img class="logo-theme-dark" src="'.esc_url(MTHEME_PATH.'/images/logo_dark.png').'" alt="logo" />';
						}
					} else {
						echo '<img class="logo-theme-light" src="'.esc_url(MTHEME_PATH.'/images/logo.png').'" alt="logo" />';
						echo '<img class="logo-theme-dark" src="'.esc_url(MTHEME_PATH.'/images/logo_dark.png').'" alt="logo" />';
					}
					?>
				</a>
			</div>
		</div>
		<div class="vertical-menu-wrap">
			<nav>
				<div class="vertical-navigation">
						<?php
						$wpml_lang_selector_disable= of_get_option('wpml_lang_selector_disable');
						if (!$wpml_lang_selector_disable) {
						?>
						<div class="wpml-lang-selector-wrap">
							<?php do_action('icl_language_selector'); ?>
						</div>
						<?php
						}
						?>
						<div class="vertical-menu">
						<?php

						$custom_menu_call = '';
						if (MTHEME_DEMO_STATUS) {
							if ( is_page('one-page') ) {
								$custom_menu_call = 'onepage';
							}
						}
						wp_nav_menu( array(
						 'container' =>false,
						 'menu' => $custom_menu_call,
						 'theme_location' => 'top_menu',
						 'menu_class' => 'mtree',
						 'echo' => true,
						 'before' => '',
						 'after' => '',
						 'link_before' => '',
						 'link_after' => '',
						 'depth' => 0,
						 'fallback_cb' => 'mtheme_nav_fallback'
						 )
						);
						?>
						</div>
				</div>
			</nav>
		</div>
		<div class="vertical-menu-footer">
			<div class="login-socials-wrap clearfix">
				<?php
				if ( !of_get_option('headersearch_disable') ) {
				?>
				<span class="header-search"><i class="feather-icon-search"></i></span>
				<?php
				}
				if ( !function_exists('dynamic_sidebar') 
			
					|| !dynamic_sidebar('Social Header') ) : ?>
			
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>