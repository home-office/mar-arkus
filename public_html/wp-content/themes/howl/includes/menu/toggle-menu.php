<div class="toggle-menu-list">
<?php
wp_nav_menu( array(
 'container' =>false,
 'theme_location' => 'toggle_menu',
 'menu_class' => 'tm-menu',
 'echo' => true,
 'before' => '',
 'after' => '',
 'link_before' => '',
 'link_after' => '',
 'depth' => 0,
 'fallback_cb' => 'mtheme_nav_fallback'
 )
);
?>
</div>