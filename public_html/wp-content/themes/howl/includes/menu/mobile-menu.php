<div class="responsive-menu-wrap">
	<div class="mobile-menu-toggle">
		<span class="mobile-menu-icon"><i class="mobile-menu-icon-toggle feather-icon-menu"></i></span>
				<div class="logo-mobile">
						<?php
						$main_logo_dark=of_get_option('main_logo_dark');
						$main_logo_bright=of_get_option('main_logo_bright');

						if ( $main_logo_dark == "" && $main_logo_bright <> "" ) {
						    $main_logo_dark = $main_logo_bright;
						}
						if ( $main_logo_bright == "" && $main_logo_dark <> "" ) {
						    $main_logo_dark = $main_logo_bright;
						}
						$responsive_logo=of_get_option('responsive_logo');
						if ( $main_logo_bright<>"" || $responsive_logo<>"" ) {
							if (of_get_option('mobile_menuonwhite')){
								$main_logo_bright=$main_logo_dark;
							}
							if ($responsive_logo<>"") {
								echo '<img class="logoimage" src="' . esc_url( $responsive_logo ) .'" alt="logo" />';
							} else {
								echo '<img class="logoimage" src="' . esc_url( $main_logo_bright ) .'" alt="logo" />';
							}
						} else {
							echo '<img class="logo-light" src="'. esc_url( MTHEME_PATH .'/images/logo.png' ) . '" alt="logo" />';
						}
						?>
				</div>
	</div>
</div>
<div class="responsive-mobile-menu clearfix">
	<?php
	$wpml_lang_selector_disable= of_get_option('wpml_lang_selector_disable');
	if (!$wpml_lang_selector_disable) {
	?>
	<div class="mobile-wpml-lang-selector-wrap">
		<?php do_action('icl_language_selector'); ?>
	</div>
	<?php
	}
	?>
	<div class="mobile-social-header">				
	<?php if ( !function_exists('dynamic_sidebar') 

	|| !dynamic_sidebar('Mobile Social Header') ) : ?>

	<?php endif; ?>
	</div>
	<?php
	get_template_part('mobile','searchform');
	?>
	<nav>
	<?php
	$custom_menu_call = '';
	// Responsive menu conversion to drop down list
	if ( function_exists('wp_nav_menu') ) { 
		wp_nav_menu( array(
		 'container' =>false,
		 'theme_location' => 'mobile_menu',
		 'menu' => $custom_menu_call,
		 'menu_class' => 'mobile-menu',
		 'echo' => true,
		 'before' => '',
		 'after' => '',
		 'link_before' => '',
		 'link_after' => '',
		 'depth' => 0,
		 'fallback_cb' => 'mtheme_nav_fallback'
		 )
		);
	}
	?>
	</nav>
</div>