<?php
/*
* @ Header
*/
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
	<?php
	$fav_icon = of_get_option('general_fav_icon');
	if ( isSet($fav_icon) && !empty($fav_icon) ) {
		echo  '<link rel="shortcut icon" href="'.esc_url( $fav_icon ) . '" />';
	}
	wp_head();
	?>
</head>
<body <?php body_class(); ?>>
<?php
get_template_part('/includes/preloader','site');
//Demo Panel if active
do_action('mtheme_demo_panel');
//Check for sidebar choice
do_action('mtheme_get_sidebar_choice');
//Mobile menu
if (is_page_template('template-blank.php')) {

	$site_layout_width='fullwidth';

} else {

	get_template_part('/includes/menu/mobile','menu');
	//Header Navigation elements
	get_template_part('header','navigation');
	$site_layout_width=of_get_option('general_theme_page');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_layout'] ) ) {
			$site_layout_width=$_GET['demo_layout'];
		}
	}

	$theme_menu_type = of_get_option('menu_type');
	if (MTHEME_DEMO_STATUS) {
		if ( isSet( $_GET['demo_menu'] ) ) {
			$theme_menu_type=$_GET['demo_menu'];
		}
	}
	if ( $theme_menu_type=="vertical" ) {
		get_template_part('/includes/menu/vertical','menu');
	}
}
if ($site_layout_width=="boxed") { 
	echo '<div id="home" class="container-wrapper container-boxed">'; 
} else {
	echo '<div id="home" class="container-wrapper container-fullwidth">';
}
if (!is_page_template('template-blank.php')) {
	get_template_part('header','title');
}
$post_type = get_post_type( get_the_ID() );
$custom = get_post_custom( get_the_ID() );
$mtheme_pagestyle='';
if (isset($custom[MTHEME . '_pagestyle'][0])) { $mtheme_pagestyle=$custom[MTHEME . '_pagestyle'][0]; }
if ( $mtheme_pagestyle=="edge-to-edge" && is_singular() ) {
	echo '<div class="container-edge-to-edge">';
} else {
	echo '<div class="container clearfix">';
}
?>