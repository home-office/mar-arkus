jQuery(document).ready(function($){
	"use strict";
	
	var stickyzone = $('.stickymenu-zone');
	var stickyNavTop = 10;

	var stickyNav = function(){
		var scrollTop = $(window).scrollTop();
		     
		if (scrollTop > stickyNavTop) { 
		   stickyzone.addClass('sticky-menu-activate');
		} else {
		   stickyzone.removeClass('sticky-menu-activate'); 
		}
	};

	stickyNav();

	$(window).scroll(function() {
		stickyNav();
	});
});
