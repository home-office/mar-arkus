jQuery(document).ready(function($){
	"use strict";

	// cache container
	var $filterContainer = $('#gridblock-container,#gridblock-container-blog');
	var AjaxPortfolio;
	var portfolio_height;
	var portfolio_width;
	var half_width;
	var image_height;
	var slideshow_active;
	var AutoStart;
	var ajax_image_height;
	var ajax_window_height;
	var $data;
	var heart;
	var post_id;
	var parentPortfolio_id;

	$('#gridblock-filters a').first().addClass('is-active');

	function AjaxLike() {

		jQuery(".mtheme-post-like a.vote-ready").click(function(){

			heart = jQuery(this);

			post_id = heart.data("post_id");
			var parentPortfolio_id = jQuery(this).closest("li").data("id");

			jQuery.ajax({
				type: "post",
				url: ajax_var.url,
				data: "action=mtheme-post-like&nonce="+ajax_var.nonce+"&mtheme_post_like=&post_id="+post_id,
	            beforeSend : function(){
					if( !heart.hasClass('voted')) {
						heart.children("span.mtheme-like").addClass("voteprogress");
					}
	            },
				success: function(data){

					// Split and Get the values in data varaible that has been given as COUNT:POSTID

					var substr = data.split(':');
					var count = substr[0];
					var post_id = substr[1];

					if(count != "already")
					{
						
						jQuery('.mtheme-post-like a[data-post_id="'+post_id+'"]').removeClass("vote-ready").addClass("vote-disabled");
						jQuery('.mtheme-post-like a[data-post_id="'+post_id+'"]').find(".mtheme-like").removeClass("like-notvoted").addClass("like-alreadyvoted");
						jQuery('.post-link-count-wrap[data-count_id="'+post_id+'"]').find("span.post-like-count").text(count);
						jQuery('.mtheme-post-like a[data-post_id="'+post_id+'"] span.mtheme-like').removeClass("voteprogress");
						
					}
				},
	            error     : function(jqXHR, textStatus, errorThrown) {
	                alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
	            }
			});

			return false;
		});
		
	}

	function PrettyPhotoLightbox() {
		jQuery("a[rel^='prettyPhoto']").prettyPhoto({
			opacity: 0.9,
			theme: 'pp_default', /* light_rounded / dark_rounded / light_square / dark_square / facebook */
			default_width: 700,
			default_height: 444,
			overlay_gallery: false,
			show_title: false,
			social_tools: false
		});
	}

	function Magnific_Lightbox() {
		$("a[data-lightbox^='magnific-image']").magnificPopup({ 
		  type: 'image'
		});
		$("a[data-lightbox^='magnific-image-gallery']").magnificPopup({ 
			type: 'image',
			gallery: {
			  enabled: true
			}
		});
		$('.mtheme-block-em_lightboxcarousel,.mtheme-block-em_thumbnails').each(function() {
			$(this).find(".column-gridblock-icon").magnificPopup({
				type: 'image',
				gallery: {
				  enabled: true
				}
		    });
		});
		$("a[data-lightbox^='magnific-video']").magnificPopup({ 
		  type: 'iframe'
		});
	}
	
	function HoverEffect() {
		//Ajax Hover
		jQuery("div.gridblock-element").hover(
		function () {
			var GotImage = $(this).find(".preload-image");
			if (GotImage.is(':visible')) {
				portfolio_height=jQuery(this).height()-10;
				portfolio_width=jQuery(this).width();

				jQuery(this).find("span.ajax-image-hover")
				.css({ "display":"block", "left":"0", "height": ""+portfolio_height+"px", "width": ""+portfolio_width+"px"})
				.stop().animate({"top": "0","opacity" : "1"}, "normal");
			}
		},
		function () {
			jQuery(this).find("span.ajax-image-hover").stop().animate({"top": "10px","opacity" : "0"}, "fast");
		});

		//Thumbnails shortcode hover
		jQuery("div.thumbnails-shortcode ul li").hover(
		function () {

			var GotImage = $(this).find(".displayed-image");
			if (GotImage.is(':visible')) {
				portfolio_height=$(this).find("img.displayed-image").height();
				portfolio_width=$(this).find("img.displayed-image").width();

				jQuery(this).find(".gridblock-image-icon")
				.css({"display":"block", "top": "0", "left":"0" , "height" : portfolio_height + "px"})
				.stop().animate({"opacity" : "1"}, "normal");
			}
		},
		function () {
			jQuery(this).find(".gridblock-image-icon").stop().animate({"opacity" : "0"}, "fast");
		});

	}

	HoverEffect();
	Magnific_Lightbox();
	AjaxLike();
	
});

jQuery(window).bind("load", function(e) {
	var AutoStart=false;
	var SlideStarted=false;
	jQuery('.ajax-next').addClass('ajax-nav-disabled').css('cursor','default');
	jQuery('.ajax-prev').addClass('ajax-nav-disabled').css('cursor','default');

});

(function($){
$(window).load(function(){

	// cache container
	var $filterContainer = $('#gridblock-container,#gridblock-container-blog,.thumbnails-grid-container');
	var AjaxPortfolio;
	var portfolio_height;
	var portfolio_width;
	var half_width;
	var image_height;
	var slideshow_active;
	var AutoStart;
	var ajax_image_height;
	var ajax_window_height;
	var $data;
		
	var ajaxLoading=0;
	var SlideStarted=false;

    //variables to confirm window height and width
    var lastWindowHeight = $(window).height();
    var lastWindowWidth = $(window).width();

	//Detect Orientaiton change
	window.onload = orientationchange;
	window.onorientationchange = orientationchange;
	jQuery(window).bind("resize", orientationchange);
	function orientationchange() {
		isotopeInit();
	}

    $(window).resize(function() {

        //confirm window was actually resized
        if($(window).height()!=lastWindowHeight || $(window).width()!=lastWindowWidth){

            //set this windows size
            lastWindowHeight = $(window).height();
            lastWindowWidth = $(window).width();

            //call my function
            if ($.fn.isotope) {
            	$filterContainer.isotope( 'layout' );
        	}

           	ajax_image_height=jQuery('.displayed-image').height();
			$('.ajax-image-selector').css({"height" : ajax_image_height + "px"});

        }
    });

	// Toggle - Show and Hide displayed portfolio showcase item
	jQuery("a.ajax-hide").click(
		function () {
			if ( jQuery(".ajax-gridblock-window").is(':animated') || jQuery(".ajax-gridblock-image-wrap").is(':animated') ) return;
			if (SlideStarted==false) {
				jQuery('.gridblock-ajax').eq(0).trigger('click');
			}
			$('.ajax-gridblock-block-wrap').toggleClass('ajax-collapsed');
			jQuery('.ajax-gridblock-window').slideToggle();
			return false;
		}
	);
	
	AjaxPortfolio = function(e) {
		// Initialize
	    var page = 1;
	    var loading = true;
		var loaded = false;
	    var $window = jQuery(window);
	    var $content = jQuery("body #ajax-gridblock-wrap");
	    var $contentData = jQuery("body #ajax-gridblock-content");
		var total = jQuery('#gridblock-container .gridblock-ajax').length;
		var index;
		var nextStatus=true;
		var prevStatus=true;
		
		var isiPhone = navigator.userAgent.toLowerCase().indexOf("iphone");
		var isiPad = navigator.userAgent.toLowerCase().indexOf("ipad");
		var isiPod = navigator.userAgent.toLowerCase().indexOf("ipod");

		var deviceAgent = navigator.userAgent.toLowerCase();
		var isIOS = deviceAgent.match(/(iphone|ipod|ipad)/);
		var ua = navigator.userAgent.toLowerCase();
		var isAndroid = ua.indexOf("android") > -1; //&& ua.indexOf("mobile");

		var altTotal=total-1;


	jQuery(".gridblock-ajax").click(function(){

		//Get postID from rel attribute of link
		var postID = jQuery(this).attr("data-portfolioid");
		if(typeof postID === 'undefined') return;
	
		AutoStart=false;
		SlideStarted=true;

		if ( $(this).parent().hasClass('gridblock-displayed') ) {
			return false;
		}

		ajax_image_height=jQuery('.displayed-image').height();
		$('.ajax-image-selector').css({"height" : ajax_image_height + "px"});

		$('.ajax-gallery-navigation').fadeIn();
		$('span.ajax-loading').show();
		//Get this index
		index=jQuery(".gridblock-ajax").index(this);
		//Store the navigation ID as the current element
		jQuery('.ajax-gallery-navigation').attr('id', index);

		//Grab the current displayed ID
		var DisplayedID = jQuery('.ajax-gallery-navigation').attr("data-portfolioid");
		
		// Compare clicked and Displayed ID. Acts as Gatekeeper


		if (postID!=DisplayedID) {

			$('.ajax-gridblock-block-wrap').addClass('ajax-active');
			// Remove previous displayed set class
			jQuery('div').removeClass("gridblock-displayed");
		
			//Add portfolio post ID to attribute
			jQuery('.ajax-gallery-navigation').attr('data-portfolioid', postID);
		
			//Add the class to currently viewing
			jQuery( '[data-portfolio=portfolio-'+postID+']').addClass('gridblock-displayed');


			var filtered_total = $('#gridblock-container div').not('.isotope-hidden').length;
			var $got_current = $filterContainer.find(".gridblock-displayed");
			var $next_portfolio = $got_current.nextAll("div:not(.isotope-hidden)").first();
			var $prev_portfolio = $got_current.prevAll("div:not(.isotope-hidden)").first();

			if ($next_portfolio.length) {
				$('.ajax-next').removeClass('ajax-nav-disabled').css('cursor','pointer');
			} else {
				$('.ajax-next').addClass('ajax-nav-disabled').css('cursor','default');
			}
			if ($prev_portfolio.length) {
				$('.ajax-prev').removeClass('ajax-nav-disabled').css('cursor','pointer');
			} else {
				$('.ajax-prev').addClass('ajax-nav-disabled').css('cursor','default');
			}

			var sitewide = $('.top-menu-wrap').width();
			// If iphone then scroll to Ajax nav bar - otherwise top of page
			if(sitewide==470 || sitewide==758) {
				jQuery('html, body').stop().animate({
				    scrollTop: jQuery(".ajax-gridblock-block-wrap").offset().top - 20
				}, 1000);
			} else {
				jQuery('html, body').stop().animate({
				    scrollTop: jQuery(".container").offset().top - 70
				}, 1000);
			}

										 function syncPosition(el) {
										     var current = this.currentItem;
										     $("#owl-ajax-2")
										         .find(".owl-item")
										         .removeClass("synced")
										         .eq(current)
										         .addClass("synced")
										     if ($("#owl-ajax-2").data("owlCarousel") !== undefined) {
										         center(current)
										     }
										 }

										 function center(number) {
										     var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
										     var num = number;
										     var found = false;
										     for (var i in sync2visible) {
										         if (num === sync2visible[i]) {
										             var found = true;
										         }
										     }

										     if (found === false) {
										         if (num > sync2visible[sync2visible.length - 1]) {
										             sync2.trigger("owl.goTo", num - sync2visible.length + 2)
										         } else {
										             if (num - 1 === -1) {
										                 num = 0;
										             }
										             sync2.trigger("owl.goTo", num);
										         }
										     } else if (num === sync2visible[sync2visible.length - 1]) {
										         sync2.trigger("owl.goTo", sync2visible[1])
										     } else if (num === sync2visible[0]) {
										         sync2.trigger("owl.goTo", num - 1)
										     }
										 }
			

				jQuery('#ajax-gridblock-loading').show();

				jQuery.ajax({
	                type: "post",
	                url: ajax_var.url,
	                data: "action=ajaxportfolio&post_id="+postID,
	                beforeSend : function(){
						ajax_window_height = $('#ajax-gridblock-content').height();
						$('.ajax-gridblock-window').css({'height': ajax_window_height + 'px'});
	                },
	                success    : function(data){
						loaded = true;
						jQuery('#ajax-gridblock-loading').hide();
						jQuery("#ajax-gridblock-content").remove();
						$('span.ajax-loading').hide();
	                    $data = $(data);

	                    if($data.length){

	                        $content.append($data);
							$('.ajax-gridblock-window').css({'height': 'auto'});
	                        jQuery('.ajax-gridblock-window').slideDown(500, function(){
								jQuery(".ajax-gridblock-image-wrap").fadeTo(100, 1);
								jQuery(".ajax-gridblock-data, .ajax-gridblock-contents-wrap").fadeIn();
	                            loading = false;
	                        });
							jQuery('.ajax-gridblock-image-wrap img').bind('load', function() {
								jQuery('.ajax-gridblock-image-wrap img').fadeTo(100, 1);
								//$('.ajax-portfolio-image-wrap').css({'background': 'none'});
							});
								$("a[data-lightbox^='magnific-image-gallery']").magnificPopup({ 
									type: 'image',
									gallery: {
									  enabled: true
									}
								});

										 var sync1 = $("#owl-ajax");
										 var sync2 = $("#owl-ajax-2");

										 sync1.owlCarousel({
										     singleItem: true,
										     slideSpeed: 500,
										     navigation: true,
										     autoHeight: true,
										     pagination: false,
										     afterAction: syncPosition,
										     navigationText : ["",""],
										     responsiveRefreshRate: 200,
										 });

										 sync2.owlCarousel({
										     items: 15,
										     itemsDesktop: [1199, 10],
										     itemsDesktopSmall: [979, 10],
										     itemsTablet: [768, 8],
										     itemsMobile: [479, 4],
										     pagination: false,
										     responsiveRefreshRate: 100,
										     afterInit: function(el) {
										         el.find(".owl-item").eq(0).addClass("synced");
										     }
										 });

										 $("#owl-ajax-2").on("click", ".owl-item", function(e) {
										     e.preventDefault();
										     var number = $(this).data("owlItem");
										     sync1.trigger("owl.goTo", number);
										 });
								
							
	                    } else {
	                        jQuery('#ajax-gridblock-loading').hide();
	                    }
	                },
	                error     : function(jqXHR, textStatus, errorThrown) {
	                    jQuery('#ajax-gridblock-loading').hide();
	                    alert(jqXHR + " :: " + textStatus + " :: " + errorThrown);
	                }
	        	});

			return false;
			}
		});
	
	}
	
	function AjaxNavigation() {

		// Next Clicked
		$('.ajax-next').click(function(){
			
			if ( $(".ajax-gridblock-window").is(':animated') || $(".ajax-gridblock-image-wrap").is(':animated') ) return;

			var $got_current = $filterContainer.find(".gridblock-displayed");
			var $next_portfolio = $got_current.nextAll("div:not(.isotope-hidden)").first();
			
			if ($next_portfolio.length) {
				$next_portfolio.find(".gridblock-ajax").trigger('click');
			}
			
			return false;

		});

		// Clicked Prev	

		$('.ajax-prev').click(function(){
			
			if ( $(".ajax-gridblock-window").is(':animated') || $(".ajax-gridblock-image-wrap").is(':animated') ) return;

			var $got_current = $filterContainer.find(".gridblock-displayed");
			var $prev_portfolio = $got_current.prevAll("div:not(.isotope-hidden)").first();

			$prev_portfolio.find(".gridblock-ajax").trigger('click');
			
			return false;
		});	
	}
	
	
	function isotopeInit() {
		// initialize isotope
		if ($.fn.isotope) {
			
			if ( $($filterContainer).hasClass('gridblock-masonary') ) {

				var photow_window_width = $('.container').width();
				if ( photow_window_width === null ) {
					photow_window_width = $('.container-edge-to-edge').width();
				}
				var wallContainer_w = $($filterContainer).width()-0.5;

				number_of_columns = $($filterContainer).attr('data-columns');

				var fourcolumn = '23%',
				threecolumn = '31%',
				twocolumn = '48%',
				onecolumn = '99%';

				if ( $($filterContainer).hasClass('thumnails-gutter-active') ) {
					fourcolumn = '25%';
					threecolumn = '33.3333%';
					twocolumn = '50%';
					onecolumn = '100%';
					wallContainer_w = $($filterContainer).width()-0.5;
				}

				if (number_of_columns==4) {
					$($filterContainer).find('.gridblock-element').css('width',fourcolumn);
				}
				if (number_of_columns==3) {
					$($filterContainer).find('.gridblock-element').css('width',threecolumn);
				}
				if (number_of_columns==2) {
					$($filterContainer).find('.gridblock-element').css('width',twocolumn);
				}
				if (number_of_columns==1) {
					$($filterContainer).find('.gridblock-element').css('width',onecolumn);
				}

				if (photow_window_width < 1035 ) {
					number_of_columns = 3;
					$($filterContainer).find('.gridblock-element').css('width',threecolumn);
				}
				if (photow_window_width < 800 ) {
					number_of_columns = 2;
					$($filterContainer).find('.gridblock-element').css('width',twocolumn);
				}			
				if (photow_window_width < 500 ) {
					number_of_columns = 2;
					$($filterContainer).find('.gridblock-element').css('width',onecolumn);
				}

				//console.log(wallContainer_w , wallContainer_w / number_of_columns , number_of_columns );
				console.log( number_of_columns , photow_window_width , wallContainer_w );

				if ( $('body.rtl').length == 1 ) {
					$filterContainer.isotope({
						isOriginLeft: false,
						resizable: false, // disable normal resizing
					  	masonry: {
					    	gutterWidth: 0,
					    	columnWidth: wallContainer_w / number_of_columns
					  }
					});
				} else {
					$filterContainer.isotope({
						resizable: false, // disable normal resizing
					  	masonry: {
					    	gutterWidth: 0,
					    	columnWidth: wallContainer_w / number_of_columns
					  }
					});					
				}

			} else {
				if ( $('body.rtl').length == 1 ) {
					$filterContainer.isotope({
						isOriginLeft: false,
						animationEngine : 'best-available',
						layoutMode : 'fitRows',
						  masonry: {
						    gutterWidth: 0
						  }
					});
				} else {
					$filterContainer.isotope({
						animationEngine : 'best-available',
						layoutMode : 'fitRows',
						  masonry: {
						    gutterWidth: 0
						  }
					});
				}
			}
		}
	}
	function isotopeClicks() {
		// filter items when filter link is clicked
		$('#gridblock-filters a').click(function(){
		  var selector = $(this).attr('data-filter');
		  var filter_title = $(this).attr('data-title');
		  $filterContainer.isotope({ filter: selector });

		  $('#gridblock-filters a').removeClass('is-active');
		  $(this).addClass('is-active');

			$('.gridblock-filter-wrap h2').text(filter_title);
			// Set index to zero and disable prev
			$('.ajax-gallery-navigation').attr('id', '-1');
			$('.ajax-prev').css('cursor','default');

		  return false;
		});
	}


	AjaxPortfolio();
	AjaxNavigation();
	isotopeInit();
	isotopeClicks();
})
})(jQuery);
