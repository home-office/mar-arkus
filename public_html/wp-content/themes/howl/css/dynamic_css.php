<?php
$dynamic_css='';
if (MTHEME_DEMO_STATUS) {
	$site_layout_width=of_get_option('general_theme_page');
if ($site_layout_width="boxed") {
		$demo_bg=array();
		$demo_bg[1]='http://howl.imaginemthemes.com/wp/files/2015/03/background1.jpg';
		$demo_bg[2]='http://howl.imaginemthemes.com/wp/files/2015/03/background2.jpg';
		$demo_bg[3]='http://howl.imaginemthemes.com/wp/files/2015/03/background3.jpg';
		if ( isSet( $_GET['demo_background'] ) ) $_SESSION['demo_background']=$_GET['demo_background'];
		if ( isSet($_SESSION['demo_background'] )) $demo_background = $_SESSION['demo_background'];
		if (isSet($demo_background)) {
			$demo_background_url=$demo_bg[$demo_background];
			$dynamic_css .= 'body.theme-boxed { background-image: url('.$demo_background_url.'); -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; } ';
		}
	}
}
$heading_classes='
input,textarea,button,
.woocommerce .product h1,
.woocommerce .product h2,
.woocommerce .product h3,
.woocommerce .product h4,
.woocommerce .product h5,
.woocommerce .product h6,
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6,
.hero-text-wrap,
.client-say,
.sidebar h3,
.homemenu,
.mtheme-button,
#header-searchform #hs,
.vertical-menu,
.wpcf7-form p,
.toggle-menu-list li a,
.blog-details-section-inner,
.responsive-mobile-menu,
.page-link,
.entry-title h1,
.pricing-wrap,
#gridblock-filters li a,
.pricing-table .pricing-row,
.search-instruction,
.portfolio-end-block h2.section-title';

$page_contents_classes='
body,
.entry-content,
.sidebar-widget,
.homemenu .sf-menu .megamenu-textbox,
.homemenu .sf-menu ul li a
';
//Font
if (of_get_option('default_googlewebfonts')) {
	$dynamic_css .= mtheme_apply_font ( "page_contents" , $page_contents_classes );
	$dynamic_css .= mtheme_apply_font ( "heading_font" , $heading_classes );
	$dynamic_css .= mtheme_apply_font ( "menu_font" , ".mainmenu-navigation .homemenu ul li, .mainmenu-navigation .homemenu ul li a" );
}
//Logo
$logo_width=of_get_option('logo_width');
if ($logo_width) {
	$dynamic_css .= '.logo img { width: '.$logo_width.'px; }';
}
$logo_topmargin=of_get_option('logo_topmargin');
if ($logo_topmargin) {
	$dynamic_css .= '.logo img { margin-top: '.$logo_topmargin.'px; }';
}
$logo_leftmargin=of_get_option('logo_leftmargin');
if ($logo_leftmargin) {
	$dynamic_css .= '.logo img { margin-left: '.$logo_leftmargin.'px; }';
}
$responsive_logo_width = of_get_option('responsive_logo_width');
if ($responsive_logo_width) {
	$dynamic_css .= '.logo-mobile .logoimage { width: '.$responsive_logo_width.'px; }';
	$dynamic_css .= '.logo-mobile .logoimage { height: auto; }';
}
$responsive_logo_topmargin = of_get_option('responsive_logo_topmargin');
if ($responsive_logo_topmargin) {
	$dynamic_css .= '.logo-mobile .logoimage { top: '.$responsive_logo_topmargin.'px; }';
}

$general_background_image = of_get_option('general_background_image');
if ( isSet($general_background_image) )  {
	$dynamic_css .= 'body { background: url('.$general_background_image.') no-repeat center center fixed; 
-webkit-background-size: cover;
-moz-background-size: cover;
-o-background-size: cover;
background-size: cover;}';
}

//Accents
$accent_color=of_get_option('accent_color');
$accent_color_rgb=mtheme_hex2RGB($accent_color,true);
$slideshow_transbar_rgb=mtheme_hex2RGB($accent_color,true);

if (MTHEME_DEMO_STATUS) {
	if ( isSet( $_SESSION['demo_color'] ) || isSet( $_GET['demo_color'] ) ) {
		if ( isSet( $_GET['demo_color'] ) ) $_SESSION['demo_color']=$_GET['demo_color'];
		if ( isSet($_SESSION['demo_color'] )) $demo_color = $_SESSION['demo_color'];
		if (isSet($demo_color)) {
			$accent_color='#'.$demo_color;
		}
	}
}

if ($accent_color) {
$dynamic_css .= mtheme_change_class('.grid-preloader-accent',"fill",$accent_color,'');
$accent_change_color = "
.project-details a,
.post-single-tags a:hover,
.post-meta-category a:hover,
.post-single-meta a:hover,
.post-navigation a:hover,
.sidebar ul li a:hover,
.entry-post-title h2 a:hover,
.comment-reply-title small a,
.header-shopping-cart a:hover,
#gridblock-filter-select i,
.entry-content .blogpost_readmore a,
.pricing-table .pricing_highlight .pricing-price,
#wp-calendar tfoot td#prev a,
#wp-calendar tfoot td#next a,
.sidebar-widget .widget_nav_menu a:hover,
.footer-widget .widget_nav_menu a:hover,
.entry-content .faq-toggle-link:before,
.mtheme-knowledgebase-archive ul li:before,
.like-vote-icon,
.project-details-link h4 a:hover,
.readmore-service a,
.work-details h4,
.work-details h4 a:hover,
#gridblock-filters li .is-active,
#gridblock-filters li a:focus,
#gridblock-filters a:focus,
#gridblock-filters li .is-active,
#gridblock-filters li .is-active:hover,
.post-single-tags a,
.service-content h4 a:hover,
.postsummarywrap a:hover,
.entry-content .heading-block h3,
.entry-content h3.section-subtitle,
.toggle-menu-list li a:hover,
.ui-accordion-header:hover .ui-accordion-header-icon:after,
.quote_say i,
#footer a:hover,
.woocommerce a.add_to_cart_button.added,
.woocommerce ul.products li.product h3 a:hover,
.woocommerce-page ul.products li.product h3 a:hover,
.woocommerce .mtheme-woocommerce-description-wrap:hover a.add_to_cart_button,
.cart-elements .cart-title:hover,
.nav-previous a:hover,
.nav-next a:hover,
.nav-lightbox a:hover
";
$accent_change_background = "
.gridblock-displayed .gridblock-selected-icon,
.skillbar-title,
.skillbar-bar,
div.jp-volume-bar-value,
div.jp-play-bar,
.portfolio-nav-item a:hover,
#wp-calendar caption,
#wp-calendar tbody td a,
.like-alreadyvoted,
.flexslider-container-page .flex-direction-nav li a:hover,
.lightbox-toggle a:hover,
a.ajax-navigation-arrow,
.blog-timeline-month,
.ui-accordion-header.ui-state-active a,
.entry-content .ui-tabs .ui-tabs-nav .ui-state-active a,
.entry-content .ui-tabs .ui-tabs-nav .ui-state-active a:hover,
.person .person-image-wrap,
.pagination span.current,
.gridblock-thumbnail-element:hover,
.synced .gridblock-thumbnail-element,
.woocommerce span.onsale,
.woocommerce-page span.onsale,
.mtheme-woo-order-list ul li:hover,
.woocommerce #respond input#submit.alt,
.woocommerce a.button.alt,
.woocommerce button.button.alt,
.woocommerce input.button.alt,
.woocommerce #content div.product form.cart .button,
.woocommerce div.product form.cart .button,
.woocommerce-page #content div.product form.cart .button,
.woocommerce-page div.product form.cart .button,
.woocommerce input.button,
.woocommerce .shipping-calculator-form button,
.woocommerce .woocommerce-message a.button
";

$accent_change_border = "
ul#thumb-list li.current-thumb,
ul#thumb-list li.current-thumb:hover,
.home-step:hover .step-element img,
.home-step-wrap li,
.gridblock-element:hover,
.gridblock-grid-element:hover,
.gridblock-displayed:hover,
.entry-content blockquote,
#gridblock-filters li .is-active,
#gridblock-filters li a:focus,
#gridblock-filters a:focus,
#gridblock-filters li .is-active,
#gridblock-filters li .is-active:hover,
.person:hover .person-image img,
.main-menu-wrap .homemenu .sf-menu .mega-item .children-depth-0,
.main-menu-wrap .homemenu ul ul,
.like-vote-icon,
#gridblock-timeline .blog-grid-element-left:before,
#gridblock-timeline .blog-grid-element-right:before,
#header-searchform #hs,
.pagination span.current,
.sidebar h3:after,
.woocommerce .quantity input.qty:hover,
.woocommerce #content .quantity input.qty:hover,
.woocommerce-page .quantity input.qty:hover,
.woocommerce-page #content .quantity input:hover,
.woocommerce .quantity input.qty:focus,
.woocommerce #content .quantity input.qty:focus,
.woocommerce-page .quantity input.qty:focus,
.woocommerce-page #content .quantity input:focus,
.fotorama__thumb-border,
.wpcf7-form input:focus,
.wpcf7-form textarea:focus
";

	$dynamic_css .= mtheme_change_class($accent_change_color,"color",$accent_color,'');
	$dynamic_css .= mtheme_change_class($accent_change_background,"background-color",$accent_color,'');
	$dynamic_css .= mtheme_change_class($accent_change_border,"border-color",$accent_color,'');

	$dynamic_css .= ".homemenu .cart { border-bottom-color:".$accent_color.";}";
	$dynamic_css .= ".wp-accordion h3.ui-state-active { border-bottom-color:".$accent_color.";}";
	$dynamic_css .= ".entry-content .pullquote-left { border-right-color:".$accent_color.";}";

	$dynamic_css .= ".entry-content .pullquote-center { border-top-color:".$accent_color.";}";
	$dynamic_css .= ".entry-content .pullquote-center { border-bottom-color:".$accent_color.";}";

	$dynamic_css .= ".blog-details-section-inner,.entry-content .pullquote-right,.callout,.calltype-line-left .callout { border-left-color:".$accent_color.";}";
	
	$dynamic_css .= ".homemenu .sf-menu .mega-item .children-depth-0, .homemenu ul ul { border-bottom-color:".$accent_color.";}";


	$dynamic_css .= '.column-gridblock-icon:after { background-color: rgba('.$accent_color_rgb.',0.1);}';
	$dynamic_css .= '.column-gridblock-icon:hover:after { background-color: rgba('.$accent_color_rgb.',0.3);}';
	
}

//Preloader
$preloader_color=of_get_option('preloader_color');
if ($preloader_color) {
	$dynamic_css .= mtheme_change_class('.grid-preloader-accent',"fill",$preloader_color,'');
}

$title_backgroundcolor=of_get_option('title_backgroundcolor');
if ($title_backgroundcolor) {
	$dynamic_css .= mtheme_change_class('.header-compact .title-container-wrap',"background-color",$title_backgroundcolor,'');
}

// Menu colors

$menu_title_color=of_get_option('menu_title_color');
if ($menu_title_color) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul li a, .mobile-menu-selected',"color",$menu_title_color,'');
}

$menu_titlelinkhover_color=of_get_option('menu_titlelinkhover_color');
if ($menu_titlelinkhover_color) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul li a:hover',"color",$menu_titlelinkhover_color,'');
}

$menusubcat_bgcolor=of_get_option('menusubcat_bgcolor');
if ($menusubcat_bgcolor) {
	$dynamic_css .= mtheme_change_class('.homemenu .sf-menu .mega-item .children-depth-0, .homemenu ul ul',"background-color",$menusubcat_bgcolor,'');
}


$menusubcat_linkcolor=of_get_option('menusubcat_linkcolor');
if ($menusubcat_linkcolor) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul ul li a',"color",$menusubcat_linkcolor,'');
}

$menusubcat_linkhovercolor=of_get_option('menusubcat_linkhovercolor');
if ($menusubcat_linkhovercolor) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul ul li:hover>a ',"color",$menusubcat_linkhovercolor,'');
}
$menusubcat_linkunderlinecolor=of_get_option('menusubcat_linkunderlinecolor');
if ($menusubcat_linkunderlinecolor) {
	$dynamic_css .= mtheme_change_class('.mainmenu-navigation .homemenu ul ul li a ',"border-color",$menusubcat_linkunderlinecolor,'');
}
$menu_parentactive_color=of_get_option('menu_parentactive_color');
if ($menu_parentactive_color) {
	$dynamic_css .= mtheme_change_class('.homemenu li.current-menu-item a:before, .homemenu li.current-menu-ancestor a:before ',"background-color",$menu_parentactive_color,'');
}
$menu_search_color=of_get_option('menu_search_color');
if ($menu_search_color) {
	$dynamic_css .= mtheme_change_class('.header-search i.icon-search,.header-search i.icon-remove',"color",$menu_search_color,'');
}
$menu_search_hovercolor=of_get_option('menu_search_hovercolor');
if ($menu_search_hovercolor) {
	$dynamic_css .= mtheme_change_class('.header-search i.icon-search:hover,.header-search i.icon-remove:hover',"color",$menu_search_hovercolor,'');
}
$menu_stickymenu_bgcolor=of_get_option('menu_stickymenu_bgcolor');
$menu_stickymenu_bgcolor_rgb=mtheme_hex2RGB($menu_stickymenu_bgcolor,true);
if ($menu_stickymenu_bgcolor) {
	$dynamic_css .= '.sticky-menu-activate.outer-wrap { background: '.$menu_stickymenu_bgcolor.'; }';
}
$menu_lower_border=of_get_option('menu_lower_border');
if ($menu_lower_border) {
	$dynamic_css .= mtheme_change_class('.main-menu-wrap .homemenu .sf-menu .mega-item .children-depth-0,.main-menu-wrap .homemenu ul ul',"border-color",$menu_lower_border,'');
}

// Vertical Menu

$vmenu_bg_color=of_get_option('vmenu_bg_color');
if ($vmenu_bg_color) {
	$dynamic_css .= mtheme_change_class('.vertical-sidemenu-wrap',"background-color",$vmenu_bg_color,'');
}
$vmenu_item_bgcolor=of_get_option('vmenu_item_bgcolor');
if ($vmenu_item_bgcolor) {
	$dynamic_css .= mtheme_change_class('.vertical-menu-wrap',"background-color",$vmenu_item_bgcolor,'');
}
$vmenu_active_itemcolor=of_get_option('vmenu_active_itemcolor');
if ($vmenu_active_itemcolor) {
	$dynamic_css .= mtheme_change_class('ul.mtree li.mtree-open > a:hover, ul.mtree li.mtree-open > a',"color",$vmenu_active_itemcolor,'');
}
$vmenu_itemcolor=of_get_option('vmenu_itemcolor');
if ($vmenu_itemcolor) {
	$dynamic_css .= mtheme_change_class('ul.mtree a',"color",$vmenu_itemcolor,'');
}
$vmenu_hover_itemcolor=of_get_option('vmenu_hover_itemcolor');
if ($vmenu_hover_itemcolor) {
	$dynamic_css .= mtheme_change_class('ul.mtree li> a:hover,ul.mtree li.mtree-active > a:hover,ul.mtree li.mtree-active > a',"color",$vmenu_hover_itemcolor,'');
}
$vmenu_search_itemcolor=of_get_option('vmenu_search_itemcolor');
if ($vmenu_search_itemcolor) {
	$dynamic_css .= mtheme_change_class('.menu-is-vertical .header-search i',"color",$vmenu_search_itemcolor,'');
}
$vmenu_social_itemcolor=of_get_option('vmenu_social_itemcolor');
if ($vmenu_social_itemcolor) {
	$dynamic_css .= mtheme_change_class('.menu-is-vertical .social-header-wrap ul li.social-icon i',"color",$vmenu_social_itemcolor,'');
}


// Slideshow Color

$slideshow_title=of_get_option('slideshow_title');
if ($slideshow_title) {
	$dynamic_css .= mtheme_change_class( '.slideshow_title', "color",$slideshow_title,'' );
}
$slideshow_captiontxt=of_get_option('slideshow_captiontxt');
if ($slideshow_captiontxt) {
	$dynamic_css .= mtheme_change_class( '#slidecaption .slideshow_caption', "color",$slideshow_captiontxt,'' );
}
$slideshow_buttontxt=of_get_option('slideshow_buttontxt');
if ($slideshow_buttontxt) {
	$dynamic_css .= mtheme_change_class( '.slideshow_content_link a, .static_slideshow_content_link a', "color",$slideshow_buttontxt,'' );
}
$slideshow_buttonborder=of_get_option('slideshow_buttonborder');
if ($slideshow_buttonborder) {
	$dynamic_css .= mtheme_change_class( '.slideshow_content_link a, .static_slideshow_content_link a', "border-color",$slideshow_buttonborder,'' );
}
$slideshow_buttonhover_text=of_get_option('slideshow_buttonhover_text');
if ($slideshow_buttonhover_text) {
	$dynamic_css .= mtheme_change_class( '.slideshow_content_link a:hover, .static_slideshow_content_link a:hover', "color",$slideshow_buttonhover_text,'' );
}
$slideshow_buttonhover_bg=of_get_option('slideshow_buttonhover_bg');
if ($slideshow_buttonhover_bg) {
	$dynamic_css .= mtheme_change_class( '.slideshow_content_link a:hover, .static_slideshow_content_link a:hover', "background-color",$slideshow_buttonhover_bg,'' );
	$dynamic_css .= mtheme_change_class( '.slideshow_content_link a:hover, .static_slideshow_content_link a:hover', "border-color",$slideshow_buttonhover_bg,'' );
}
$slideshow_captionbg=of_get_option('slideshow_captionbg');
$slideshow_captionbg_rgb=mtheme_hex2RGB($slideshow_captionbg,true);
if ($slideshow_captionbg) {
	$dynamic_css .= "#slidecaption {
background: -moz-linear-gradient(top,  rgba(0,0,0,0) 0%, rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0)), color-stop(100%,rgba(".$slideshow_captionbg_rgb.",0.55)));
background: -webkit-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: -o-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: -ms-linear-gradient(top,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
background: linear-gradient(to bottom,  rgba(0,0,0,0) 0%,rgba(".$slideshow_captionbg_rgb.",0.55) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='".$slideshow_captionbg."', endColorstr='".$slideshow_captionbg."',GradientType=0 );
}";
}
$slideshow_transbar=of_get_option('slideshow_transbar');
$slideshow_transbar_rgb=mtheme_hex2RGB($slideshow_transbar,true);
if ($slideshow_transbar) {
	$dynamic_css .= "
	#progress-bar {
	background:".$slideshow_transbar.";
	}
	";
}
$slideshow_currthumbnail=of_get_option('slideshow_currthumbnail');
if ($slideshow_currthumbnail) {
	$dynamic_css .= mtheme_change_class( 'ul#thumb-list li.current-thumb', "border-color",$slideshow_currthumbnail,'');
}


$general_bgcolor = of_get_option('general_background_color');
if ($general_bgcolor) {
	$dynamic_css .= mtheme_change_class( 'body,body.theme-boxed',"background-color", $general_bgcolor,'' );
}
$page_background=of_get_option('page_background');
$page_background_rgb=mtheme_hex2RGB($page_background,true);
if ($page_background) {
	$dynamic_css .= '.container-wrapper { background: '. $page_background .'; }';
}

$page_contentscolor=of_get_option('page_contentscolor');
if ($page_contentscolor) {
	$dynamic_css .= mtheme_change_class( '.woocommerce .entry-summary div[itemprop="description"],.entry-content,.entry-content .pullquote-left,.entry-content .pullquote-right,.entry-content .pullquote-center', "color",$page_contentscolor,'' );
}
$page_contentsheading=of_get_option('page_contentsheading');
if ($page_contentsheading) {
$content_headings = '
.woocommerce div.product .product_title,
.woocommerce #content div.product .product_title,
.woocommerce-page div.product .product_title,
.woocommerce-page #content div.product .product_title,
.entry-content h1,
.entry-content h2,
.entry-content h3,
.entry-content h4,
.entry-content h5,
.entry-content h6
';
	$dynamic_css .= mtheme_change_class( $content_headings, "color",$page_contentsheading,'' );
}


$footer_bgcolor=of_get_option('footer_bgcolor');
if ($footer_bgcolor) {
	$dynamic_css .= mtheme_change_class( '.footer-container-wrap, .footer-container', "background",$footer_bgcolor,'' );
}
$footer_labeltext=of_get_option('footer_labeltext');
if ($footer_labeltext) {
	$dynamic_css .= mtheme_change_class( '#footer h3', "color",$footer_labeltext,'' );
}
$footer_text=of_get_option('footer_text');
if ($footer_text) {
	$dynamic_css .= mtheme_change_class( '#footer,#footer .footer-column .sidebar-widget', "color",$footer_text,'' );
}
$footer_link=of_get_option('footer_link');
if ($footer_link) {
	$dynamic_css .= mtheme_change_class( '#footer a,#footer .footer-column .sidebar-widget a', "color",$footer_link,'' );
}
$footer_linkhover=of_get_option('footer_linkhover');
if ($footer_linkhover) {
	$dynamic_css .= mtheme_change_class( '#footer a:hover,#footer .footer-column .sidebar-widget a:hover', "color",$footer_linkhover,'' );
}
$footer_hline=of_get_option('footer_hline');
if ($footer_hline) {
	$dynamic_css .= mtheme_change_class( '#footer.sidebar ul li', "border-top-color",$footer_hline,'' );
}
$footer_copyrightbg=of_get_option('footer_copyrightbg');
if ($footer_copyrightbg) {
	$dynamic_css .= mtheme_change_class( '#copyright', "background-color",$footer_copyrightbg,'' );
}
$footer_copyrighttext=of_get_option('footer_copyrighttext');
if ($footer_copyrighttext) {
	$dynamic_css .= mtheme_change_class( '#copyright', "color",$footer_copyrighttext,'' );
}



$fullscreen_toggle_color = of_get_option('fullscreen_toggle_color');
if ($fullscreen_toggle_color) {
	$dynamic_css .= mtheme_change_class( '.menu-toggle',"color", $fullscreen_toggle_color,'' );
}
$fullscreen_toggle_bg = of_get_option('fullscreen_toggle_bg');
if ($fullscreen_toggle_bg) {
	$dynamic_css .= mtheme_change_class( '.menu-toggle',"background-color", $fullscreen_toggle_bg,'' );
	$dynamic_css .= mtheme_change_class( '.menu-toggle:after',"border-color", $fullscreen_toggle_bg,'' );
}

$fullscreen_toggle_hovercolor = of_get_option('fullscreen_toggle_hovercolor');
if ($fullscreen_toggle_hovercolor) {
	$dynamic_css .= mtheme_change_class( '.menu-toggle:hover',"color", $fullscreen_toggle_hovercolor,'' );
}

$fullscreen_toggle_hoverbg = of_get_option('fullscreen_toggle_hoverbg');
if ($fullscreen_toggle_hoverbg) {
	$dynamic_css .= mtheme_change_class( '.menu-toggle:hover',"background-color", $fullscreen_toggle_hoverbg,'' );
	$dynamic_css .= mtheme_change_class( '.menu-toggle:hover:after',"border-color", $fullscreen_toggle_hoverbg,'' );
}

$footer_copyrightbg=of_get_option('footer_copyrightbg');
$footer_copyrightbg_rgb=mtheme_hex2RGB($footer_copyrightbg,true);
if ($footer_copyrightbg) {
	$dynamic_css .= '#copyright { background:rgba('. $footer_copyrightbg_rgb .',0.8); }';
}
$footer_copyrighttext=of_get_option('footer_copyrighttext');
if ($footer_copyrighttext) {
	$dynamic_css .= mtheme_change_class( '#copyright', "color",$footer_copyrighttext,'' );
}


$sidebar_headingcolor=of_get_option('sidebar_headingcolor');
if ($sidebar_headingcolor) {
	$dynamic_css .= mtheme_change_class( '.sidebar h3', "color",$sidebar_headingcolor,'' );
}
$sidebar_linkcolor=of_get_option('sidebar_linkcolor');
if ($sidebar_linkcolor) {
	$dynamic_css .= mtheme_change_class( '#recentposts_list .recentpost_info .recentpost_title, #popularposts_list .popularpost_info .popularpost_title,.sidebar a', "color",$sidebar_linkcolor,'' );
}
$sidebar_linkbordercolor=of_get_option('sidebar_linkbordercolor');
if ($sidebar_linkbordercolor) {
	$dynamic_css .= mtheme_change_class( '.sidebar .sidebar-widget a', "border-color",$sidebar_linkbordercolor,'' );
}
$sidebar_textcolor=of_get_option('sidebar_textcolor');
if ($sidebar_textcolor) {
	$dynamic_css .= mtheme_change_class( '.contact_address_block .about_info, #footer .contact_address_block .about_info, #recentposts_list p, #popularposts_list p,.sidebar-widget ul#recentcomments li,.sidebar', "color",$sidebar_textcolor,'' );
}

if ( of_get_option('custom_font_css')<>"" ) {
	$dynamic_css .= of_get_option('custom_font_css');
}

$photowall_title_color=of_get_option('photowall_title_color');
if ($photowall_title_color) {
$dynamic_css .= mtheme_change_class( '.photowall-title', "color",$photowall_title_color,'' );
}

$photowall_description_color=of_get_option('photowall_description_color');
if ($photowall_description_color) {
$dynamic_css .= mtheme_change_class( '.photowall-desc', "color",$photowall_description_color,'' );
}

$photowall_hover_titlecolor=of_get_option('photowall_hover_titlecolor');
if ($photowall_hover_titlecolor) {
$dynamic_css .= mtheme_change_class( '.photowall-item:hover .photowall-title', "color",$photowall_hover_titlecolor,'' );
}
$photowall_hover_descriptioncolor=of_get_option('photowall_hover_descriptioncolor');
if ($photowall_hover_descriptioncolor) {
$dynamic_css .= mtheme_change_class( '.photowall-item:hover .photowall-desc', "color",$photowall_hover_descriptioncolor,'' );
}

$photowall_description_color=of_get_option('photowall_description_color');
if ($photowall_description_color) {
$dynamic_css .= mtheme_change_class( '.photowall-desc', "color",$photowall_description_color,'' );
}

// The icons
$forum_icon_css = mtheme_set_fontawesome('fa fa-cubes' , of_get_option('forum_icon') , $get_css_code = true );
if ( isSet($forum_icon_css) ) {
	$dynamic_css .= '
	#bbpress-forums ul.forum li.bbp-forum-info:before {
	content:"'.$forum_icon_css.'";
	}
	';
}

$blog_allowedtags=of_get_option('blog_allowedtags');
if ($blog_allowedtags) {
$dynamic_css .= '.form-allowed-tags { display:none; }';
}
$dynamic_css .= stripslashes_deep( of_get_option('custom_css') );
?>