<html class="maintenance-html">
<head>
    <title><?php echo stripslashes($title); ?></title>
    <link rel="shortcut icon" href="<?php echo MARARKUS_IMAGES_URL ?>/favicon.ico" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="author" content="<?php echo esc_attr($author); ?>" />
    <meta name="description" content="<?php echo esc_attr($description); ?>" />
    <meta name="keywords" content="<?php echo esc_attr($keywords); ?>" />
    <meta name="robots" content="<?php echo esc_attr($robots); ?>" />
    <?php
    if (!empty($styles) && is_array($styles)) {
        foreach ($styles as $src) {
            ?>
            <link rel="stylesheet" href="<?php echo $src; ?>">
            <?php
        }
    }
    if (!empty($custom_css) && is_array($custom_css)) {
        echo '<style>' . implode(array_map('stripslashes', $custom_css)) . '</style>';
    }

    $body_classes .= ' mar-arkus';
    $body_classes .= ' maintenance-page';

    // do some actions
    do_action('wm_head'); // this hook will be removed in the next versions
    do_action('wpmm_head');
    ?>
</head>
<body class="<?php echo $body_classes ? $body_classes : ''; ?>">
<?php do_action('wpmm_after_body'); ?>
<h1 class="hidden">Mar-arkus - Marine Consultants</h1>
<div class="maintenance-wrap container-fluid">
    <span class="highlight-button">View our Latest Project</span>
    <div class="maintenance-stage col-xs-12 col-sm-7">
        <div class="maintenance-header">
            <h2>Coming soon</h2>
            <span>We’re making lots of improvements and will be back soon...</span>
        </div>
        <div class="maintenance-modules">
            <?php if (!empty($this->plugin_settings['modules']['subscribe_status']) && $this->plugin_settings['modules']['subscribe_status'] == 1
                // If the bot is active, legacy subscribe form will be hidden
                // !empty($this->plugin_settings['bot']['status']) &&
                && $this->plugin_settings['bot']['status'] === 0 ) { ?>
                <?php if (!empty($this->plugin_settings['modules']['subscribe_text'])) { ?><h3><?php echo stripslashes($this->plugin_settings['modules']['subscribe_text']); ?></h3><?php } ?>
                <div class="subscribe_wrapper">
                    <form class="subscribe_form">
                        <fieldset class="subscribe_border">
                            <input type="text" placeholder="<?php _e('your e-mail...', $this->plugin_slug); ?>" name="email" class="email_input" data-rule-required="true" data-rule-email="true" data-rule-required="true" data-rule-email="true" />
                            <button type="submit" ><?php _e('Subscribe', $this->plugin_slug); ?></button>
                        </fieldset>
                        <?php if (!empty($this->plugin_settings['gdpr']['status']) && $this->plugin_settings['gdpr']['status'] == 1) { ?>
                            <div class="privacy_checkbox">
                                <label>
                                    <input type="checkbox" name="acceptance" value="YES" data-rule-required="true" data-msg-required="<?php esc_attr_e('This field is required.', $this->plugin_slug); ?>" autocomplete="false">
                                    <span><?php _e("I've read and agree with the site's privacy policy", $this->plugin_slug); ?></span>
                                </label>
                            </div>
                            <?php if(!empty($this->plugin_settings['gdpr']['subscribe_form_tail'])) { ?>
                                <p class="privacy_tail"><?php echo $this->plugin_settings['gdpr']['subscribe_form_tail']; ?></p>
                            <?php }} ?>
                        <?php if ($this->plugin_settings['gdpr']['status'] == 1) { ?>
                            <p class="privacy_tail"> Check out our <a href="<?php echo $this->plugin_settings['gdpr']['policy_page_link']; ?>" class="privacy-trigger" target="_blank"><?php echo $this->plugin_settings['gdpr']['policy_page_label']; ?></a> for more information on how we store and protect your data.</p>
                        <?php } ?>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="maintenance-sider col-xs-12 col-sm-5">
        <div class="maintenance-company-contacts">
            <figure class="maintenance-logo">
                <a href="/">
                    <img src="<?php echo MARARKUS_IMAGES_URL ?>/logo_min.svg" alt="<?php echo stripslashes($title); ?>" />
                </a>
            </figure>
            <div class="maintenance-text">
                <p>
                    Rua Quinta do Arco, N.º 213<br/>
                    Ponte das Mestras<br/>
                    2400-714 Leiria
                </p>
                <p>
                    <a href="tel:+351 244 823 775">+351 244 823 775</a><br/>
                    <a href="tel:+351 914 716 100">+351 914 716 100</a>
                </p>
                <p>
                    <a href="mailto:info@mar-arkus.com">info@mar-arkus.com</a><br/>
                    <a href="mailto:s.wanderley@mar-arkus.com">s.wanderley@mar-arkus.com</a>
                </p>
            </div>
            <div class="maintenance-text maintenance-slogan">
                <p>
                    Naval Architects and<br/>
                    Marine Engineers Services
                </p>
            </div>
        </div>
    </div>
</div>
<div class="maintenance-privacy-holder hidden">
    <div class="maintenance-privacy maintenance-text">
        <h2>Quem somos</h2>
        <p>O endereço do nosso site é: http://www.mar-arkus.com/</p>
        <h2>Que dados pessoais são recolhidos e como são recolhidos</h2>
        <h3>Comentários</h3>
        <p>Quando os visitantes deixam comentários no site, são guardados os dados presentes no formulário dos comentários, bem como o endereço de IP e o agente do utilizador do navegador, para ajudar com a detecção de spam.</p>
        <p>Uma string anónima criada a partir do seu endereço de email (também designada por hash) pode ser enviada para o serviço Gravatar para verificar se o está a utilizar. A política de privacidade do serviço Gravatar está disponível aqui: https://automattic.com/privacy/. Depois do seu comentário ser aprovado, a fotografia do ser perfil fica visível para o público no contexto do seu comentário.</p>
        <h3>Multimédia</h3>
        <p>Ao carregar imagens para o site, deve evitar carregar imagens com dados incorporados de geolocalização (EXIF GPS). Os visitantes podem descarregar e extrair os dados de geolocalização das imagens do site.</p>
        <h3>Cookies</h3>
        <p>Se deixar um comentário no nosso site pode optar por guardar o seu nome, endereço de email e site nos cookies. Isto é para sua conveniência para não ter de preencher novamente os seus dados quando deixar outro comentário. Estes cookies durarão um ano.</p>
        <p>Se tem uma conta e iniciar sessão neste site, será configurado um cookie temporário para determinar se o seu navegador aceita cookies. Este cookie não contém dados pessoais e será eliminado ao fechar o seu navegador. </p>
        <p>Ao iniciar a sessão, serão configurados alguns cookies para guardar a sua informação de sessão e as suas escolhas de visualização de ecrã. Os cookies de início de sessão duram um ano. Se seleccionar "Lembrar-me", a sua sessão irá persistir durante duas semanas. Ao terminar a sessão, os cookies de inicio de sessão serão removidos.</p>
        <p>Se editar ou publicar um artigo, será guardado no seu navegador um cookie adicional. Este cookie não inclui dados pessoais apenas indica o ID de conteúdo do artigo que acabou de editar. Expira ao fim de 1 dia.</p>
        <h3>Conteúdo incorporado de outros sites</h3>
        <p>Os artigos neste site podem incluir conteúdo incorporado (por exemplo: vídeos, imagens, artigos, etc.). O conteúdo incorporado de outros sites comporta-se tal como se o utilizador visitasse esses sites.</p>
        <p>Este site pode recolher dados sobre si, usar cookies, incorporar rastreio feito por terceiros, monitorizar as suas interacções com o mesmo, incluindo registar as interacções com conteúdo incorporado se tiver uma conta e estiver com sessão iniciada nesse site.</p>
        <h2>Por quanto tempo são retidos os seus dados</h2>
        <p>Se deixar um comentário, o comentário e os seus metadados são guardados indefinidamente. Isto acontece de modo a ser possível reconhecer e aprovar automaticamente quaisquer comentários seguintes, em vez de os colocar numa fila de moderação.</p>
        <p>Para utilizadores que se registem no nosso site (se algum), guardamos a informação pessoal fornecida no seu perfil de utilizador. Todos os utilizadores podem ver, editar, ou eliminar a sua informação pessoal a qualquer momento (com a excepção de não poderem alterar o nome de utilizador). Os administradores do site podem também ver e editar essa informação.</p>
        <h2>Que direitos tem sobre os seus dados</h2>
        <p>Se tiver uma conta neste site, ou deixou comentários, pode pedir para receber um ficheiro de exportação com os dados pessoais guardados sobre si, incluindo qualquer dado pessoal que indicou. Também pode solicitar que os dados guardados sejam eliminados. Isto não inclui qualquer dado pessoal que seja obrigatório manter para fins administrativos, legais ou de segurança.</p>
        <h2>Para onde são enviados os seus dados</h2>
        <p>Os comentários dos visitantes podem ser verificados através de um serviço automático de detecção de spam.</p>
    </div>
</div>
<div class="highlight-holder hidden">
    <div class="highlight-container">
        <div class="highlight-content">
            <div class="highlight-video">
                <video id="highlight_video" width="100%" controls>
                    <source src="<?php echo MARARKUS_IMAGES_URL ?>/videos/boat.mp4" type="video/mp4">
                    Your browser does not support the video tag.
                </video>
            </div>
            <div class="highlight-text maintenance-text">
                <h2>Consultancy and Surveying</h2>
                <p>
                    <strong>Consortium Otten<br/>Research Vessels and Mar-Arkus Marine Consultants</strong><br/>
                    (<a href="https://www.research-vessels.de" target="_blank">https://www.research-vessels.de</a> and <a href="https://www.mar-arkus.com" target="_blank">https://www.mar-arkus.com</a> )<br/>
                    <br/>
                    <strong>Vessel’s Name:</strong>  FRV “BAÍA FARTA” <br/>
                    <strong>Owner:</strong> Ministry of Fisheries and The Sea - ANGOLA<br/>
                    <br/>
                    <strong>Designer:</strong> SKIPSTEKNISK <br/>(<a href="https://www.skipsteknisk.no" target="_blank">https://www.skipsteknisk.no</a>)<br/>
                    <br/>
                    <strong>Builder:</strong> DAMEN SHIPYARDS GORINCHEM <br/>(<a href="https://www.damen.com/companies/damen-shipyards-gorinchem" target="_blank">https://www.damen.com/companies/damen-shipyards-gorinchem</a> )<br/>
                    <br/>
                    <strong>Vessel’s Arrival to Luanda – Angola – 12/DEC/2018</strong>
                </p>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript'>
    var wpmm_vars = {"ajax_url": "<?php echo admin_url('admin-ajax.php'); ?>"};
</script>

<?php

// Hook before scripts, mostly for internationalization
do_action('wpmm_before_scripts');

if (!empty($scripts) && is_array($scripts)) {
    foreach ($scripts as $src) {
        ?>
        <script src="<?php echo $src; ?>"></script>
        <?php
    }
}
// Do some actions
do_action('wm_footer'); // this hook will be removed in the next versions
do_action('wpmm_footer');
?>
</body>
</html>