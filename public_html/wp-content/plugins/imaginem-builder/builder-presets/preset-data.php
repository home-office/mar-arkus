<?php
$presets = array();
$presets[] = array( "name"			=> __("Homepage 1", "mthemelocal" ),
					"slug"			=> "homepage-1");

$presets[] = array( "name"			=> __("Homepage 2", "mthemelocal" ),
					"slug"			=> "homepage-2");

$presets[] = array( "name"			=> __("Homepage 3", "mthemelocal" ),
					"slug"			=> "homepage-3");

$presets[] = array( "name"			=> __("Homepage 4", "mthemelocal" ),
					"slug"			=> "homepage-4");

$presets[] = array( "name"			=> __("Homepage OnePage", "mthemelocal" ),
					"slug"			=> "homepage-onepage");

$presets[] = array( "name"			=> __("Homepage Blog 1", "mthemelocal" ),
					"slug"			=> "homepage-blog1");

$presets[] = array( "name"			=> __("Homepage Blog 2", "mthemelocal" ),
					"slug"			=> "homepage-blog2");

$presets[] = array( "name"			=> __("Homepage Creative", "mthemelocal" ),
					"slug"			=> "homepage-creative");

$presets[] = array( "name"			=> __("Homepage Portfolio 1", "mthemelocal" ),
					"slug"			=> "homepage-portfolio1");

$presets[] = array( "name"			=> __("Homepage Portfolio 2", "mthemelocal" ),
					"slug"			=> "homepage-portfolio2");

$presets[] = array( "name"			=> __("Homepage Portfolio 3", "mthemelocal" ),
					"slug"			=> "homepage-portfolio3");

$presets[] = array( "name"			=> __("Homepage Portfolio Wall", "mthemelocal" ),
					"slug"			=> "homepage-portfoliowall");

$presets[] = array( "name"			=> __("About Us", "mthemelocal" ),
					"slug"			=> "about-us");

$presets[] = array( "name"			=> __("About Me", "mthemelocal" ),
					"slug"			=> "about-me");

$presets[] = array( "name"			=> __("Contact Us", "mthemelocal" ),
					"slug"			=> "contact-us");

$presets[] = array( "name"			=> __("Meet the team", "mthemelocal" ),
					"slug"			=> "meet-the-team");

$presets[] = array( "name"			=> __("Our Office", "mthemelocal" ),
					"slug"			=> "our-office");

$presets[] = array( "name"			=> __("Our Services", "mthemelocal" ),
					"slug"			=> "our-services");

$presets[] = array( "name"			=> __("Blog Timeline", "mthemelocal" ),
					"slug"			=> "blog-timeline");

$presets[] = array( "name"			=> __("Blog Grid", "mthemelocal" ),
					"slug"			=> "blog-grid");
?>