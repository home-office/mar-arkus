<?php
/*
Plugin Name: iMaginem Gallery Creator
Plugin URI: http://www.imaginemthemes.com/
Description: Imaginem Themes Gallery Custom Post Type
Version: 1.0
Author: iMaginem
Author URI: http://www.imaginemthemes.com
*/

class mtheme_Gallery_Posts {

    function __construct() 
    {
		//require_once ( plugin_dir_path( __FILE__ ) . 'gallery-post-sorter.php');
		
        add_action('init', array(&$this, 'init'));
        add_action('admin_init', array(&$this, 'admin_init'));
        add_filter("manage_edit-mtheme_gallery_columns", array(&$this, 'mtheme_gallery_edit_columns'));
		//add_action("manage_posts_custom_column",  array(&$this, 'mtheme_gallery_custom_columns'));
		add_action("manage_mtheme_gallery_posts_custom_column",  array(&$this, 'mtheme_gallery_custom_columns'));
	}

	/*
	* Gallery Admin columns
	*/
	function mtheme_gallery_custom_columns($column){
		global $post;
		$image_url=wp_get_attachment_thumb_url( get_post_thumbnail_id( $post->ID ) );
		
		$full_image_id = get_post_thumbnail_id(($post->ID), 'thumbnail'); 
		$full_image_url = wp_get_attachment_image_src($full_image_id,'thumbnail');  
		$full_image_url = $full_image_url[0];

		if (!defined('MTHEME')) {
			$mtheme_shortname = "mtheme_p2";
			define('MTHEME', $mtheme_shortname);
		}
	    switch ($column)
	    {
	        case "gallery_image":
				if ( isset($image_url) && $image_url<>"") {
	            echo '<a class="thickbox" href="'.esc_url($full_image_url).'"><img src="'.esc_url($image_url).'" width="60px" height="60px" alt="featured" /></a>';
				}
	            break;
	        case "galleries":
	            echo get_the_term_list($post->ID, 'galleries', '', ', ','');
	            break;
	    } 
	}

	function mtheme_gallery_edit_columns($columns){

	    $columns = array(
	        "cb" => "<input type=\"checkbox\" />",
	        "title" => __('Gallery Title','mthemelocal'),
	        "galleries" => __('Categories','mthemelocal'),
			"gallery_image" => __('Image','mthemelocal')
	    );
	 	
	    return $columns;
	}
	
	/**
	 * Registers TinyMCE rich editor buttons
	 *
	 * @return	void
	 */
	function init()
	{
		/*
		* Register Featured Post Manager
		*/
		//add_action('init', 'mtheme_featured_register');
		//add_action('init', 'mtheme_gallery_register');//Always use a shortname like "mtheme_" not to see any 404 errors
		/*
		* Register Gallery Post Manager
		*/
	    $mtheme_gallery_slug="gallery";
	    if (function_exists('of_get_option')) {
	    	$mtheme_gallery_slug = of_get_option('gallery_permalink_slug');
		}
	    if ( $mtheme_gallery_slug=="" || !isSet($mtheme_gallery_slug) ) {
	        $mtheme_gallery_slug="gallery";
	    }
	    $mtheme_gallery_singular_refer = "Gallery";
	    if (function_exists('of_get_option')) {
	    	$mtheme_gallery_singular_refer = of_get_option('gallery_singular_refer');
		}
		if ( $mtheme_gallery_singular_refer == '' ) {
			$mtheme_gallery_singular_refer= 'Gallery';
		}
	    $args = array(
	        'label' => $mtheme_gallery_singular_refer . __(' Items','mthemelocal'),
	        'singular_label' => __('Gallery','mthemelocal'),
	        'public' => true,
	        'show_ui' => true,
	        'capability_type' => 'post',
	        'hierarchical' => true,
	        'has_archive' =>true,
			'menu_position' => 6,
	    	'menu_icon' => plugin_dir_url( __FILE__ ) . 'images/portfolio.png',
	        'rewrite' => array('slug' => $mtheme_gallery_slug),//Use a slug like "work" or "gallery" that shouldnt be same with your page name
	        'supports' => array('title', 'excerpt','editor', 'thumbnail','comments','revisions')//Boxes will be shown in the panel
	       );
	 
	    register_post_type( 'mtheme_gallery' , $args );
		/*
		* Add Taxonomy for Gallery 'Type'
		*/
		register_taxonomy("galleries", array("mtheme_gallery"), array("hierarchical" => true, "label" => "Gallery Categories", "singular_label" => "Galleries", "rewrite" => true));
		 
		/*
		* Hooks for the Gallery and Featured viewables
		*/
	}
	/**
	 * Enqueue Scripts and Styles
	 *
	 * @return	void
	 */
	function admin_init()
	{
		if( is_admin() ) {
			// Load only if in a Post or Page Manager	
			if ('edit.php' == basename($_SERVER['PHP_SELF'])) {
				//wp_enqueue_script('jquery-ui-sortable');
				wp_enqueue_script('thickbox');
				wp_enqueue_style('thickbox');
				//wp_enqueue_style( 'mtheme-gallery-sorter-CSS',  plugin_dir_url( __FILE__ ) . '/css/style.css', false, '1.0', 'all' );
				if ( isSet($_GET["page"]) ) {
					if ( $_GET["page"] == "gallery-post-sorter.php" ) {
						//wp_enqueue_script("post-sorter-JS", plugin_dir_url( __FILE__ ) . "js/post-sorter.js", array( 'jquery' ), "1.0");
					}
				}
			}
		}
	}
    
}
$mtheme_gallery_post_type = new mtheme_Gallery_Posts();
?>