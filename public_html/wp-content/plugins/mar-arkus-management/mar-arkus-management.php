<?php

/**
 * Mar Arkus Management
 *
 * Plugin Name: Mar-arkus Management
 * Plugin URI: http://www.mar-arkus.com/
 * Description: All management functions and customizations.
 * Version: 1.0
 * Author: Pedro Alfaiate
 * Author URI: http://www.mar-arkus.com/
 * Twitter:
 * GitHub Plugin URI:
 * GitHub Branch:
 * Text Domain: mar-arkus-management
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path: /mararkus
 */


define('MARARKUS_PATH', plugin_dir_path(__FILE__));
define('MARARKUS_FUNCTIONS_PATH', MARARKUS_PATH . 'includes/functions/');

/**
 * DEFINE URLS
 */

define('MARARKUS_URL', plugin_dir_url(__FILE__));
define('MARARKUS_JS_URL', MARARKUS_URL . 'assets/js/');
define('MARARKUS_CSS_URL', MARARKUS_URL . 'assets/css/');
define('MARARKUS_IMAGES_URL', MARARKUS_URL . 'assets/images/');

/**
 * OTHER DEFINES
 */

define('SCRIPT_MARARKUS_DEBUG', true);
define('MARARKUS_ASSETS_SUFFIX', (defined('SCRIPT_MARARKUS_DEBUG') && SCRIPT_MARARKUS_DEBUG) ? '' : '.min');

/**
 * FUNCTIONS
 */

abstract class Mararkus_Project
{
    protected function cleanCode(){

        /* ====== Remove Emoji =======*/

        //remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
        //remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
        //remove_action( 'wp_print_styles', 'print_emoji_styles' );
        //remove_action( 'admin_print_styles', 'print_emoji_styles' );

    }
    public function load() {
        $output = "";
        $this->cleanCode();
        return $output;
    }
}

class Page extends Mararkus_Project
{
    public function init(){

        // Maintenance

        $Maintenance = new Maintenance();
        $Maintenance->load();

        // Backend

        //$Backend = new Backend();
        //$Backend->load();

        add_action( 'wp_loaded',[new Backend,'load']);

        $Frontend = new Frontend();
        $Frontend->load();

        //add_action( 'wp_loaded',[new Frontend,'load']);

    }
}

require_once(MARARKUS_FUNCTIONS_PATH . 'maintenance.php');
require_once(MARARKUS_FUNCTIONS_PATH . 'backend.php');
require_once(MARARKUS_FUNCTIONS_PATH . 'frontend.php');

$projectPage = new Page();
$projectPage -> init();


if (is_multisite() && !function_exists('is_plugin_active_for_network')) {
	require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
}

/**
 * FRONTEND
 */

register_activation_hook(__FILE__, array('Mar_arkus_management', 'activate'));
register_deactivation_hook(__FILE__, array('Mar_arkus_management', 'deactivate'));

//add_action('plugins_loaded', array('Mar_arkus_management', 'get_instance'));