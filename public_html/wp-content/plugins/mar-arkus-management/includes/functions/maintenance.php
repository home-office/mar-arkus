<?php

class Maintenance extends Mararkus_Project
{

    public function load(){

        add_filter('wpmm_styles',function($styles){
            $styles["frontend"] = MARARKUS_CSS_URL . 'maintenance' . MARARKUS_ASSETS_SUFFIX . '.css';
            $styles["fancybox"] = MARARKUS_CSS_URL . 'libraries/jquery.fancybox.css';
            return $styles;
        });

        add_filter('wpmm_scripts',function($scripts){
            $scripts["mararkus"] = MARARKUS_JS_URL . 'main.js';
            $scripts["fancybox"] = MARARKUS_JS_URL . 'libraries/jquery.fancybox.js';
            $scripts["fancybox_media"] = MARARKUS_JS_URL . 'libraries/jquery.fancybox-media.js';
            return $scripts;
        });

        add_action('wpmm_head',function (){

            $script = "<!-- Global site tag (gtag.js) - Google Analytics -->
                        <script async src='https://www.googletagmanager.com/gtag/js?id=UA-126403566-1'></script>
                        <script>
                          window.dataLayer = window.dataLayer || [];
                          function gtag(){dataLayer.push(arguments);}
                          gtag('js', new Date());
                          gtag('config', 'UA-126403566-1');
                        </script>";

            $script .= "<!-- Google Tag Manager -->
                        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                        })(window,document,'script','dataLayer','GTM-W29DN2V');</script>
                        <!-- End Google Tag Manager -->";

            echo($script);
        });

        add_action('wpmm_after_body',function (){
            $script = '<!-- Google Tag Manager (noscript) -->
                        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-W29DN2V"
                        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
                        <!-- End Google Tag Manager (noscript) -->';
            echo($script);
        });

        add_action('wpmm_footer',function (){
            $script = "<script type='text/javascript'>jQuery(function () {Mararkus.Maintenance().load();});</script>";
            echo($script);
        });

    }

}