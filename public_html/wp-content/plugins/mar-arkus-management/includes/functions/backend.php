<?php

class Backend extends Mararkus_Project
{

    public function load(){


        global $current_user;
        global $user_ID;
        $user = new WP_User($user_ID); //$current_user = wp_get_current_user();

        //Remove admin bar

        add_action('after_setup_theme', function(){
            if (!current_user_can('administrator') && !is_admin()) {
                show_admin_bar(false);
            }
        });

        foreach ($user->roles as $urole) {
            $userRole = get_role($urole);

            // Add Remove custom role
            //remove_role('admin');
            //add_role('admin', __( 'Admin' ),$userRole->capabilities);
            //$userRole->remove_cap( 'delete_users' );

            if ($urole == "administrator" && !$userRole->has_cap( 'backoffice_administrator')) {
                $userRole->add_cap('backoffice_administrator');
                $userRole->remove_cap('backoffice_admin');
            } else if ($urole == "admin" && !$userRole->has_cap( 'backoffice_admin')) {
                $userRole->add_cap('backoffice_admin');
                $userRole->remove_cap('backoffice_administrator');
            } else if ($urole == "customer" && !$userRole->has_cap( 'backoffice_customer')) {
                $userRole->add_cap('backoffice_customer');
            }
        }

        // Custom Admin Bar

        add_action('admin_bar_menu', function($wp_admin_bar){
            $wp_admin_bar->remove_node( 'wp-logo' );
            $wp_admin_bar->remove_node( 'theme_options' );
            $wp_admin_bar->remove_node( 'wpseo-menu' );
            $wp_admin_bar->remove_node( 'new-content' );
            $wp_admin_bar->remove_node( 'w3tc' );
        }, 999 );

        // Set Admin Menus

        add_action( 'admin_menu', function() {
            if (current_user_can("backoffice_admin")) {
                global $submenu;
                unset($submenu['index.php'][10]);
            }
            return $submenu;
        });

        add_action('admin_head', function(){

            if (current_user_can("backoffice_admin")) {

                // Top

                remove_submenu_page('index.php', 'sendgrid-statistics');
                remove_submenu_page('index.php', 'update-core.php');// Updates

                // Theme

                //remove_menu_page('Jupiter');

                //Generic Menus

                //remove_menu_page('edit.php?post_type=page'); //Pages
                //remove_menu_page('edit.php'); // Posts
                //remove_menu_page('edit-comments.php'); // Comments

                remove_menu_page('themes.php'); // Appearance
                remove_menu_page('users.php'); //Users
                remove_menu_page('tools.php'); //Tools
                remove_menu_page('options-general.php'); // Options
                remove_menu_page('upload.php'); // Media

                // Custom Post Types

                //remove_menu_page('edit.php?post_type=photo_album');
                //remove_menu_page('edit.php?post_type=testimonial');
                remove_menu_page('edit.php?post_type=tab_slider');
                remove_menu_page('edit.php?post_type=pricing');
                //remove_menu_page('edit.php?post_type=portfolio');
                //remove_menu_page('edit.php?post_type=news');
                //remove_menu_page('edit.php?post_type=faq');
                //remove_menu_page('edit.php?post_type=employees');
                remove_menu_page('edit.php?post_type=edge');
                remove_menu_page('edit.php?post_type=animated-columns');
                remove_menu_page('edit.php?post_type=clients');

                // Features

                //remove_menu_page('wpcf7');
                //remove_menu_page('onesignal-push');
                //remove_menu_page('wplivechat-menu');
                remove_menu_page('wpseo_dashboard');
                remove_menu_page('vc-general');
                remove_menu_page('duplicator');
                remove_menu_page('w3tc_dashboard');

                remove_submenu_page('upload.php','wp-smush-bulk');
                remove_submenu_page('tools.php','sql-executioner');

                remove_submenu_page('options-general.php', 'sendgrid-settings');
                //remove_submenu_page('options-general.php', 'wpmem-settings');
                remove_submenu_page('options-general.php', 'wp-mail-smtp/wp_mail_smtp.php');
                remove_submenu_page('options-general.php', 'ctcc');

                remove_submenu_page('tools.php', 'redux-about');

                // Woocommerce

                //remove_menu_page('woocommerce');
                //remove_menu_page('edit.php?post_type=product');
                remove_menu_page('mycred');
                //remove_submenu_page('users.php', 'mycred_default-history');
                remove_menu_page('yit_plugin_panel');

                // Galleries

                remove_menu_page('revslider');
                //remove_menu_page('layerslider');
                remove_menu_page('masterslider');

                //remove_admin_menu_separator(2);
                //remove_admin_menu_separator(3);

            }
        } , 99999 );

        // Custom Update Notifications

        add_action('in_admin_header',function(){

            if(!current_user_can("backoffice_administrator")) {
                global $wp_filter;
                if (is_network_admin() and isset($wp_filter["network_admin_notices"])) {
                    unset($wp_filter['network_admin_notices']);
                } elseif (is_user_admin() and isset($wp_filter["user_admin_notices"])) {
                    unset($wp_filter['user_admin_notices']);
                } else {
                    if (isset($wp_filter["admin_notices"])) {
                        unset($wp_filter['admin_notices']);
                    }
                }

                if (isset($wp_filter["all_admin_notices"])) {
                    unset($wp_filter['all_admin_notices']);
                }
            }

        },100000);

        // Custom Admin Widgets Dashboard

        add_action('wp_dashboard_setup', function(){
            if(!current_user_can("backoffice_administrator")) {

                remove_meta_box('dashboard_quick_press', 'dashboard', 'side'); // rascunho rapido
                remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); //resumo paginas, artigos e comentarios
                remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //actividade
                //remove_meta_box('cms_tpv_dashboard_widget_page', 'dashboard', 'normal'); // treepageview
                remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal'); //yoast SEO
                remove_meta_box('mk_posts_like_stats', 'dashboard', 'side'); //likes
                remove_meta_box('dashboard_custom_feed', 'dashboard', 'side'); //feed wordpress
                remove_meta_box('dashboard_primary', 'dashboard', 'side'); //Noticias wordpress
                remove_meta_box('mycred_overview', 'dashboard', 'side'); // Pontos
                //remove_meta_box('woocommerce_dashboard_recent_reviews', 'dashboard', 'side'); //reviews
                //remove_meta_box('woocommerce_dashboard_status', 'dashboard', 'side'); //encomendas

                remove_action('welcome_panel', 'wp_welcome_panel');

                // Custom Boxes

                add_meta_box( 'id', 'Mar-arkus Marine Consultants', 'dash_widget', 'dashboard', 'side', 'high' );
                function dash_widget(){
                    echo '<p>Naval Architects and Marine Engineers Services</p><br>';
                }

            }
        });

        // Custom Admin WP version

        add_action('admin_menu', function(){
            // Remove WP version
            remove_filter( 'update_footer', 'core_update_footer' );
        });

        // Custom Admin Footer Text

        add_filter('admin_footer_text', function(){
            echo '<span id="footer-thankyou">Mar-arkus Marine Consultants | Naval Architects and Marine Engineers Services</span>';
        });

        // Custom Admin Login

        add_action('login_head', function() {
            echo '<link rel="stylesheet" type="text/css" href="' . MARARKUS_CSS_URL . 'login.css" />';
        });

        add_action('login_head',function() {

            echo '<style type="text/css">
                     h1 a {
                            background-image:url("'.MARARKUS_IMAGES_URL.'logo_min.svg") !important;
                      }
                   </style>';

        });

        add_filter('login_headerurl', function()
        {
            return get_bloginfo('url');  //or other custom Url
        });

        add_filter('login_headertitle', function()
        {
            return get_option('blogname'); // or other custom Text
        });

        // Allow SVG files

        add_filter('upload_mimes', function ($mimes){
            $mimes['svg'] = 'image/svg+xml';
            return $mimes;
        });

    }

}