<?php

class Frontend extends Mararkus_Project
{

    public function load(){


        global $current_user;
        global $user_ID;
        $user = new WP_User($user_ID); //$current_user = wp_get_current_user();

        $this->styles();


    }

    public function styles(){
        add_action('init', function (){
            if(!is_admin()){
                wp_enqueue_style('mar-arkus-main', MARARKUS_CSS_URL .'main.less', array('MainStyle'));
            }
        }, PHP_INT_MAX);
    }

    public function scrips(){

    }

}