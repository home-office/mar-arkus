
var Mararkus = {
    Page:function () {
        return{
            init:function () {
                Mararkus.Tools().fancybox();
            }
        }
    },
    Maintenance: function () {
        return{
            load:function (options) {

                var $privacy_trigger = jQuery('.privacy-trigger');

                $privacy_trigger.click(function (event) {
                    event.preventDefault();

                    var screen = Mararkus.Tools().device().dimension(jQuery(window).width()),
                        maxWidth = jQuery(window).width() * 60 / 100,
                        margin = 60;

                    if(screen == 'xs' || screen == 'sm' || screen == 'md'){
                        maxWidth = jQuery(window).width() * 90 / 100;
                        margin = 30;
                    }

                    jQuery.fancybox({
                        maxWidth: maxWidth,
                        margin  : margin,
                        openEffect: 'elastic',
                        nextEffect : 'elastic',
                        closeEffect: 'elastic',
                        openEasing:'swing',
                        closeEasing:'swing',
                        openMethod:'dropUp',
                        closeMethod:'dropDown',
                        scrolling: 'auto',
                        scrollOutside:false,
                        content:jQuery('.maintenance-privacy-holder').html(),
                        beforeClose:function () {

                        },
                        tpl: {
                            wrap: '<div class="fancybox-wrap fancybox-privacy" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'
                        }
                    });

                });


                function load_highlight() {

                    var screen = Mararkus.Tools().device().dimension(jQuery(window).width()),
                        maxWidth = jQuery(window).width(), // * 60 / 100,
                        margin = 60;

                    if(screen == 'xs' || screen == 'sm' || screen == 'md'){
                        maxWidth = jQuery(window).width(), // * 90 / 100;
                            margin = 30;
                    }



                    jQuery.fancybox({
                        maxWidth: maxWidth,
                        margin  : margin,
                        openEffect: 'elastic',
                        nextEffect : 'elastic',
                        closeEffect: 'elastic',
                        openEasing:'swing',
                        closeEasing:'swing',
                        openMethod:'dropUp',
                        closeMethod:'dropDown',
                        scrolling: 'auto',
                        scrollOutside:true,
                        content:jQuery('.highlight-holder').html(),
                        afterShow:function(){

                            setTimeout(function () {
                                var highlight_video = jQuery(".fancybox-highlight #highlight_video").get(0);
                                highlight_video.play();
                            },1000);

                            /*
                             jQuery('.highlight-video').click(function () {
                             highlight_video = document.getElementById("highlight_video");
                             if (highlight_video.paused){
                             highlight_video.play();
                             }else{
                             highlight_video.pause();
                             }
                             });
                             */
                        },
                        beforeClose:function () {
                            var highlight_video = document.getElementById("highlight_video");
                            highlight_video.pause();
                        },
                        tpl: {
                            wrap: '<div class="fancybox-wrap fancybox-highlight" tabIndex="-1"><div class="fancybox-skin"><div class="fancybox-outer"><div class="fancybox-inner"></div></div></div></div>'
                        }
                    });

                }

                window.highlight_started = false;

                setTimeout(function () {
                    if(!window.highlight_started){
                        window.highlight_started = true;
                        load_highlight();
                    }
                },6000);

                jQuery('.highlight-button').click(function () {
                    window.highlight_started = true;
                    load_highlight();
                });

            }
        }
    },
    Tools:function(){
        return{
            scroll:function (options){

                var scrollTop = 0,
                    easeTime = 500,
                    delay = 1;

                if(typeof options.time != "undefined"){
                    easeTime = options.time;
                }
                if(typeof options.$element != "undefined"){
                    scrollTop = options.$element.offset().top;
                }
                if(typeof options.offsetTop != "undefined"){
                    scrollTop = scrollTop + options.offsetTop;
                }
                if(typeof options.top != "undefined"){
                    scrollTop = options.top;
                }
                if(typeof options.delay != "undefined"){
                    delay = options.delay;
                }

                setTimeout(function () {
                    jQuery('html, body').stop(true).animate({scrollTop:scrollTop},easeTime);
                },delay);

            },
            device:function () {
                return{
                    dimension: function (width) {

                        var deviceXS = 480,
                            deviceSM = 768,
                            deviceMD = 992,
                            deviceLG = 1200,
                            output = '';

                        if (width < (deviceSM - 1)) { output = "xs"; }
                        if (width >= deviceSM && width < (deviceMD - 1)) { output = "sm"; }
                        if (width >= deviceMD && width < (deviceLG - 1)) { output = "md"; }
                        if (width >= deviceLG) { output = "lg"; }

                        return output;
                    }
                }
            },
            fancybox: function () {

                (function ($, F) {

                    F.transitions.dropUp = function() {
                        var endPos = F._getPosition(true);

                        endPos.top = (parseInt(endPos.top, 10) + 200) + 'px';

                        F.wrap.css(endPos).css({'opacity':0}).show().animate({
                            top: '-=200px',
                            opacity:1
                        }, {
                            duration: F.current.openSpeed,
                            complete: F._afterZoomIn
                        });
                    };

                    F.transitions.dropDown = function() {
                        F.wrap.removeClass('fancybox-opened').animate({
                            top: '+=200px',
                            opacity:0
                        }, {
                            duration: F.current.closeSpeed,
                            complete: F._afterZoomOut
                        });
                    };

                }(jQuery, jQuery.fancybox));

            }
        }
    }
};

jQuery(function () {
    Mararkus.Page().init();
});